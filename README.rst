Installation
============

Download python package using git::

  $ git clone git@gitlab.com:fgoudreault/abicards.git
  $ cd abicards

Use python venvs and install package using pip::

  $ python3.11 -m venv venv/abicards
  $ pip install -U pip
  $ pip install -e .

Execute post install script::

  $ abicards --install
