"""Main widget code."""
import logging
from typing import Any, Mapping

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
        QFrame, QScrollArea,
        QTabWidget,
        QVBoxLayout,
        QWidget,
        )

from .routines import get_loglevel, get_main_toolbar, unsaved_changes


class DraggableScrollArea(QScrollArea):
    """Class for a draggable scroll area using right click."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._begin_drag = None

    def mousePressEvent(self, event):
        """Call on mouse press event."""
        if event.buttons() == Qt.RightButton:
            self._begin_drag = event.pos()
        return super().mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        """Call on mouse release event."""
        if event.buttons() == Qt.RightButton and self._begin_drag is not None:
            self._begin_drag = None
        return super().mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        """Call on mouse move event."""
        if event.buttons() == Qt.RightButton and self._begin_drag is not None:
            dx = self._begin_drag.x() - event.x()
            dy = self._begin_drag.y() - event.y()
            self._begin_drag = event.pos()
            hbar = self.horizontalScrollBar()
            vbar = self.verticalScrollBar()
            hbar.setValue(hbar.value() + dx)
            vbar.setValue(vbar.value() + dy)


class AbicardsMainWidget(QWidget):
    """Abicards main widget.

    It is a tab widget and each tab is a board.
    """

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._logger = logging.getLogger("AbicardsMainWidget")
        self._logger.setLevel(get_loglevel())
        self.boards = []
        self.setup_ui()
        self.setAcceptDrops(True)
        self.pomodoro = None
        # self.global_overlay = None

    @property
    def board(self):
        """The currently shown board."""
        return self.board_tabs.currentWidget()

    def setup_ui(self) -> None:
        """Setup main widget ui."""
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.board_tabs = QTabWidget(parent=self)
        self.layout().addWidget(self.board_tabs)
        # empty tab with nothing in it
        self.add_board_tab = QFrame()
        self.board_tabs.addTab(self.add_board_tab, "+")
        self.board_tabs.tabBarClicked.connect(self.on_tab_bar)
        self.board_tabs.currentChanged.connect(self.on_tab_changed)

    def add_pomodoro(self) -> None:
        """Add the pomodoro dialog."""
        if self.pomodoro is not None:
            raise ValueError("There is already a pomodoro card.")
        from .pomodoro import Pomodoro
        self.pomodoro = Pomodoro(parent=self)
        self.update_main_toolbar_buttons()
        self.pomodoro.show()

    def remove_pomodoro(self) -> None:
        """Remove the pomodoro timer."""
        self.pomodoro.hide()
        self.pomodoro.destroy()
        self.pomodoro = None
        self.update_main_toolbar_buttons()
        self._logger.info("Removing pomodoro.")

    def update_main_toolbar_buttons(self):
        """Update the main toolbar buttons."""
        tb = get_main_toolbar()
        if tb is None:
            return
        tb.add_pomodoro_button.setEnabled(self.pomodoro is None)

    def on_tab_bar(self, index: int) -> None:
        """Called when tab bar is clicked."""
        if index == len(self.boards):
            # add a board and switch to it
            self.add_board()
            return
        # ask to rename the board title
        if index == self.board_tabs.currentIndex():
            self.board.toggle_edit_dialog()
        else:
            # switch boards
            self.switch_to_board(None)

    def switch_to_board(
            self, index: int | None,
            ) -> None:
        """Switch tp the board at given index."""
        from .board import Board
        if index is not None:
            if isinstance(index, Board):
                index = self.boards.index(index)
            self.board_tabs.setCurrentIndex(index)
            # index can be none when clicking on tabs
            self._logger.info(
                    f"Switching to board '{self.board.title}'.")

    def on_tab_changed(self, index: int) -> None:
        """Called when current tab changed."""
        if index == len(self.boards) + 1:
            return
        from .board import Board
        if not isinstance(self.board, Board):
            # for some reason, the current widget is not a board...
            return
        self.board.update_main_toolbar_buttons()

    @unsaved_changes
    def add_board(
            self, load_from: dict[str, Any] = None,
            unsaved_changes: bool = True) -> None:
        """Add a board."""
        from .board import Board
        board = Board()
        self.boards.append(board)
        if load_from is not None:
            board.load_json_data(load_from)
        if board.title == "":
            title = "Unknown"
        else:
            title = board.title
        self.board_tabs.insertTab(len(self.boards) - 1, board, title)
        board.title_changed.connect(self.update_board_tabs)
        self.switch_to_board(
                len(self.boards) - 1)
        # , unsaved_changes=unsaved_changes)
        if unsaved_changes:
            self._logger.info("Adding board.")
            return True

    @unsaved_changes
    def delete_board(
            self, board) -> None:
        """Delete the given board."""
        self.boards.remove(board)
        self.board_tabs.removeTab(self.board_tabs.indexOf(board))
        board.hide()
        board.destroy()

    def update_board_tabs(self):
        """Update the board tabs titles."""
        for iboard, board in enumerate(self.boards):
            self.board_tabs.setTabText(
                    iboard, board.title)

    def add_card_list(self) -> None:
        """Add a card."""
        self.board.add_card_list()

    def add_planer(self) -> None:
        """Add the planer."""
        self.board.add_planer()

    def highlight_widgets(self, *args) -> None:
        """Highlight widgets."""
        self.board.highlight_widgets(*args)

    def unhighlight(self) -> None:
        """Unhighlight widgets."""
        self.board.unhighlight()

    def clear_boards(self) -> None:
        """Clear all boards from main widget."""
        for board in self.boards:
            board.hide()
            board.destroy()
        self.boards = []

    def get_json_data(self) -> dict:
        """Json data to save on file."""
        data = {"boards": [], "current_board_index": None}
        for board in self.boards:
            data["boards"].append(board.get_json_data())
        if len(self.boards):
            data["current_board_index"] = self.board_tabs.currentIndex()
        return data

    def load_json_data(self, data: Mapping[str, Any]) -> None:
        """Load data from a json dict."""
        self.clear_boards()
        if "cards_container" in data:
            # legacy
            self.add_board(
                    load_from=data["cards_container"],
                    unsaved_changes=False)
            return
        if "board" in data:
            # legacy
            self.add_board(
                    load_from=data["board"], unsaved_changes=False)
            return
        if "boards" in data:
            # most recent
            for board_data in data["boards"]:
                self.add_board(load_from=board_data, unsaved_changes=False)
        self.switch_to_board(0)
        # if "current_board_index" in data:
        #    # if data["current_board_index"] is not None:
        #    #     self.switch_to_board(
        #    #             data["current_board_index"], unsaved_changes=False)
