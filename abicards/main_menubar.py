"""The menu bar module."""
from PySide6.QtGui import QAction, QKeySequence
from PySide6.QtWidgets import QMenuBar

from .styles_constants import __PALETTE__


class AbicardsMainMenuBar(QMenuBar):
    """Main menubar for the abicards app."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._logger = self.parent()._logger
        self.setup_ui()

    def setup_ui(self) -> None:
        """Build the menu bar."""
        self._build_file_menu()
        self._build_edit_menu()
        self.setStyleSheet(
                f"""
                QMenuBar::item {{
                    background-color: transparent;
                    }}
                QMenuBar::item::selected {{
                    background-color: {__PALETTE__['purple']}
                }}""")

    def _build_edit_menu(self):
        """Build the edit menu."""
        self.edit_menu = self.addMenu("Edit")
        # add card
        add_card_list_action = QAction("new card list", self.parent())
        add_card_list_action.triggered.connect(
                self.parent().abicards_widget.add_card_list)
        self.edit_menu.addAction(add_card_list_action)

    def _build_file_menu(self):
        """Build the file menu."""
        self.file_menu = self.addMenu("File")
        # open
        open_action = QAction("Open", self.parent())
        open_action.triggered.connect(self.parent().open_boards)
        open_action.setShortcut(QKeySequence("Ctrl+o"))
        self.file_menu.addAction(open_action)

        # save & quick save
        save_action = QAction("Save", self.parent())
        save_action.triggered.connect(self.parent().save)
        save_action.setShortcut(QKeySequence("Ctrl+s"))
        self.file_menu.addAction(save_action)

        save_as_action = QAction("Save as", self.parent())
        save_as_action.triggered.connect(self.parent().save_as)
        self.file_menu.addAction(save_as_action)

        # settings
        self.file_menu.addSeparator()
        open_settings_action = QAction("Settings", self.parent())
        open_settings_action.triggered.connect(self.parent().open_settings)
        self.file_menu.addAction(open_settings_action)

        # close
        self.file_menu.addSeparator()
        exit_action = QAction("Close", self.parent())
        exit_action.setShortcut(QKeySequence.Quit)
        exit_action.triggered.connect(self.parent().close)
        self.file_menu.addAction(exit_action)
