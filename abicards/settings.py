"""Settings window for the application."""
import logging
import os
from math import floor
from typing import Any, Mapping

from PySide6.QtCore import QObject, QSettings, Qt, Signal
from PySide6.QtWidgets import (
        QCheckBox, QDialog, QGridLayout, QLabel, QPushButton, QWidget,
        )

from .routines import (
        get_generic_file_dialog_for_loading_board,
        get_loglevel,
        get_main_window,
        )


__DEFAULT_SETTINGS__ = {
        "last_open_card_file_dir": os.path.expanduser("~"),
        "last_open_card_file_basename": "mycards.json",
        "load_file_on_startup": "None",
        "check_update_available_on_startup": "True",
    }
# these are stored in settings file but do not appear in settings window
__OTHER_VALID_SETTINGS__ = ("geometry", )
__WIDGETS_GETTER_SETTER_MAP__ = {
        'QCheckBox': ('checkState', 'setCheckState'),
        'QLineEdit': ('text', 'setText'),
        'QSpinBox': ('value', 'setValue'),
        'QRadioButton': ('isChecked', 'setChecked'),
    }
__DEFAULT_BUTTON_HEIGHT__ = 30
__DEFAULT_TITLE_LABEL_HEIGHT__ = 20


class SettingsManager(QObject):
    """Create a settings manager for the abicards application."""

    settings_changed = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.settings = QSettings("abicards", "settings")
        self._logger = logging.getLogger("SettingsManager")
        self._logger.setLevel(get_loglevel())

    def get_value(self, attr: str) -> Any:
        """Get a settings value.

        Arguments
        ---------
        attr: str
            The value to get.
        """
        if attr not in __DEFAULT_SETTINGS__:
            if attr not in __OTHER_VALID_SETTINGS__:
                raise KeyError(f"Invalid settings: {attr}")
            return self.settings.value(attr)  # no defaults
        return self.settings.value(attr, __DEFAULT_SETTINGS__[attr])

    def set_value(self, *args, **kwargs) -> None:
        """Set a settings value."""
        self.settings.setValue(*args, **kwargs)

    def save_settings(self) -> None:
        """An alias to sync_settings."""
        self.sync_settings()

    def sync_settings(self) -> None:
        """Sync settings with file."""
        self.settings.sync()

    def update_widgets_from_settings(
            self, widgets: Mapping[str, QWidget]) -> None:
        """Update a map of widgets for the settings.

        Parameters
        ----------
        widgets: Mapping[str, QWidget]
            Keys are the name of the setting and values are the widget to
            update.
        """
        for name, widget in widgets.items():
            cls = widget.__class__.__name__
            getter, setter = __WIDGETS_GETTER_SETTER_MAP__.get(
                    cls, (None, None))
            value = self.settings.value(name)
            if setter and value is not None:
                fn = getattr(widget, setter)
                fn(value)  # Set the widget.

    def update_settings_from_widgets(
            self, widgets: Mapping[str, QWidget]) -> None:
        """Update settings from a map of widgets.

        Parameters
        ----------
        widgets: Mapping[str, QWidget]
            The map of widgets where the keys are the settings name
            and the values are the widgets to use.
        """
        for name, widget in widgets.items():
            cls = widget.__class__.__name__
            getter, setter = __WIDGETS_GETTER_SETTER_MAP__.get(
                    cls, (None, None))
            if getter:
                fn = getattr(widget, getter)
                value = fn()
                if value is not None:
                    self.set_value(name, value)  # Set the settings.
        # Notify watcher of changed settings.
        self.settings_changed.emit()


class SettingsWindow(QDialog):
    """Settings window."""

    def __init__(self, settings_manager: SettingsManager, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setWindowTitle("Settings")
        self._logger = logging.getLogger("SettingsWindow")
        self._logger.setLevel(get_loglevel())
        self.settings_manager = settings_manager
        self.settings_widgets = {}
        self.setup_ui()
        self.settings_manager.update_widgets_from_settings(
                self.settings_widgets)
        self.resize(600, 300)

    def reset_defaults(self) -> None:
        """Reset default settings."""
        self._logger.info("Resetting to default settings.")
        for name, widget in self.settings_widgets.items():
            if name in __DEFAULT_SETTINGS__:
                clsname = widget.__class__.__name__
                setmethname = __WIDGETS_GETTER_SETTER_MAP__[clsname][1]
                setter = getattr(widget, setmethname)
                setter(__DEFAULT_SETTINGS__[name])

    def save_and_exit(self):
        """Save and exit settings window."""
        self.save_settings()
        self.hide()

    def save_settings(self):
        """Save settings on file."""
        self.settings_manager.update_settings_from_widgets(
                self.settings_widgets)
        self.settings_manager.save_settings()

    def hide(self):
        """Hide settings window."""
        self._logger.info("Hiding settings window.")
        self.setVisible(False)

    def setup_ui(self):
        """Build settings window gui."""
        self.layout = QGridLayout(self)
        self.setLayout(self.layout)
        nrows, ncols = 0, 0

        # widgets for settings
        nrows, ncols = self._build_load_on_startup_setting_gui(nrows, ncols)
        nrows, ncols = self._build_update_app_gui(nrows, ncols)
        nrows, ncols = self.setup_update_gui(nrows, ncols)

        # other widgets not related to settings
        self.save_and_exit_button = QPushButton("Save && Close")
        self.save_and_exit_button.setFixedHeight(__DEFAULT_BUTTON_HEIGHT__)
        midspan = ncols // 2
        rest = ncols - midspan
        self.layout.addWidget(
                self.save_and_exit_button, nrows + 1, 0, 1, midspan)
        self.save_and_exit_button.clicked.connect(self.save_and_exit)
        self.layout.setAlignment(self.save_and_exit_button, Qt.AlignBottom)

        self.reset_defaults_button = QPushButton("Reset defaults")
        self.reset_defaults_button.setFixedHeight(__DEFAULT_BUTTON_HEIGHT__)
        self.layout.addWidget(
                self.reset_defaults_button, nrows + 1, midspan, 1, rest)
        self.layout.setAlignment(self.reset_defaults_button, Qt.AlignBottom)
        self.reset_defaults_button.clicked.connect(self.reset_defaults)

    def setup_update_gui(self, nrows, ncols):
        """Setup the update part of the settings panel."""
        self.check_update_avail_label = QLabel(
            parent=self, text="Check an update is available on startup.")
        midspan = ncols // 2
        self.layout.addWidget(
                self.check_update_avail_label, nrows + 1, 0, 1, midspan)
        self.check_update_avail_checkbox = QCheckBox(
                parent=self)
        self.check_update_avail_checkbox.setChecked(
                self.settings_manager.get_value(
                    "check_update_available_on_startup"
                    ).lower() == "true")
        self.check_update_avail_checkbox.checkStateChanged.connect(
                self.changed_check_update_available)
        self.layout.addWidget(
                self.check_update_avail_checkbox, nrows + 1, midspan, 1,
                ncols - midspan)
        return nrows + 1, ncols

    def changed_check_update_available(self):
        """Callback when changing the 'check_update_available' chkbx."""
        self.settings_manager.set_value(
                "check_update_available_on_startup",
                self.check_update_avail_checkbox.isChecked())

    def _build_update_app_gui(self, nrows, ncols) -> None:
        # add a label + btn to update the app
        self.update_app_label = QLabel(parent=self, text="Update application")
        midspan = ncols // 2
        rest = ncols - midspan
        self.layout.addWidget(self.update_app_label, nrows + 1, 0, 1, midspan)

        self.update_app_btn = QPushButton(parent=self, text="Update")
        self.update_app_btn.clicked.connect(self.update_app)
        self.layout.addWidget(self.update_app_btn, nrows + 1, midspan, 1, rest)
        return nrows + 1, ncols

    def _build_load_on_startup_setting_gui(self, nrows, ncols):
        self.load_file_on_startup_title_label = QLabel("Load file on startup")
        self.load_file_on_startup_title_label.setFixedHeight(
                __DEFAULT_TITLE_LABEL_HEIGHT__)
        self.layout.addWidget(
                self.load_file_on_startup_title_label, nrows + 1, 0, 1, 2)
        self.load_file_on_startup_label = QLabel(
                self.settings_manager.get_value("load_file_on_startup"))
        # span ~ 75%
        if ncols < 4:
            ncols = 4
        ncolspan = floor(0.75 * ncols)
        rest = ncols - ncolspan
        self.layout.addWidget(
                self.load_file_on_startup_label, nrows + 2, 0, 1, ncolspan)

        self.load_file_on_startup_open_btn = QPushButton("Set", parent=self)
        self.load_file_on_startup_open_btn.clicked.connect(
                self.set_load_file_on_startup)
        self.load_file_on_startup_open_btn.setFixedHeight(
                __DEFAULT_BUTTON_HEIGHT__)
        self.layout.addWidget(
                self.load_file_on_startup_open_btn,
                nrows + 2, ncolspan, 1, rest)
        nrows += 2
        return nrows, ncols

    def set_load_file_on_startup(self) -> None:
        """Set the load file on startup path."""
        dialog = get_generic_file_dialog_for_loading_board(
                self.settings_manager, self)
        file_name = dialog.getOpenFileName(
                filter="JSON (*.json)",
                selectedFilter="JSON (*.json)")[0]
        if not file_name:
            return
        self.settings_manager.set_value(
                "load_file_on_startup", file_name)
        self.load_file_on_startup_label.setText(file_name)

    def update_app(self) -> None:
        """Update the app."""
        window = get_main_window()
        window.update_app()
