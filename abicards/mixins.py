"""Mixin classes submodule."""
from typing import Sequence

from PySide6.QtCore import Qt, Signal
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QLayout, QWidget)

from .routines import build_qss, parse_qss, set_widget_font


class IndicatedObject:
    """Widget with some small indicator widgets."""

    def __init__(self, *args, indicator_alignment: str = "left", **kwargs):
        self.indicators_aligned = indicator_alignment

    @property
    def indicators(self) -> Sequence[QWidget]:
        """The list of indicators."""
        return self.get_indicators()

    def get_indicators(self):
        """Get the list of indicators."""
        raise NotImplementedError

    def setup_ui(self) -> None:
        """Setup the indicated part of the UI."""
        self.setup_indicators()

    def setup_indicators(
            self, parent: QWidget = None, layout: QLayout = None) -> None:
        """Setup the indicators. The widgets are not added to layouts.

        Add them in child classes.

        Parameters
        ----------
        layout: QLayout, optional
            The layout onto which we add the indicators. If None,
            the parent's layout is taken.
        parent: QWidget, optional
            The indicator container parent. If None, the mixin object itself
            is taken as parent.
        """
        self.setup_indicator_container(parent=parent)
        for indicator in self.indicators:
            if indicator is None:
                continue
            self.setup_indicator(indicator)
        self.update_indicators()
        if layout is None:
            layout = self.layout()
        layout.addWidget(self.indicator_container)

    def setup_indicator_container(self, parent: QWidget = None) -> None:
        """Setup the indicator container."""
        if parent is None:
            parent = self
        self.indicator_container = QFrame(parent=parent)
        self.indicator_container.setLayout(QHBoxLayout())
        cm = self.indicator_container.layout().contentsMargins()
        cm.setTop(0)
        cm.setBottom(0)
        self.indicator_container.layout().setContentsMargins(cm)
        # self.indicator_container.setStyleSheet("background-color: red")
        self.indicator_container.layout().addStretch()

    def setup_indicator(self, indicator) -> None:
        """Setup a single indicator."""
        if not hasattr(self, "indicator_container"):
            self.setup_indicator_container()
            return
        if self.indicators_aligned == "left":
            self.indicator_container.layout().insertWidget(
                    self.indicator_container.layout().count() - 1,
                    indicator)
        else:
            self.indicator_container.layout().addWidget(indicator)

    def indicator_hidden(self):
        """Called when one indicator has been hidden."""
        # check if all indicators are hidden. if so hide the whole container
        for indicator in self.indicators:
            if indicator is None:
                continue
            if indicator.isVisible():
                return
        self.indicator_container.hide()

    def indicator_shown(self):
        """Called when one indicator is shown."""
        self.indicator_container.show()

    def update_indicators(self) -> None:
        """Update all indicators."""
        raise NotImplementedError


class BaseIndicator(QFrame):
    """Base class for small indicator widgets."""

    hidden = Signal()
    shown = Signal()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, objectName="indicator", **kwargs)
        self.setup_ui()

    def setup_ui(self) -> None:
        """Setup the indicator UI."""
        # basically a label and a status tag
        self.setLayout(QHBoxLayout())
        self.setStyleSheet(
                "QFrame#indicator { border-radius: 8px; }")
        self.setFixedHeight(35)
        self.setup_text_label()

    def setup_text_label(self) -> None:
        """Setup the indicator text label."""
        self.text_label = QLabel(parent=self)
        self.layout().addWidget(self.text_label, alignment=Qt.AlignTop)
        self.text_label.setAlignment(Qt.AlignLeft)
        set_widget_font(self.text_label, point_size=8)

    def hideEvent(self, *args, **kwargs):
        """Hide the dates indicator."""
        tortn = super().hideEvent(*args, **kwargs)
        self.hidden.emit()
        return tortn

    def show(self, *args, **kwargs):
        """Show the dates indicator."""
        self.shown.emit()
        return super().show(*args, **kwargs)

    def setText(self, text: str) -> None:
        """Set the label text."""
        self.text_label.setText(text)

    def set_color(self, color: str) -> None:
        """Set the color of the indicator."""
        name, ss = parse_qss(self.styleSheet())
        ss["background-color"] = color
        if not name:
            name = "QFrame"
        self.setStyleSheet(build_qss(name, ss))

    def update(self) -> None:
        """Update the indicator."""
        raise NotImplementedError
