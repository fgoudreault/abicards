"""Cards and card lists module."""
from math import inf as infinity
from typing import Any, Sequence

from PySide6.QtCore import (
        QObject, QPoint, Qt, Signal,
        )
from PySide6.QtGui import QShortcut
from PySide6.QtWidgets import (
        QFrame,
        QHBoxLayout,
        QLabel,
        QLineEdit,
        QMessageBox, QPushButton,
        QScrollArea, QVBoxLayout,
        )

from .card import Card
from .convert import ConvertToDialog, ConvertibleObject
from .dates import DatableObject, Dates, DatesPickerDialog
from .duration import Duration, DurationObject, DurationSetterDialog
from ..board.board_item import BaseBoardItem
from ..custom_widgets import MinimalSizeTextEdit, SmallCloseButton
from ..mixins import IndicatedObject
from ..routines import (
        get_icon_pixmap, set_widget_font, unsaved_changes,
        )
from ..styles_constants import __BUTTON_HEIGHT_MEDIUM__, __PALETTE__


__ADD_CARD_BUTTON_HEIGHT__ = 40
__CARD_LIST_MAX_HEIGHT__ = 600


class ExpandingScrollArea(QScrollArea):
    """A scroll area that adjust itself to the size of its content.

    Up to a maximum threshold decided upon init.
    """

    def __init__(
            self, *args, maximum_height: int = infinity, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.maximum_height = maximum_height

    def update_size(self):
        """Update the widget size. Must be called by parent."""
        left, top, right, bottom = self.widget().layout().getContentsMargins()
        hint = self.widget().layout().sizeHint().height()
        hint = min(hint, self.maximum_height)
        self.setMaximumHeight(hint + bottom + top + 1)
        self.setMinimumHeight(hint + bottom + top + 1)


class CardList(
        BaseBoardItem, DatableObject, DurationObject, IndicatedObject,
        ConvertibleObject,
        ):
    """Card list containing the cards.

    This object can also be attributed dates to appear in planer.
    """

    card_list_updated = Signal()
    card_updated = Signal()
    has_draggable_subelements = True
    has_collision = True
    has_dates_indicator = True

    def __init__(
            self, *args, title: str = "",
            edit_title_upon_creation: bool = True,
            **kwargs) -> None:
        self.cards = []
        self.edit_title_lineedit = None
        self.edit_enter_shortcut = None
        self.add_card_form = None
        self.add_card_button = None
        self.escape_shortcut = None
        DatableObject.__init__(self)
        DurationObject.__init__(
                self, *args, fixed_by_children_duration=True, **kwargs)
        IndicatedObject.__init__(self, indicator_alignment="right")
        BaseBoardItem.__init__(self, *args, **kwargs)
        self.dates_changed.connect(self.update_dates)
        self.dates_changed.connect(self.card_list_updated)
        self.completed_changed.connect(self.card_list_updated)
        self.duration_changed.connect(self.card_list_updated)
        if not self.title and edit_title_upon_creation:
            self.edit_title()

    def __repr__(self) -> str:
        """The card list representation."""
        return f"< CardList: {self.title} >"

    def can_be_converted(self) -> bool:
        """Return True if card list can be converted."""
        if self.cards:
            return False
        return True

    def get_json_data(self) -> dict[str, Any]:
        """Return json data for storage."""
        data = BaseBoardItem.get_json_data(self)
        data.update(DatableObject.get_json_data(self))
        data.update(DurationObject.get_json_data(self))
        data["cards"] = [card.get_json_data() for card in self.cards]
        data["collapsed"] = self.collapsed
        return data

    def load_json_data(self, data: dict[str, Any] = None) -> None:
        """Load card list data from a json dict."""
        BaseBoardItem.load_json_data(self, data)
        if "cards" in data:
            for card in data["cards"]:
                self.add_card(load_from=card, unsaved_changes=False)
        if "collapsed" in data:
            if data["collapsed"]:
                self.collapse_cards(unsaved_changes=False)
        DatableObject.load_json_data(self, data)
        DurationObject.load_json_data(self, data)

    def adjustSize(self, *args, **kwargs) -> Any:
        """Adjust the card list size."""
        if hasattr(self, "cards_container_scroll"):
            self.cards_container_scroll.update_size()
        return BaseBoardItem.adjustSize(self, *args, **kwargs)

    def subdialogs_opened(self) -> bool:
        """Return True if some subdialogs are opened and under the mouse."""
        if self.is_editing_title():
            return True
        if self.edit_menu is not None:
            if self.edit_menu.isVisible():
                return True
        if self.add_card_form is not None:
            if self.add_card_form.isVisible():
                return True
        for card in self.cards:
            if card.subdialogs_opened():
                return True
        return False

    def close_subdialogs(self) -> None:
        """Close the subdialogs if needed."""
        # don't close if mouse is above given widget
        if self.edit_title_lineedit is not None:
            if self.edit_title_lineedit.isVisible and not (
                    self.edit_title_lineedit.underMouse()):
                self.done_editing_title()
        if self.edit_menu is not None:
            any_subdialog_under = False
            for subdialog in self.edit_menu.subdialogs:
                if subdialog is None:
                    continue
                if not subdialog.isVisible():
                    continue
                if subdialog.underMouse():
                    any_subdialog_under = True
                    break
            if not any_subdialog_under:
                for subdialog in self.edit_menu.subdialogs:
                    if subdialog is None:
                        continue
                    subdialog.hide()
            if self.add_card_form is not None:
                if self.add_card_form.isVisible() and not (
                        self.edit_menu.underMouse()):
                    self.done_editing()
        if self.add_card_form is not None:
            if self.add_card_form.isVisible() and not (
                    self.add_card_form.underMouse()):
                self.init_add_card_button()
        for card in self.cards:
            if not card.subdialogs_opened():
                continue
            card.close_subdialogs()

    def draggable_subelement(self) -> Card | None:
        """Check if we start dragging a subelement."""
        for card in self.cards:
            if card.underMouse():
                return card
        return None

    def get_indicators(self) -> Sequence[QObject]:
        """Get the indicators."""
        return (
                DatableObject.get_indicators(self) +
                DurationObject.get_indicators(self)
                )

    def update_indicators(self) -> None:
        """Update the indicators."""
        DatableObject.update_indicators(self)
        DurationObject.update_indicators(self)

    def get_children_duration(self) -> Sequence[Duration]:
        """Get the children duration."""
        durs = []
        for card in self.cards:
            if not card.has_duration():
                durs += card.get_children_duration()
            else:
                durs.append(card.duration)
        return durs

    def any_children_has_duration(self) -> bool:
        """Return True if any children has duration."""
        for card in self.cards:
            if card.any_children_has_duration():
                return True
        return DurationObject.any_children_has_duration(self)

    def setup_ui(self) -> None:
        """Setup the UI."""
        BaseBoardItem.setup_ui(self)
        DatableObject.setup_ui(self)
        DurationObject.setup_ui(self)
        IndicatedObject.setup_ui(self)
        self.setup_cards_container()
        self.init_add_card_button()

    def setup_cards_container(self) -> None:
        """Setup the cards container."""
        self.cards_container = QFrame(parent=self)
        self.cards_container.setStyleSheet("background-color: transparent")
        self.cards_container.setLayout(QVBoxLayout())
        self.cards_container.setContentsMargins(0, 0, 0, 0)
        self.cards_container.layout().setContentsMargins(0, 0, 0, 0)
        self.cards_container_scroll = ExpandingScrollArea(
                maximum_height=__CARD_LIST_MAX_HEIGHT__,
                parent=self)
        self.cards_container_scroll.setHorizontalScrollBarPolicy(
                Qt.ScrollBarAlwaysOff)
        self.cards_container_scroll.setVerticalScrollBarPolicy(
                Qt.ScrollBarAlwaysOff)
        self.cards_container_scroll.setStyleSheet(
                "background-color: transparent")
        self.cards_container_scroll.setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self.cards_container_scroll)
        self.cards_container_scroll.setWidgetResizable(True)
        self.cards_container_scroll.setWidget(self.cards_container)

    def setup_header_ui(self) -> None:
        """Setup the header ui."""
        BaseBoardItem.setup_header_ui(self)
        # insert collapse card button between empty label and dots
        self.collapse_cards_button = QPushButton(
                parent=self.header_container,
                text="Collapse", objectName="transparent_btn",
                )
        self.collapse_cards_button.clicked.connect(self.collapse_cards)
        self.header_container.layout().insertWidget(
                1, self.collapse_cards_button)

    def setup_title_ui(self) -> None:
        """Setup the title ui."""
        BaseBoardItem.setup_title_ui(self)
        # card count label (when collapsing)
        self.card_count_label = QLabel(
                parent=self.title_label_container,
                )
        set_widget_font(self.card_count_label, color=__PALETTE__["gray"])
        self.title_label_container.layout().addWidget(self.card_count_label)

    def indicator_hidden(self):
        """Called when one indicater has been hidden."""
        IndicatedObject.indicator_hidden(self)
        self.adjustSize()

    def indicator_shown(self):
        """Called when one indicator is shown."""
        IndicatedObject.indicator_shown(self)
        self.adjustSize()

    @property
    def collapsed(self) -> bool:
        """Return True if the card list is collapsed."""
        return self.collapse_cards_button.text() == "Expand"

    @unsaved_changes
    def collapse_cards(self, unsaved_changes: bool = True) -> None:
        """Collapse the cards."""
        if not self.collapsed:
            # collapse
            # remove add card form if relevent
            self.init_add_card_button()
            # hide everything
            self.cards_container_scroll.hide()
            self.add_card_button.hide()
            set_widget_font(
                    self.card_count_label,
                    color=__PALETTE__["white"],
                    )
            # self.card_count_label.show()
            self.collapse_cards_button.setText("Expand")
        else:
            # expand
            self.collapse_cards_button.setText("Collapse")
            # self.card_count_label.hide()
            self.cards_container_scroll.show()
            self.add_card_button.show()
            set_widget_font(
                    self.card_count_label,
                    color=__PALETTE__["gray"],
                    )
        self.adjustSize()
        if unsaved_changes:
            if self.collapsed:
                self._logger.info(
                        f"Collapsing card list '{self.title}'.")
            else:
                self._logger.info(
                    f"Uncollapsing card list '{self.title}'.")
            return True

    def toggle_edit_menu(self) -> None:
        """Toggle on or off the card list edit menu."""
        if self.edit_menu is None:
            self.edit_menu = CardListEditMenu(
                    self, parent=self.parent())
            self.edit_menu.done.connect(
                    self.done_editing)
            self.edit_menu.delete_card_list.connect(
                    self.delete)
        if self.edit_menu.isVisible():
            self.done_editing()
        else:
            self.edit_menu.show()
        # position next to the dots button
        abspos = self.edit_button.mapToGlobal(
                QPoint(self.edit_button.width(), 0))
        relpos = self.parent().mapFromGlobal(abspos)
        self.edit_menu.move(relpos)

    def done_editing(self):
        """Called when edit menu is dismissed."""
        if self.edit_menu is None:
            return
        if self.edit_menu.isVisible():
            self.edit_menu.hide()
        if self.edit_menu.dates_picker_dialog is not None:
            self.edit_menu.dates_picker_dialog.hide()

    def delete(self):
        """Delete the card list."""
        self.parent().delete_card_list(self)
        self.card_list_updated.emit()
        self.done_editing()

    def init_add_card_button(
            self, add_card_if_non_empty: bool = False) -> None:
        """Initialize the 'add card button'.

        Also disables the 'add card form'.
        """
        if self.add_card_button is None:
            self.add_card_button = AddCardButton(parent=self)
            self.layout().addWidget(self.add_card_button)
        if self.add_card_form is not None:
            if add_card_if_non_empty:
                # check if form was filled. if so, add a card
                self.add_card()
                return
            self.add_card_form.hide()
        if self.escape_shortcut is not None:
            self.escape_shortcut.setEnabled(False)
        self.add_card_button.show()

    def init_add_card_form(self) -> None:
        """Initialize the 'add card form'.

        Also disables the 'add card button'.
        """
        if self.add_card_form is None:
            self.add_card_form = AddCardForm(parent=self)
            self.layout().addWidget(self.add_card_form)
            self.escape_shortcut = QShortcut(Qt.Key_Escape, self.add_card_form)
            self.escape_shortcut.activated.connect(
                    self.init_add_card_button)
        if self.add_card_button is not None:
            self.add_card_button.hide()
            # disable all other add card form of other card lists
            for card_list in self.parent().card_lists:
                if card_list is self:
                    continue
                card_list.init_add_card_button()
        self.escape_shortcut.setEnabled(True)
        self.add_card_form.reset_text()
        self.add_card_form.setFocus()
        self.add_card_form.show()

    @unsaved_changes
    def delete_card(self, card: Card) -> bool | None:
        """Delete a card."""
        for c in self.cards:
            if card is not c:
                continue
            self.cards.remove(c)
            break
        else:
            return None
        c.hide()
        self.cards_container.layout().removeWidget(c)
        self._logger.info(
                f"Deleting card named '{card.title}' from list '{self.title}'."
                )
        c.destroy()
        self.adjustSize()
        self.card_list_updated.emit()
        return True

    def mousePressEvent(self, *args, **kwargs) -> Any:
        """Called when mouse is pressed inside card list."""
        if self.subdialogs_opened():
            self.close_subdialogs()
            return BaseBoardItem.mousePressEvent(self, *args, **kwargs)
        if self.title_label.underMouse():
            self.edit_title()
        return BaseBoardItem.mousePressEvent(self, *args, **kwargs)

    @unsaved_changes
    def add_card(
            self, load_from: dict[str, Any] = None,
            unsaved_changes: bool = True,
            ) -> None:
        """Add an empty card to the card list.

        Parameters
        ----------
        load_from: dict, optional
            The card properties to load from a json dict.
        """
        # if card was added because of add card form, retrieve title
        title = ""
        if self.add_card_form is not None:
            if self.add_card_form.isVisible():
                title = self.add_card_form.get_title()
                if title == "" and load_from is None:
                    # don't add any card
                    self.init_add_card_button()
                    return
        card = Card(parent=self.cards_container)
        card.card_updated.connect(self.card_updated)
        card.card_updated.connect(self.update_card_count_label)
        card.card_updated.connect(self.update_dates)
        card.card_updated.connect(self.card_list_updated)
        card.card_updated.connect(self.update_duration)
        if load_from is not None:
            card.load_json_data(load_from)
        else:
            card.set_title(title)
        self.cards_container.layout().addWidget(card)
        self.cards.append(card)
        self.update_card_count_label()
        self.update_dates()
        # connect here in case we're loading data to not tell user
        # of unsaved changes
        card.dates_changed.connect(self.update_dates)
        # once this is done, immediately ask for a new card
        # unless we're loading data
        if load_from is None:
            self.init_add_card_form()
        else:
            self.init_add_card_button()
        if unsaved_changes:
            self._logger.info(
                f"Adding card named '{card.title}' to card list "
                f"named '{self.title}'.")
            return True

    def get_children_dates(self) -> Sequence[Dates]:
        """The children dates."""
        return [c.dates for c in self.cards]

    def get_dates_picker_dialog(self):
        """Get the dates picker dialog if it exists."""
        if self.edit_menu is None:
            return None
        return self.edit_menu.dates_picker_dialog

    def update_card_count_label(self) -> None:
        """Update the card count label."""
        n = len(self.cards)
        plural = "" if n in (0, 1) else "s"
        self.card_count_label.setText(f"{n} card{plural}")

    def edit_title(self) -> None:
        """Initialize the edit title mode."""
        # enter editing mode
        self._logger.info(
                f"Editing title of card list named '{self.title}'")
        self.title_label_container.hide()
        # replace with qlineedit
        if self.edit_title_lineedit is None:
            self.edit_title_lineedit = QLineEdit(
                parent=self, objectName="card_list_name_lineedit",
                )
            set_widget_font(self.edit_title_lineedit, point_size=15, bold=True)
            self.edit_enter_shortcut = QShortcut(Qt.Key_Enter, self)
            self.edit_enter_shortcut.activated.connect(
                    self.done_editing_title)
            self.edit_return_shortcut = QShortcut(Qt.Key_Return, self)
            self.edit_return_shortcut.activated.connect(
                    self.done_editing_title)
        self.edit_enter_shortcut.setEnabled(True)
        self.edit_return_shortcut.setEnabled(True)
        self.edit_title_lineedit.setText(
                self.title if self.title else "Card list title")
        self.edit_title_lineedit.show()
        self.edit_title_lineedit.selectAll()
        self.edit_title_lineedit.setFocus()
        self.layout().replaceWidget(
                self.title_label_container, self.edit_title_lineedit)
        self.adjustSize()

    def done_editing_title(self):
        """Called when user is done editing title."""
        self._logger.info("Done editing card list title.")
        self.edit_enter_shortcut.setEnabled(False)
        self.edit_return_shortcut.setEnabled(False)
        self.layout().replaceWidget(
                self.edit_title_lineedit, self.title_label_container)
        self.set_title(self.edit_title_lineedit.text())
        self.title_label_container.show()
        self.edit_title_lineedit.hide()
        self.adjustSize()
        self.card_list_updated.emit()

    def is_editing_title(self):
        """Return True if we are editing the title at the moment."""
        if self.edit_title_lineedit is None:
            return False
        return self.edit_title_lineedit.isVisible()

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing card list."""
        DatableObject.showEvent(self, *args, **kwargs)
        DurationObject.showEvent(self, *args, **kwargs)
        self.adjustSize()
        return super().showEvent(*args, **kwargs)


class AddCardButton(QPushButton):
    """The +Add card button when the form is absent."""

    def __init__(self, *args, **kwargs):
        super().__init__(
                *args,
                text="+ Add a card", objectName="add_card_button",
                **kwargs,
                )
        self.setFixedHeight(__ADD_CARD_BUTTON_HEIGHT__)
        self.clicked.connect(self.parent().init_add_card_form)

    def hideEvent(self, *args, **kwargs):
        """Called when hiding event occurs."""
        super().hideEvent(*args, **kwargs)
        self.parent().adjustSize()

    def showEvent(self, *args, **kwargs):
        """Called when a show event occurs."""
        super().showEvent(*args, **kwargs)
        self.parent().adjustSize()


class AddCardForm(QFrame):
    """The small form to add a card."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initial_text = "Enter a title for this card..."
        self.setup_ui()

    def setup_ui(self):
        """Setup the add card form UI."""
        self._layout = QVBoxLayout()
        self.setLayout(self._layout)
        self.setup_lineedit()
        self.setup_buttons()

    def setup_lineedit(self) -> None:
        """Setup the lineedit part of the UI."""
        self.card_name_lineedit = MinimalSizeTextEdit(
                parent=self, disable_enter=True,
                # objectName="card_name_lineedit",
                )
        self.card_name_lineedit.pressed_enter.connect(
                self.parent().add_card)
        self.card_name_lineedit.pressed_escape.connect(
                self.parent().init_add_card_button)
        self.card_name_lineedit.height_changed.connect(
                self.adjust_size)
        self.layout().addWidget(self.card_name_lineedit)
        self.return_shortcut = QShortcut(
                Qt.Key_Return, self.card_name_lineedit)
        self.return_shortcut.activated.connect(self.parent().add_card)
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self.card_name_lineedit)
        self.enter_shortcut.activated.connect(self.parent().add_card)
        self.card_name_lineedit.setFixedHeight(30)

    def adjust_size(self):
        """Adjust size of add card form following a change in text edit."""
        self.resize(self.width(), self.minimumSizeHint().height())
        self.parent().adjustSize()

    def setup_buttons(self):
        """Setup the buttons part of the UI."""
        self.buttons_container = QFrame(parent=self)
        self.layout().addWidget(self.buttons_container)
        self.buttons_container_layout = QHBoxLayout()
        self.buttons_container.setLayout(self.buttons_container_layout)
        # add button
        self.add_button = QPushButton(
                "Add card",
                parent=self.buttons_container,
                objectName="purple_btn",
                )
        self.add_button.setFixedWidth(100)
        self.add_button.setFixedHeight(35)
        self.buttons_container_layout.addWidget(self.add_button)
        self.add_button.clicked.connect(self.parent().add_card)

        # cancel button
        self.cancel_button = SmallCloseButton(
                parent=self.buttons_container,
                )
        self.buttons_container_layout.addWidget(
                self.cancel_button)
        self.cancel_button.clicked.connect(
                self.parent().init_add_card_button)
        self.buttons_container_layout.addStretch()

    def clear_lineedit(self):
        """Clear the lineedit text."""
        if self.card_name_lineedit.text() == self._initial_text:
            self.card_name_lineedit.clear()

    def get_title(self) -> str:
        """Get the card name title as written in line edit."""
        return self.card_name_lineedit.toPlainText()

    def reset_text(self):
        """Reset the lineedit text."""
        self.card_name_lineedit.setText("")
        self.card_name_lineedit.setPlaceholderText(
                self._initial_text)

    def setFocus(self):
        """Set the focus on the lineedit."""
        self.card_name_lineedit.setFocus()
        self.enter_shortcut.setEnabled(True)
        self.return_shortcut.setEnabled(True)

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing the widget."""
        # adjust parent size since parent is not in a layout
        self.parent().adjustSize()
        self.enter_shortcut.setEnabled(True)
        self.return_shortcut.setEnabled(True)
        return super().showEvent(*args, **kwargs)

    def hideEvent(self, *args, **kwargs):
        """Called when hiding the event."""
        self.enter_shortcut.setEnabled(False)
        self.return_shortcut.setEnabled(False)
        super().hideEvent(*args, **kwargs)
        self.parent().adjustSize()


class CardListEditMenu(QFrame):
    """The card list edit menu."""

    done = Signal()
    delete_card_list = Signal()

    def __init__(self, card_list, *args, **kwargs):
        super().__init__(*args, objectName="light_gray_frame", **kwargs)
        self.card_list = card_list
        self.dates_picker_dialog = None
        self.duration_setter_dialog = None
        self.convert_to_dialog = None
        self.setup_ui()

    @property
    def subdialogs(self) -> Sequence:
        """The list of subdialogs."""
        subs = [self.dates_picker_dialog, self.duration_setter_dialog,
                self.convert_to_dialog,
                ]
        if self.convert_to_dialog is not None:
            subs += self.convert_to_dialog.subdialogs
        return subs

    def setup_ui(self):
        """Setup the card list edit menu UI."""
        # self.setStyleSheet("background-color: rgba(255, 0, 0, 100)")
        # self.setFixedSize(100, 100)
        self.setLayout(QVBoxLayout())
        self.setFixedWidth(200)
        self.setup_buttons()

    def setup_buttons(self) -> None:
        """Setup the card list edit menu buttons."""
        self.dates_button = QPushButton(
                parent=self, text="Dates",
                objectName="gray_btn",
                )
        self.dates_button.setFixedHeight(
                __BUTTON_HEIGHT_MEDIUM__)
        self.dates_button.setIcon(get_icon_pixmap("dates_color"))
        self.dates_button.clicked.connect(self.open_dates_dialog)
        self.layout().addWidget(self.dates_button)
        # duration button
        self.duration_button = QPushButton(
                parent=self, text="Duration",
                objectName="gray_btn",
                )
        self.duration_button.setIcon(get_icon_pixmap("duration_color"))
        self.duration_button.clicked.connect(self.open_duration_dialog)
        self.layout().addWidget(self.duration_button)
        # convert button
        self.convert_button = QPushButton(
            parent=self, text="Convert",
            objectName="gray_btn",
            )
        self.convert_button.clicked.connect(self.open_convert_to_dialog)
        self.layout().addWidget(self.convert_button)
        # delete button
        self.delete_button = QPushButton(
                parent=self, text="Delete card list",
                objectName="red_btn",
                )
        self.layout().addWidget(self.delete_button)
        self.delete_button.setFixedHeight(
                __BUTTON_HEIGHT_MEDIUM__)
        self.delete_button.clicked.connect(self.open_delete_dialog)

    def open_delete_dialog(self):
        """Open the confirm delete dialog."""
        btn = QMessageBox.question(
                self,
                f"Delete '{self.card_list.title}'?",
                "Are you sure you want to delete this card list?",
                QMessageBox.Yes | QMessageBox.No)
        if btn == QMessageBox.Yes:
            self.delete_card_list.emit()
        else:
            self.done.emit()

    def open_dates_dialog(self):
        """Open the dates dialog."""
        if self.dates_picker_dialog is None:
            self.dates_picker_dialog = DatesPickerDialog(
                    self.card_list.dates, parent=self.parent())
            self.dates_picker_dialog.set_dates.connect(self.set_dates)
            self.dates_picker_dialog.remove_dates.connect(self.remove_dates)
            self.dates_picker_dialog.fixed_by_children_changed.connect(
                    self.card_list.update_dates)
        # place the dialog under dates button
        self.dates_picker_dialog.move(
                self.parent().mapFromGlobal(
                    self.dates_button.parent().mapToGlobal(
                        self.dates_button.geometry().bottomLeft())))
        if self.dates_picker_dialog.isVisible():
            self.dates_picker_dialog.hide()
        else:
            self.dates_picker_dialog.show()

    def set_dates(self) -> None:
        """Set the dates from the dates picker dialog."""
        start_date = self.dates_picker_dialog.get_start_date()
        end_date = self.dates_picker_dialog.get_end_datetime()
        self.card_list.set_dates(
                start_date=start_date, end_datetime=end_date)
        self.dates_picker_dialog.hide()
        self.hide()

    def remove_dates(self):
        """Remove the dates from the card list."""
        self.card_list.remove_dates()
        self.dates_picker_dialog.hide()
        self.hide()

    def open_duration_dialog(self) -> None:
        """Open the duration setter dialog."""
        if self.duration_setter_dialog is None:
            self.duration_setter_dialog = DurationSetterDialog(
                    self.card_list.duration,
                    parent=self.parent())
            self.duration_setter_dialog.save.connect(self.set_duration)
            self.duration_setter_dialog.remove.connect(self.remove_duration)
            self.card_list.duration_setter_dialog = (
                    self.duration_setter_dialog)
        self.duration_setter_dialog.init_from_duration_object(self.card_list)
        self.duration_setter_dialog.move(
                self.parent().mapFromGlobal(
                    self.duration_button.parent().mapToGlobal(
                        self.duration_button.geometry().bottomLeft())))
        if self.duration_setter_dialog.isVisible():
            self.duration_setter_dialog.hide()
        else:
            self.duration_setter_dialog.show()

    def set_duration(self) -> None:
        """Set the duration from the setter dialog."""
        self.card_list.set_duration(self.duration_setter_dialog.get_duration())
        self.hide_all()

    def open_convert_to_dialog(self) -> None:
        """Open the convert to dialog."""
        if self.convert_to_dialog is None:
            self.convert_to_dialog = ConvertToDialog(
                    "card_list", self.card_list, parent=self.parent(),
                    )
            self.convert_to_dialog.converted.connect(
                    self.hide_all)
        self.convert_to_dialog.setEnabled(
                self.card_list.can_be_converted())
        if self.convert_to_dialog.isVisible():
            self.convert_to_dialog.hide()
        else:
            self.convert_to_dialog.move(
                    self.parent().mapFromGlobal(
                        self.convert_button.parent().mapToGlobal(
                            self.convert_button.geometry().bottomLeft())))
            self.convert_to_dialog.show()

    def remove_duration(self) -> None:
        """Remove the duration."""
        self.card_list.remove_duration()
        self.hide_all()

    def hide_all(self) -> None:
        """Hide all dialogs."""
        self.hide()
        for dialog in self.subdialogs:
            if dialog is None:
                continue
            dialog.hide()
