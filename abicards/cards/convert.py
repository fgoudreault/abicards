"""Board widget conversion module."""
from typing import Any, Sequence

from PySide6.QtCore import QPoint, Qt, Signal
from PySide6.QtWidgets import (
        QComboBox, QFrame, QHBoxLayout, QLabel, QPushButton, QWidget,
        )

from ..custom_widgets import SimpleDialogWithHeader
from ..routines import get_main_widget, set_widget_font
from ..styles_constants import __BUTTON_HEIGHT_MEDIUM__


class ConvertibleObject:
    """A convertible object."""

    def __init__(self, *args, **kwargs) -> None:
        pass

    def can_be_converted(self) -> bool:
        """Return true if the object can be converted."""
        raise NotImplementedError


class ConvertToDialog(SimpleDialogWithHeader):
    """Dialog asking user how to convert given object."""

    convert_to_checklist_item = Signal()
    convert_to_checklist = Signal()
    convert_to_card = Signal()
    convert_to_card_list = Signal()
    convert_to_board = Signal()
    converted = Signal()

    def __init__(self, objecttype: str, obj: QWidget, *args, **kwargs):
        if objecttype not in (
                "board", "checklist_item", "checklist", "card", "card_list"):
            raise ValueError(objecttype)
        self.objecttype = objecttype
        self.object = obj
        self.convert_to_checklist_dialog = None
        self.convert_to_checklist_item_dialog = None
        self.convert_to_card_dialog = None
        self.convert_to_card_list_dialog = None
        super().__init__(
                *args,
                title="Convert to",
                objectName="dark_grey_dialogs",
                **kwargs)
        for signal in [
                self.convert_to_checklist_item,
                self.convert_to_checklist,
                self.convert_to_card,
                self.convert_to_card_list,
                self.convert_to_board,
                ]:
            signal.connect(self.converted)

    @property
    def subdialogs(self) -> Sequence:
        """The list of subdialogs."""
        return [self.convert_to_checklist_dialog,
                self.convert_to_checklist_item_dialog,
                self.convert_to_card_dialog,
                self.convert_to_card_list_dialog,
                ]

    def setup_ui(self) -> None:
        """Setup the convert to dialog."""
        super().setup_ui()
        self.setup_convert_buttons()
        self.adjustSize()

    def setup_convert_buttons(self) -> None:
        """Setup the convert to buttons."""
        self.board_button = None
        self.card_list_button = None
        self.card_button = None
        self.checklist_button = None
        self.checklist_item_button = None
        if self.objecttype == "checklist_item":
            self.add_checklist_button()
            self.add_card_button()
            self.add_card_list_button()
            self.add_board_button()
        elif self.objecttype == "checklist":
            self.add_checklist_item_button()
            self.add_card_button()
            self.add_card_list_button()
            self.add_board_button()
        elif self.objecttype == "card":
            self.add_checklist_item_button()
            self.add_checklist_button()
            self.add_card_list_button()
            self.add_board_button()
        elif self.objecttype == "card_list":
            self.add_checklist_item_button()
            self.add_checklist_button()
            self.add_card_button()
            self.add_board_button()
        elif self.objecttype == "board":
            self.add_checklist_item_button()
            self.add_checklist_button()
            self.add_card_button()
            self.add_card_list_button()
        else:
            raise NotImplementedError(self.objecttype)

    def add_checklist_item_button(self) -> None:
        """Add the convert to checklist item button."""
        self.checklist_item_button = QPushButton(
                parent=self, text="Checklist item",
                objectName="gray_btn_centered",
                )
        self.checklist_item_button.clicked.connect(
                self.open_convert_to_checklist_item_dialog)
        self.checklist_item_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.checklist_item_button)

    def add_checklist_button(self) -> None:
        """Add the convert to checklist button."""
        self.checklist_button = QPushButton(
                parent=self, text="Checklist",
                objectName="gray_btn_centered",
                )
        self.checklist_button.clicked.connect(
                self.open_convert_to_checklist_dialog)
        self.checklist_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.checklist_button)

    def add_card_button(self) -> None:
        """Add the convert to card button."""
        self.card_button = QPushButton(
                parent=self, text="Card",
                objectName="gray_btn_centered",
                )
        self.card_button.clicked.connect(
                self.open_convert_to_card_dialog)
        self.card_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.card_button)

    def add_card_list_button(self) -> None:
        """Add the convert to card list button."""
        self.card_list_button = QPushButton(
                parent=self, text="Card list",
                objectName="gray_btn_centered",
                )
        self.card_list_button.clicked.connect(
                self.open_convert_to_card_list_dialog)
        self.card_list_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.card_list_button)

    def add_board_button(self) -> None:
        """Add the convert to board button."""
        self.board_button = QPushButton(
                parent=self, text="Board",
                objectName="gray_btn_centered")
        self.board_button.clicked.connect(
                self._convert_to_board)
        self.board_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.board_button)

    def setEnabled(self, enabled: bool) -> None:
        """Enables or disables all buttons."""
        for button in [self.card_list_button, self.card_button,
                       self.checklist_button, self.checklist_item_button,
                       self.board_button,
                       ]:
            if button is None:
                continue
            button.setEnabled(enabled)

    def open_convert_to_checklist_item_dialog(self) -> None:
        """Convert obj to a checklist item."""
        if self.convert_to_checklist_item_dialog is None:
            self.convert_to_checklist_item_dialog = (
                    ConvertToChecklistItemDialog(
                        parent=self.parent(), obj=self.object,
                        objecttype=self.objecttype,
                        )
                    )
            self.convert_to_checklist_item_dialog.cancel.connect(self.hide)
            self.convert_to_checklist_item_dialog.convert.connect(self.hide)
            self.convert_to_checklist_item_dialog.convert.connect(
                    self._convert_to_checklist_item)
        self._toggle_dialog(self.convert_to_checklist_item_dialog)

    def open_convert_to_card_dialog(self) -> None:
        """Convert obj to a card."""
        if self.convert_to_card_dialog is None:
            self.convert_to_card_dialog = ConvertToCardDialog(
                    parent=self.parent(), obj=self.object,
                    objecttype=self.objecttype,
                    )
            self.convert_to_card_dialog.cancel.connect(self.hide)
            self.convert_to_card_dialog.convert.connect(self.hide)
            self.convert_to_card_dialog.convert.connect(
                    self._convert_to_card)
        self._toggle_dialog(self.convert_to_card_dialog)

    def open_convert_to_card_list_dialog(self) -> None:
        """Convert checklist item to a card list."""
        if self.convert_to_card_list_dialog is None:
            self.convert_to_card_list_dialog = ConvertToCardListDialog(
                    parent=self.parent(), obj=self.object,
                    objecttype=self.objecttype,
                    )
            self.convert_to_card_list_dialog.cancel.connect(self.hide)
            self.convert_to_card_list_dialog.convert.connect(self.hide)
            self.convert_to_card_list_dialog.convert.connect(
                    self._convert_to_card_list)
        self._toggle_dialog(self.convert_to_card_list_dialog)

    def _toggle_dialog(self, dialog) -> None:
        # match bottom position so that we don't have to care about the
        # parent to be large enough to display it all
        pos = self.geometry().bottomLeft()
        dialog.move(
                QPoint(pos.x() - dialog.width() - 3,
                       pos.y() - dialog.height()
                       ))
        if dialog.isVisible():
            dialog.hide()
        else:
            dialog.show()

    def open_convert_to_checklist_dialog(self) -> None:
        """Convert to a checklist."""
        if self.convert_to_checklist_dialog is None:
            self.convert_to_checklist_dialog = ConvertToChecklistDialog(
                    parent=self.parent(), obj=self.object,
                    objecttype=self.objecttype,
                    )
            self.convert_to_checklist_dialog.cancel.connect(self.hide)
            self.convert_to_checklist_dialog.convert.connect(self.hide)
            self.convert_to_checklist_dialog.convert.connect(
                    self._convert_to_checklist)
        self._toggle_dialog(self.convert_to_checklist_dialog)

    def _get_board_and_switch_to_it(self, dialog) -> None:
        board = dialog.get_selected_board()
        main = get_main_widget()
        if board is None:
            # Create a new board
            board = self._get_new_board()
        else:
            main.switch_to_board(board)
        return board

    def _get_new_board(self):
        """Create a new board and return it."""
        main = get_main_widget()
        main.add_board()
        # adding a board automatically switch to it
        return main.boards[-1]

    def _convert_to_board(self) -> None:
        """Convert item to a new board."""
        self.convert_to_board.emit()
        board = self._get_new_board()
        board.set_title(self.object.title)
        self.object.delete()

    def _convert_to_card_list(self) -> None:
        """Convert item to a card list."""
        board = self._get_board_and_switch_to_it(
                self.convert_to_card_list_dialog)
        board.add_card_list(load_data={"title": self.object.title})
        self.object.delete()
        self.convert_to_card_list.emit()

    def _get_card_list_from_board(self, board, dialog):
        card_list = dialog.get_selected_card_list()
        if card_list is None:
            # create new card list
            board.add_card_list()
            card_list = board.card_lists[-1]
        return card_list

    def _convert_to_card(self) -> None:
        """Convert item to a card."""
        board = self._get_board_and_switch_to_it(self.convert_to_card_dialog)
        card_list = self._get_card_list_from_board(
                board, self.convert_to_card_dialog)
        card_list.add_card(load_from={"title": self.object.title})
        self.object.delete()
        self.convert_to_card.emit()

    def _get_card_from_card_list(self, card_list, dialog):
        """Get the card from the card list based on dialog selection."""
        card = dialog.get_selected_card()
        if card is None:
            card_list.add_card()
            card = card_list.cards[-1]
        return card

    def _convert_to_checklist(self) -> None:
        """Convert item to a checklist."""
        self.convert_to_checklist.emit()
        board = self._get_board_and_switch_to_it(
                self.convert_to_checklist_dialog)
        card_list = self._get_card_list_from_board(
                board, self.convert_to_checklist_dialog)
        card = self._get_card_from_card_list(
                card_list, self.convert_to_checklist_dialog)
        card.add_checklist(load_data={"title": self.object.title})
        self.object.delete()
        card.toggle_full_edit()

    def _get_checklist_from_card(self, card, dialog):
        """Get the checklist object from the card."""
        checklist = dialog.get_selected_checklist()
        if checklist is None:
            card.add_checklist()
            checklist = card.checklists[-1]
        return checklist

    def _convert_to_checklist_item(self) -> None:
        """Convert item to a checklist item."""
        self.convert_to_checklist_item.emit()
        board = self._get_board_and_switch_to_it(
                self.convert_to_checklist_item_dialog)
        card_list = self._get_card_list_from_board(
                board, self.convert_to_checklist_item_dialog)
        card = self._get_card_from_card_list(
                card_list, self.convert_to_checklist_item_dialog)
        checklist = self._get_checklist_from_card(
                card, self.convert_to_checklist_item_dialog)
        checklist.add_checklist_item(load_data={"title": self.object.title})
        self.object.delete()
        card.toggle_full_edit()

    def hide(self, *args, **kwargs) -> Any:
        """Hide dialog and subdialogs."""
        for dialog in self.subdialogs:
            if dialog is None:
                continue
            dialog.hide()
        return super().hide(*args, **kwargs)


class ConvertToCardListDialog(SimpleDialogWithHeader):
    """Convert an object to a card list."""

    cancel = Signal()
    convert = Signal()

    def __init__(self, *args, obj=None, objecttype: str = None, **kwargs):
        self.object = obj
        self.objecttype = objecttype
        super().__init__(
                *args, title="Convert to card list",
                objectName="dark_grey_dialogs",
                **kwargs)
        self.convert.connect(self.hide)
        self.cancel.connect(self.hide)

    def setup_ui(self) -> None:
        """Setup the convert to card list dialog ui."""
        # simple UI which asks which board to convert to
        super().setup_ui()
        self.setup_contents()
        self.setup_buttons()
        self.adjustSize()

    def setup_contents(self) -> None:
        """Setup the contents of the dialog."""
        self.setup_board_selector()

    def setup_board_selector(self) -> None:
        """Setup the board list selection."""
        self.board_list_selection_label = QLabel(
                parent=self, text="Select board:")
        set_widget_font(
                self.board_list_selection_label, point_size=10,
                bold=True,
                )
        self.board_list_selection_label.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.board_list_selection_label)
        self.board_combobox = QComboBox(
                parent=self,
                )
        self.layout().addWidget(self.board_combobox)

    def setup_buttons(self) -> None:
        """Setup convert and cancel button."""
        self.buttons = QFrame(parent=self)
        self.buttons.setLayout(QHBoxLayout())
        self.layout().addWidget(self.buttons)
        self.convert_button = QPushButton(
                parent=self, text="&Convert", objectName="purple_btn",
                )
        self.buttons.layout().addWidget(self.convert_button)
        self.convert_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.convert_button.clicked.connect(self.convert)
        self.cancel_button = QPushButton(
                parent=self, text="Cancel", objectName="gray_btn",
                )
        self.buttons.layout().addWidget(self.cancel_button)
        self.cancel_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.cancel_button.clicked.connect(self.cancel)

    def update(self) -> None:
        """Update the board combobox using latest boards."""
        self.update_board_selector()

    def update_board_selector(self) -> None:
        """Update the board selector."""
        self.board_combobox.clear()
        boards = get_main_widget().boards
        self.board_selection_map = []
        for iboard, board in enumerate(boards):
            if self.objecttype == "board" and board is self.object:
                continue
            self.board_selection_map.append(iboard)
            self.board_combobox.addItem(board.title)
        if self.objecttype != "board":
            board_index = boards.index(self.object.board)
            self.board_combobox.setCurrentIndex(board_index)
        self.board_combobox.addItem("+ New board")

    def showEvent(self, *args, **kwargs) -> None:
        """Called when showing widget."""
        self.update()
        return super().showEvent(*args, **kwargs)

    def get_selected_board(self):
        """Get the selected board."""
        idx = self.board_combobox.currentIndex()
        boards = get_main_widget().boards
        if idx >= len(self.board_selection_map):
            return None
        return boards[self.board_selection_map[idx]]


class ConvertToCardDialog(ConvertToCardListDialog):
    """Convert to a card dialog."""

    def setup_contents(self) -> None:
        """Setup the contents of the dialog."""
        super().setup_contents()
        self.setup_card_list_selector()

    def setup_board_selector(self) -> None:
        """Setup the board selector."""
        super().setup_board_selector()
        self.board_combobox.activated.connect(self.update_card_list_selector)

    def setup_card_list_selector(self) -> None:
        """Setup the card list selector based on the board selection."""
        self.card_list_selection_label = QLabel(
                parent=self, text="Select card list:")
        set_widget_font(
                self.card_list_selection_label, point_size=10,
                bold=True,
                )
        self.card_list_selection_label.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.card_list_selection_label)
        self.card_list_combobox = QComboBox(
                parent=self,
                )
        self.layout().addWidget(self.card_list_combobox)

    def update(self) -> None:
        """Update the whole dialog."""
        super().update()
        self.update_card_list_selector()

    def update_card_list_selector(self) -> None:
        """Update the card list selector."""
        self.card_list_combobox.clear()
        board = self.get_selected_board()
        self.card_list_selection_map = []
        if board is not None:
            for icard_list, card_list in enumerate(board.card_lists):
                if self.objecttype == "card_list" and (
                        card_list is self.object):
                    continue
                self.card_list_selection_map.append(icard_list)
                self.card_list_combobox.addItem(
                    card_list.title)
            # if board is same as the one of the item,
            # set the default card list to the one which contains the object
            if self.objecttype not in ("board", "card_list"):
                if board is self.object.board:
                    card_list_index = board.card_lists.index(
                            self.object.card_list)
                    self.card_list_combobox.setCurrentIndex(card_list_index)
        self.card_list_combobox.addItem("+ New card list")

    def get_selected_card_list(self):
        """Get the selected card list."""
        board = self.get_selected_board()
        if board is None:
            return None
        idx = self.card_list_combobox.currentIndex()
        if idx >= len(self.card_list_selection_map):
            return None
        return board.card_lists[self.card_list_selection_map[idx]]


class ConvertToChecklistDialog(ConvertToCardDialog):
    """Convert to a checklist dialog."""

    def setup_contents(self) -> None:
        """Setup the dialog content."""
        super().setup_contents()
        self.setup_card_selector()

    def setup_card_list_selector(self) -> None:
        """Setup the card list selector."""
        super().setup_card_list_selector()
        self.card_list_combobox.activated.connect(self.update_card_selector)

    def setup_card_selector(self) -> None:
        """Setup the card selector."""
        self.card_selection_label = QLabel(
                parent=self, text="Select card:")
        set_widget_font(
                self.card_selection_label, point_size=10,
                bold=True,
                )
        self.card_selection_label.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.card_selection_label)
        self.card_combobox = QComboBox(
                parent=self,
                )
        self.layout().addWidget(self.card_combobox)

    def update(self) -> None:
        """Update the whole dialog."""
        super().update()
        self.update_card_selector()

    def update_card_list_selector(self) -> None:
        """Update the card list selector."""
        super().update_card_list_selector()
        self.update_card_selector()

    def update_card_selector(self) -> None:
        """Update the card selector."""
        self.card_combobox.clear()
        card_list = self.get_selected_card_list()
        self.card_selection_map = []
        if card_list is not None:
            for icard, card in enumerate(card_list.cards):
                if self.objecttype == "card" and card is self.object:
                    continue
                self.card_selection_map.append(icard)
                self.card_combobox.addItem(
                    card.title)
            # if card list is same as the one of the item,
            # set the default card to the one which contains the object
            if self.objecttype not in ("board", "card_list", "card"):
                if card_list is self.object.card_list:
                    card_index = card_list.cards.index(self.object.card)
                    self.card_combobox.setCurrentIndex(card_index)
        self.card_combobox.addItem("+ New card")

    def get_selected_card(self):
        """Get the selected card."""
        card_list = self.get_selected_card_list()
        if card_list is None:
            return None
        idx = self.card_combobox.currentIndex()
        if idx >= len(self.card_selection_map):
            return None
        return card_list.cards[self.card_selection_map[idx]]


class ConvertToChecklistItemDialog(ConvertToChecklistDialog):
    """Dialog when user selects convert item to a checklist item."""

    def setup_contents(self) -> None:
        """Setup dialog contents."""
        super().setup_contents()
        self.card_combobox.activated.connect(self.update_checklist_selector)
        self.setup_checklist_selector()

    def setup_checklist_selector(self) -> None:
        """Setup the checklist selector."""
        self.checklist_selection_label = QLabel(
                parent=self, text="Select checklist:")
        set_widget_font(
                self.checklist_selection_label, point_size=10, bold=True)
        self.checklist_selection_label.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.checklist_selection_label)
        self.checklist_combobox = QComboBox(parent=self)
        self.layout().addWidget(self.checklist_combobox)

    def update(self) -> None:
        """Update the whole dialog."""
        super().update()
        self.update_checklist_selector()

    def update_card_selector(self) -> None:
        """Update the card selector."""
        super().update_card_selector()
        self.update_checklist_selector()

    def update_checklist_selector(self) -> None:
        """Update the checklist selector."""
        self.checklist_combobox.clear()
        card = self.get_selected_card()
        self.checklist_selection_map = []
        if card is not None:
            for ichecklist, checklist in enumerate(card.checklists):
                if self.objecttype == "checklist" and checklist is self.object:
                    continue
                self.checklist_combobox.addItem(checklist.title)
                self.checklist_selection_map.append(ichecklist)
        self.checklist_combobox.addItem("+ New checklist")

    def get_selected_checklist(self):
        """Get the selected checklist."""
        card = self.get_selected_card()
        if card is None:
            return None
        idx = self.checklist_combobox.currentIndex()
        if idx >= len(self.checklist_selection_map):
            return None
        # using a map cause we skipped the index of the checklist
        # if we are converting a checklist to a checklist item
        return card.checklists[self.checklist_selection_map[idx]]
