"""The full card edit dialog module."""
import logging
from typing import Any, Sequence

from PySide6.QtCore import QEvent, QObject, QPoint, Qt
from PySide6.QtGui import QShortcut
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel,
        QPushButton, QScrollArea, QVBoxLayout,
        QWidget,
        )

from .checklist import (
        AddChecklistDialog, Checklist, FullEditDialogChecklistField,
        )
from .checklist_item import FullEditCardDialogChecklistItem
from .color import CardColorPickerDialog
from .convert import ConvertToDialog
from .dates import DatesPickerDialog, FullEditDialogDatesField
from .description import FullEditDialogDescriptionField
from .duration import DurationIndicator, DurationSetterDialog
from .title import FullCardEditDialogTitleTextEdit
from ..board.board_item import BaseBoardItem
from ..custom_widgets import HeightForWidthLabel, SmallCloseButton
from ..routines import (
        build_qss, get_icon_pixmap, get_loglevel, parse_qss,
        )
from ..styles_constants import (
        __FULL_CARD_EDIT_DIALOG_BUTTONS_COLUMN_WIDTH__,
        __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__,
        __LABEL_HEIGHT_SMALL__, __PALETTE__,
        )


class FullCardEditDialog(BaseBoardItem):
    """The full card edit dialog with tons of options."""

    has_collision = False
    has_draggable_subelements = True
    has_editable_title = False
    close_when_clicking_outside = True
    save_moves = False

    def __init__(self, *args, card=None, **kwargs) -> None:
        self.card = card
        self.board = card.board
        super().__init__(
                *args, objectName="full_card_edit_dialog", **kwargs)

    def __repr__(self) -> str:
        """The full edit card dialog representation."""
        return f"< FullCardEditDialog: title = '{self.card.title}' >"

    @property
    def title(self) -> str:
        """The title as stored in the text edit."""
        ct = self.card_title_and_close_button_widget.card_title
        return ct.text_edit.toPlainText()

    @property
    def checklist_fields(self) -> Sequence[FullEditDialogChecklistField]:
        """The list of checklists fields."""
        ntfb = self.non_title_fields_and_buttons
        ntf = ntfb.non_title_fields
        return ntf.checklist_fields

    def draggable_subelement(self) -> FullEditCardDialogChecklistItem | None:
        """Check if we start dragging a subelement."""
        for checklist in self.checklist_fields:
            for item in checklist.checklist_item_widgets:
                if item.underMouse():
                    return item
            if checklist.underMouse():
                return checklist
        return None

    def set_color(self, color: str) -> None:
        """Sets the border color to the given color."""
        ss = self.styleSheet()
        name, attrs = parse_qss(ss)
        attrs["border"] = f"3px solid {color}"
        newss = build_qss(name, attrs)
        self.setStyleSheet(newss)

    def setup_ui(self):
        """Setup the UI."""
        self.setLayout(QVBoxLayout())
        self.setStyleSheet(
                f"""QFrame#full_card_edit_dialog {{
                        background-color: {__PALETTE__['dark_gray']};
                        border-radius: 10px;
                }}""",
                )
        self.set_color(self.card.color)
        # take min and set it square with min value
        self.setFixedSize(900, 900)
        # header
        self.card_title_and_close_button_widget = (
                CardTitleAndCloseButtonForFullEditDialog(
                    self.card,
                    parent=self))
        self.layout().addWidget(
                self.card_title_and_close_button_widget)
        # scroll area
        self.scroll_area = QScrollArea(parent=self)
        self.layout().addWidget(self.scroll_area)
        # the following widget actually contains the content
        # we do this such that we can use a scroll area
        self.full_edit_card_widget = QFrame()
        self.full_edit_card_widget.setLayout(QVBoxLayout())
        self.full_edit_card_widget.setMaximumWidth(self.width())
        self.scroll_area.setWidget(self.full_edit_card_widget)
        self.scroll_area.setHorizontalScrollBarPolicy(
                Qt.ScrollBarAlwaysOff)
        self.scroll_area.setFrameShape(QFrame.NoFrame)
        self.scroll_area.setVerticalScrollBarPolicy(
                Qt.ScrollBarAlwaysOff)
        self.scroll_area.setWidgetResizable(True)
        # these margins make sure we see the border in case it is colored
        self.layout().setContentsMargins(5, 5, 5, 5)
        self.setContentsMargins(0, 0, 0, 0)

        self.non_title_fields_and_buttons = (
                FullEditDialogNonTitleFieldsAndButtons(
                    self,
                    parent=self.full_edit_card_widget)
                )
        self.full_edit_card_widget.layout().addWidget(
                self.non_title_fields_and_buttons)
        self.full_edit_card_widget.layout().addStretch()
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(
                  self.discard_editing)

    @property
    def non_title_fields(self):
        """The non title fields."""
        return self.non_title_fields_and_buttons.non_title_fields

    @property
    def non_title_buttons(self):
        """The non title buttons."""
        return self.non_title_fields_and_buttons.non_title_buttons

    @property
    def subdialogs(self):
        """The list of all subdialogs."""
        dialogs = [self.add_checklist_dialog]
        dialogs += self.edit_checklist_dialogs
        dialogs += self.edit_checklist_item_dialogs
        ntb = self.non_title_fields_and_buttons.non_title_buttons
        dialogs += ntb.subdialogs
        return dialogs

    @property
    def dates_picker_dialog(self) -> DatesPickerDialog:
        """The dates picker dialog."""
        ntb = self.non_title_fields_and_buttons.non_title_buttons
        return ntb.dates_picker_dialog

    @property
    def color_picker_dialog(self):
        """The color picker subdialog."""
        ntb = self.non_title_fields_and_buttons.non_title_buttons
        return ntb.color_picker_dialog

    @property
    def edit_checklist_item_dialogs(self):
        """The list of the checklist item edit dialogs."""
        ntf = self.non_title_fields_and_buttons.non_title_fields
        lst = []
        for checklist_field in ntf.checklist_fields:
            for itemfield in checklist_field.checklist_item_widgets:
                # lst.append(itemfield.title_label.edit_item_dialog)
                lst += itemfield.subdialogs
        return lst

    @property
    def edit_checklist_dialogs(self) -> Sequence:
        """The list of the checklist edit dialogs."""
        ntf = self.non_title_fields_and_buttons.non_title_fields
        lst = []
        for checklist_field in ntf.checklist_fields:
            edit_dialog = checklist_field.edit_dialog
            lst.append(edit_dialog)
            if edit_dialog is None:
                continue
            lst += edit_dialog.subdialogs
        return lst

    @property
    def add_checklist_dialog(self):
        """The add checklist dialog."""
        ntf = self.non_title_fields_and_buttons
        btns = ntf.non_title_buttons
        return btns.add_checklist_dialog

    def subdialogs_opened(self) -> bool:
        """Return True if some subdialogs are opened."""
        for dialog in self.subdialogs:
            if dialog is None:
                continue
            if dialog.isVisible():
                return True
        return False

    def close_subdialogs(self) -> None:
        """Close all subdialogs."""
        for dialog in self.subdialogs:
            if dialog is None:
                continue
            if dialog.underMouse():
                continue
            if hasattr(dialog, "subdialogs"):
                # don't close this dialog if one of its subdialog is undermouse
                any_under_mouse = False
                for subdialog in dialog.subdialogs:
                    if subdialog is None:
                        continue
                    if subdialog.underMouse():
                        any_under_mouse = True
                        break
                if not any_under_mouse:
                    for subdialog in dialog.subdialogs:
                        if subdialog is None:
                            continue
                        subdialog.hide()
                    dialog.hide()
            else:
                dialog.hide()
        ntf = self.non_title_fields_and_buttons.non_title_fields
        ntf.close_subdialogs()

    def mousePressEvent(self, *args, **kwargs) -> Any:
        """Close subdialogs if necessary."""
        self.close_subdialogs()
        return super().mousePressEvent(*args, **kwargs)

    def hideEvent(self, *args, **kwargs) -> Any:
        """Hide quick edit menu."""
        self.escape_shortcut.setEnabled(False)
        self.board.unhighlight()
        return super().hideEvent(*args, **kwargs)

    def showEvent(self, *args, **kwargs) -> Any:
        """Show quick edit menu."""
        self.escape_shortcut.setEnabled(True)
        # highlight self
        self.board.highlight_widgets(self)
        return super().showEvent(*args, **kwargs)

    def resizeEvent(self, event: QEvent) -> Any:
        """Called when resizing."""
        # place this widget in the middle
        w, h = self.width(), self.height()
        totx, toty = self.parent().width(), self.parent().height()
        self.move((totx - w) / 2, (toty - h) / 2)

    def discard_editing(self):
        """Discard all editing and close window."""
        ct = self.card_title_and_close_button_widget.card_title
        if not ct.text_edit.isReadOnly():
            ct.discard_editing()
            return
        self.card.cancel_full_edit()

    def add_checklist_widget(self, *args, **kwargs) -> None:
        """Add a checklist widget to dialog."""
        self.non_title_fields_and_buttons.add_checklist_widget(
                *args, **kwargs)

    def remove_checklist_widget(self, *args, **kwargs) -> None:
        """Remove a checklist widget from dialog."""
        self.non_title_fields_and_buttons.remove_checklist_widget(
                *args, **kwargs)


class FullEditDialogNonTitleFieldsAndButtons(QWidget):
    """The widget for the other non-title fields and buttons."""

    def __init__(
            self, full_edit_card_dialog: FullCardEditDialog, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.full_edit_card_dialog = full_edit_card_dialog
        self.card = self.full_edit_card_dialog.card
        self._logger = logging.getLogger(
                "FullEditDialogNonTitleFieldsAndButtons")
        self._logger.setLevel(get_loglevel())
        self.setup_ui()

    def setup_ui(self):
        """Setup ui."""
        self.setLayout(QHBoxLayout())
        self.non_title_fields = FullEditDialogNonTitleFields(
                self.full_edit_card_dialog, parent=self)
        self.layout().addWidget(self.non_title_fields)
        self.non_title_buttons = FullEditDialogNonTitleButtons(
                self.full_edit_card_dialog,
                parent=self)
        self.non_title_buttons.setFixedWidth(
                __FULL_CARD_EDIT_DIALOG_BUTTONS_COLUMN_WIDTH__)
        self.layout().addWidget(self.non_title_buttons)

    def add_checklist(self, *args, **kwargs):
        """Add a checklist. Also add information to the card."""
        self.non_title_fields.add_checklist(*args, **kwargs)

    def add_checklist_widget(self, *args, **kwargs) -> None:
        """Add a checklist widget."""
        self.non_title_fields.add_checklist_widget(*args, **kwargs)

    def remove_checklist_widget(self, *args, **kwargs) -> None:
        """Remove a checklist widget from dialog."""
        self.non_title_fields.remove_checklist_widget(*args, **kwargs)


class FullEditDialogNonTitleFields(QFrame):
    """The non title fields like description and checklists."""

    def __init__(
            self, full_edit_card_dialog: FullCardEditDialog,
            *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.card = self.parent().card
        self.full_edit_card_dialog = full_edit_card_dialog
        self.checklist_fields = []
        self._logger = self.parent()._logger
        self.setup_ui()

    def setup_ui(self):
        """Setup ui."""
        self.setContentsMargins(0, 0, 0, 0)
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        # dates and duration indicators
        self.indicators_field = QFrame(parent=self)
        self.indicators_field.setLayout(QHBoxLayout())
        self.layout().addWidget(self.indicators_field)
        self.dates_field = FullEditDialogDatesField(
                parent=self)
        self.indicators_field.layout().addWidget(self.dates_field)
        if self.card.dates is None:
            self.dates_field.hide()
        self.duration_field = DurationIndicator(
                self.card.duration, parent=self)
        self.duration_field.setFixedHeight(30)
        if not self.card.has_duration():
            self.duration_field.hide()
        self.indicators_field.layout().addWidget(self.duration_field)
        self.indicators_field.layout().addStretch()
        # description and checklists
        self.description_field = FullEditDialogDescriptionField(
                parent=self)
        self.layout().addWidget(self.description_field)
        self.layout().addStretch()
        for checklist in self.card.checklists:
            self.add_checklist_widget(checklist=checklist)

    def close_subdialogs(self) -> None:
        """Close subdialogs."""
        for checklist_field in self.checklist_fields:
            checklist_field.close_subdialogs()

    def add_checklist(
            self, *args, checklist: Checklist = None, **kwargs) -> None:
        """Add a checklist to the ui."""
        if checklist is None:
            self.card.add_checklist(*args, **kwargs)
        else:
            self._logger.info(
                    f"Adding checklist named: '{checklist.title}'.")

    def add_checklist_widget(self, checklist) -> None:
        """Add a checklist widget to dialog."""
        checklist_field = FullEditDialogChecklistField(
                self.full_edit_card_dialog,
                checklist, parent=self,
                )
        # insert before the final stretch
        self.layout().insertWidget(self.layout().count() - 1, checklist_field)
        self.checklist_fields.append(checklist_field)

    def delete_checklist(self, checklist_widget):
        """Delete a checklist from the ui."""
        self.card.delete_checklist(checklist_widget.checklist)
        self.remove_checklist_widget(checklist_widget)

    def remove_checklist_widget(self, checklist_widget) -> None:
        """Remove the checklist widget."""
        if checklist_widget not in self.checklist_fields:
            # already non-existant
            return
        self.layout().removeWidget(checklist_widget)
        self.checklist_fields.remove(checklist_widget)
        checklist_widget.hide()
        checklist_widget.destroy()


class FullEditDialogNonTitleButtons(QFrame):
    """The column widget for the buttons."""

    def __init__(self, full_edit_card_dialog, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.full_edit_card_dialog = full_edit_card_dialog
        self.board = self.full_edit_card_dialog.board
        self.card = self.full_edit_card_dialog.card
        self.card.card_updated.connect(self.update_duration_indicator)
        self.add_checklist_dialog = None
        self.color_picker_dialog = None
        self.dates_picker_dialog = None
        self.duration_setter_dialog = None
        self.convert_to_dialog = None
        self.setup_ui()

    @property
    def subdialogs(self) -> None:
        """The non-title buttons related subdialogs."""
        dialogs = self.main_subdialogs
        if self.convert_to_dialog is not None:
            dialogs += self.convert_to_dialog.subdialogs
        return dialogs

    @property
    def main_subdialogs(self) -> Sequence:
        """The main non-title button related subdialogs."""
        return [
                self.add_checklist_dialog,
                self.color_picker_dialog,
                self.dates_picker_dialog,
                self.duration_setter_dialog,
                self.convert_to_dialog,
                ]

    def setup_ui(self):
        """Setup the ui."""
        self.setLayout(QVBoxLayout())

        # button column title ('Add to card')
        self.add_to_card_label = QLabel(
                "Add to card", objectName="small_transparent_label",
                parent=self)
        self.add_to_card_label.setFixedHeight(__LABEL_HEIGHT_SMALL__)
        self.layout().addWidget(self.add_to_card_label)
        # checklist button
        self.checklist_button = QPushButton(
                "  Checklist", objectName="gray_btn", parent=self,
                )
        self.checklist_button.setIcon(get_icon_pixmap("checklist_color"))
        self.checklist_button.clicked.connect(self.open_checklist_dialog)
        self.layout().addWidget(self.checklist_button)
        # color button
        self.color_button = QPushButton(
                "  Color", objectName="gray_btn", parent=self,
                )
        self.color_button.clicked.connect(self.open_card_color_dialog)
        self.color_button.setIcon(get_icon_pixmap("color"))
        self.layout().addWidget(self.color_button)

        # Dates button
        self.dates_button = QPushButton(
                "  Dates", objectName="gray_btn", parent=self,
                )
        self.dates_button.setIcon(get_icon_pixmap("dates_color"))
        self.layout().addWidget(self.dates_button)
        self.dates_button.clicked.connect(self.open_card_dates_dialog)
        # Duration
        self.duration_button = QPushButton(
                "  Duration", objectName="gray_btn", parent=self)
        self.duration_button.setIcon(get_icon_pixmap("duration_color"))
        self.layout().addWidget(self.duration_button)
        self.duration_button.clicked.connect(self.open_card_duration_dialog)

        # convert
        self.convert_button = QPushButton(
                "        Convert", objectName="gray_btn", parent=self)
        self.layout().addWidget(self.convert_button)
        self.convert_button.clicked.connect(self.open_convert_to_dialog)

        # stretch
        self.layout().addStretch()

    def open_card_color_dialog(self) -> None:
        """Open card color dialog."""
        if self.color_picker_dialog is None:
            self.color_picker_dialog = CardColorPickerDialog(
                    self.card, parent=self.board)
            self.color_picker_dialog.set_color.connect(self.set_color)
        # this dialog a top widget so that it appears on top of everything
        self._place_dialog_under_widget(
                self.color_button, self.color_picker_dialog)

    def open_card_dates_dialog(
            self, clicked: bool = False, under_widget: QObject = None) -> None:
        """Open the card dates dialog."""
        if self.dates_picker_dialog is None:
            self.dates_picker_dialog = DatesPickerDialog(
                    self.card.dates, parent=self.board)
            self.dates_picker_dialog.set_dates.connect(
                    self.set_dates)
            self.dates_picker_dialog.remove_dates.connect(
                    self.remove_dates)
        if self.dates_picker_dialog.isVisible():
            # it was already open so close it
            self.dates_picker_dialog.hide()
            return
        if under_widget is None:
            under_widget = self.dates_button
        self._place_dialog_under_widget(
                under_widget, self.dates_picker_dialog)
        if self.dates_picker_dialog.isVisible():
            self.full_edit_card_dialog.escape_shortcut.setEnabled(False)

    def open_card_duration_dialog(self) -> None:
        """Open the card duration dialog."""
        if self.duration_setter_dialog is None:
            self.duration_setter_dialog = DurationSetterDialog(
                    self.card.duration, parent=self.board)
            self.duration_setter_dialog.save.connect(
                    self.set_duration)
            self.duration_setter_dialog.remove.connect(
                    self.remove_duration)
        self.duration_setter_dialog.enable_lineedits(
                not self.card.any_children_has_duration())
        if self.card.has_duration():
            self.duration_setter_dialog.duration = self.card.duration
            self.duration_setter_dialog.update_from_duration()
        else:
            self.duration_setter_dialog.update_from_duration(
                    self.card.total_children_duration())
        if self.duration_setter_dialog.isVisible():
            # it was already open so close it
            self.duration_setter_dialog.hide()
            return
        self._place_dialog_under_widget(
                self.duration_button, self.duration_setter_dialog)
        if self.duration_setter_dialog.isVisible():
            self.full_edit_card_dialog.escape_shortcut.setEnabled(False)

    def _place_dialog_under_widget(
            self, widget: QObject, dialog: QObject) -> None:
        if dialog.isVisible():
            dialog.hide()
            return
        abs_pos = widget.mapToGlobal(QPoint(0, 0))
        rel_pos = self.board.mapFromGlobal(abs_pos)
        # now put it below and align centers horizontally
        xshift = -(dialog.width() - widget.width()) / 2
        yshift = widget.height() + 5
        dialog.move(rel_pos.x() + xshift, rel_pos.y() + yshift)
        dialog.show()
        dialog.raise_()
        # close other dialogs
        for d in self.main_subdialogs:
            if d is dialog:
                continue
            if d is None:
                continue
            d.hide()

    def open_checklist_dialog(self) -> None:
        """Add a check list to the card."""
        # open a dialog window that asks for a title.
        if self.add_checklist_dialog is None:
            self.add_checklist_dialog = AddChecklistDialog(
                self.full_edit_card_dialog,
                parent=self.board)
            self.add_checklist_dialog.add.connect(self.add_checklist)
        self._place_dialog_under_widget(
                self.checklist_button, self.add_checklist_dialog)
        self.add_checklist_dialog.focus_on_lineedit()

    def open_convert_to_dialog(self) -> None:
        """Open the convert to dialog."""
        if self.convert_to_dialog is None:
            self.convert_to_dialog = ConvertToDialog(
                    "card", self.card, parent=self.board)
            self.convert_to_dialog.converted.connect(
                    self.hide_after_convert)
        self.convert_to_dialog.setEnabled(self.card.can_be_converted())
        self._place_dialog_under_widget(
                self.convert_button, self.convert_to_dialog)

    def add_checklist(self):
        """Add a checklist from the title given in the dialog."""
        self.parent().add_checklist(
                title=self.add_checklist_dialog.get_checklist_title())
        self.add_checklist_dialog.hide()

    def set_color(self) -> None:
        """Set the card color."""
        color = self.color_picker_dialog.get_color()
        self.card.set_color(color)
        self.full_edit_card_dialog.set_color(color)

    def set_dates(self) -> None:
        """Set the card due dates."""
        start_date = self.dates_picker_dialog.get_start_date()
        end_date = self.dates_picker_dialog.get_end_datetime()
        self.card.set_dates(start_date=start_date, end_datetime=end_date)
        self.dates_picker_dialog.hide()
        # enable full edit dialog escape shortcut
        self.full_edit_card_dialog.escape_shortcut.setEnabled(True)
        self.parent().non_title_fields.dates_field.show()

    def remove_dates(self) -> None:
        """Remove the card due dates."""
        self.card.remove_dates()
        self.parent().non_title_fields.dates_field.hide()
        self.dates_picker_dialog.hide()
        self.full_edit_card_dialog.escape_shortcut.setEnabled(True)

    def set_duration(self) -> None:
        """Set the card duration."""
        self.card.set_duration(self.duration_setter_dialog.get_duration())
        self.duration_setter_dialog.hide()
        self.update_duration_indicator()

    def update_duration_indicator(self) -> None:
        """Update the duration indicator."""
        indicator = self.parent().non_title_fields.duration_field
        indicator.duration = self.card.duration
        if self.card.has_duration():
            # show the indicator
            indicator.update()
            # indicator.adjustSize()
            return
        indicator.hide()

    def remove_duration(self) -> None:
        """Remove the card duration."""
        self.card.remove_duration()
        self.duration_setter_dialog.hide()
        self.parent().non_title_fields.duration_field.hide()

    def hide_after_convert(self) -> None:
        """Hide full edit dialog after convertion."""
        self.convert_to_dialog.hide()
        self.full_edit_card_dialog.hide()


class CardTitleAndCloseButtonForFullEditDialog(QFrame):
    """The widget container for the card title and close button."""

    def __init__(self, card, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.card = card
        self.setup_ui()

    def setup_ui(self):
        """Setup ui specific to the card title and the close button."""
        # leave first column for some icons TODO
        # add a place holder widget for first column in meantime
        self.setLayout(QHBoxLayout())
        self.layout().setSpacing(0)
        self.setContentsMargins(10, 0, 0, 0)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.setup_icon()
        self.card_title = FullCardEditDialogTitleTextEdit(parent=self)
        self.layout().addWidget(self.card_title)

        self.card_title.textChanged.connect(self.on_text_changed)
        self.card_title.setPlainText(self.card.title)

        # add close button inside another layout such that it aligns on top
        self.close_button_container = QFrame(parent=self)
        self.close_button_container.setLayout(QVBoxLayout())
        self.layout().addWidget(self.close_button_container)
        self.close_button = SmallCloseButton(parent=self)
        self.close_button.clicked.connect(self.card.full_editing_done)
        self.close_button_container.layout().addWidget(
                self.close_button, Qt.AlignTop)

    def setup_icon(self) -> None:
        """Setup the card edit dialog icon."""
        self.icon_container = QFrame(parent=self)
        self.icon_container.setLayout(QVBoxLayout())
        self.icon_container.setContentsMargins(20, 20, 0, 0)
        self.icon_container.layout().setContentsMargins(0, 0, 0, 0)
        self.icon_container.setFixedWidth(
                __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__)
        self.layout().addWidget(self.icon_container)
        self.icon_label = HeightForWidthLabel(
                height_factor=1.09,
                parent=self.icon_container)
        self.icon_pixmap = get_icon_pixmap("card")
        self.icon_label.setPixmap(self.icon_pixmap)
        self.icon_label.setScaledContents(True)
        self.icon_label.setFixedWidth(15)
        self.icon_container.layout().addWidget(self.icon_label)
        self.icon_container.layout().addStretch()

    def on_text_changed(self) -> None:
        """Called when text changed in title text edit."""
        self.setFixedHeight(self.card_title.text_edit.height() + 15)

    def change_card_title(self, title: str) -> None:
        """Change the card title."""
        self.card.set_title(title)
