"""The due dates card attribute submodule."""
from datetime import date, datetime, timedelta
from typing import Any, Sequence

from PySide6.QtCore import QDate, QObject, Qt, Signal
from PySide6.QtGui import QColor, QMouseEvent, QPalette, QShortcut
from PySide6.QtWidgets import (
        QCalendarWidget, QCheckBox, QFrame, QHBoxLayout, QLabel,
        QPushButton, QVBoxLayout, QWidget,
        )

from ..constants import __MONTHS__
from ..custom_widgets import (
        FocusAwareLineEdit, HeightForWidthLabel,
        RangeCalendar, SmallCloseButton,
        )
from ..mixins import BaseIndicator
from ..routines import (
        date_to_datetime, date_to_qdate, get_icon_pixmap,
        qdate_to_date, set_widget_font, unsaved_changes,
        )
from ..styles_constants import (
        __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__,
        __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__,
        __LABEL_HEIGHT_SMALL__,
        __PALETTE__,
        )


def datetime_to_date(dt: datetime) -> date:
    """Convert a datetime to a date object."""
    return date(dt.year, dt.month, dt.day)


class DatableObject:
    """A base class for objects with dates."""

    has_dates_indicator = False
    dates_changed = Signal()
    completed_changed = Signal()

    def __init__(self, *args, **kwargs):
        # need **kwargs as Qt passes them around for some reason...
        self._dates = None
        self.dates_indicator = None

    def setup_ui(self) -> None:
        """Setup datable part of widget UI."""
        pass

    def setup_dates_indicator(self) -> None:
        """Setup the dates indicator."""
        self.dates_indicator = DatesIndicator(
            self.dates, parent=self)
        self.dates_indicator.clicked.connect(
                self.dates_indicator_clicked)
        self.dates_indicator.hidden.connect(self.indicator_hidden)
        self.dates_indicator.shown.connect(self.indicator_shown)
        self.setup_indicator(self.dates_indicator)

    @property
    def dates(self):
        """The due dates for this object."""
        return self._dates

    @unsaved_changes
    def set_dates(
            self, start_date: date = None, end_datetime: datetime = None,
            load_from: dict[str, Any] = None,
            unsaved_changes: bool = True) -> None:
        """Set the due dates for this card."""
        if self._dates is None:
            self._dates = Dates(parent=self)
            self.setup_dates_indicator()
            self._dates.dates_changed.connect(self.dates_changed)
            self.dates.completed_changed.connect(
                    self.completed_changed)
            if self.has_dates_indicator:
                if self.dates_indicator is not None:
                    self.dates_indicator.dates = self._dates
                self.dates.dates_changed.connect(self.update_dates_indicator)
                self.dates.completed_changed.connect(
                        self.update_dates_indicator)
        self.dates.set_start_date(
                start_date, unsaved_changes=unsaved_changes)
        self.dates.set_end_datetime(
                end_datetime, unsaved_changes=unsaved_changes)
        if load_from is not None:
            self.dates.load_json_data(load_from)
        if self.has_dates_indicator:
            self.update_dates_indicator()
        if unsaved_changes:
            self._logger.info(
                    f"Setting dates '{self.dates}' to {self}.")
            return True

    @unsaved_changes
    def remove_dates(self) -> None:
        """Remove the due dates."""
        if self.dates is None:
            return
        # force this to false so that it does not reappear
        self.dates.set_fixed_by_children_dates(False)
        self.set_dates(
                start_date=None, end_datetime=None,
                )
        self.dates_changed.emit()
        self._logger.info(f"Removing dates from {self}.")
        if self.has_dates_indicator:
            self.update_dates_indicator()
        return True

    def get_indicators(self) -> Sequence[QObject]:
        """Get the dates indicators."""
        return [self.dates_indicator]

    def update_indicators(self) -> None:
        """Update the indicators."""
        self.update_dates_indicator()

    def update_dates_indicator(self) -> None:
        """Update the dates indicator."""
        if self.dates_indicator is None:
            return
        if self.dates is None:
            self.dates_indicator.hide()
            return
        self.dates_indicator.update()

    def dates_indicator_clicked(self):
        """The dates indicator has been clicked."""
        # invert selection of checkbox.
        self.dates_indicator.checkbox.setChecked(
                not self.dates_indicator.checkbox.isChecked())
        self.dates.set_completed(self.dates_indicator.checkbox.isChecked())
        self.update_dates_indicator()

    def get_children_dates(self):
        """Get the children dates."""
        raise NotImplementedError(
                f"{self.__class__} not implemented.")

    def get_dates_picker_dialog(self):
        """Get the dates picker dialog."""
        raise NotImplementedError(
                f"{self.__class__} not implemented.")

    def update_dates(self, **kwargs) -> None:
        """Called to update the dates based on the children dates."""
        if self.dates is not None:
            # fixed by children dates is True by default
            if not self.dates.fixed_by_children_dates:
                return  # do not update
        children_dates = self.get_children_dates()
        # if any([d is not None for d in children_dates]):
        #     self._logger.info(
        #             f"Updating dates of {self} based on children dates.")
        starts, ends = [], []
        for dates in children_dates:
            if dates is None:
                continue
            sd = dates.start_date
            ed = dates.end_datetime
            if sd is not None:
                starts.append(date_to_datetime(sd))
            if ed is not None:
                ends.append(ed)
        if not starts and not ends:
            # no dates for children
            self.set_dates(
                    start_date=None, end_datetime=None,
                    unsaved_changes=False,
                    )
            self.update_dates_picker_dialog()
            self.dates_indicator.hide()
            return
        if starts and not ends:
            sort = list(sorted(starts))
            self.set_dates(
                    start_date=datetime_to_date(sort[0]),
                    unsaved_changes=False,
                    **kwargs)
            self.update_dates_picker_dialog()
            return
        if ends and not starts:
            sort = list(sorted(ends))
            self.set_dates(
                    end_datetime=sort[0],
                    unsaved_changes=False,
                    **kwargs)
            self.update_dates_picker_dialog()
            return
        sort = list(sorted(starts + ends))
        self.set_dates(
                start_date=datetime_to_date(sort[0]),
                end_datetime=sort[-1],
                unsaved_changes=False,
                **kwargs,
                )
        self.update_dates_picker_dialog()

    def update_dates_picker_dialog(self) -> None:
        """Update the dates picker dialog."""
        # update the dates picker dialog as well
        picker_dialog = self.get_dates_picker_dialog()
        if picker_dialog is not None:
            picker_dialog.update_from_dates()

    def showEvent(self, *args, **kwargs) -> None:
        """Called when showing the widget."""
        self.update_dates_indicator()
        self.update_dates()

    def get_json_data(self) -> dict[str, Any]:
        """Get json data for dates part of object."""
        data = {"dates": None}
        if self.dates is not None:
            data["dates"] = self.dates.get_json_data()
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load data for dates part of object."""
        if "dates" not in data:
            return
        if data["dates"] is None:
            return
        self.set_dates(load_from=data["dates"], unsaved_changes=False)
        if self.has_dates_indicator:
            self.update_dates_indicator()


class Dates(QObject):
    """Class that represent the due dates for a card."""

    completed_changed = Signal()
    dates_changed = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._logger = self.parent()._logger
        self.card = self.parent()
        self._start_date = None
        self._end_date = None
        self._completed = False
        self.fixed_by_children_dates = True

    def __repr__(self) -> str:
        """The string representation of a Dates object."""
        s = "< Dates: "
        if self.start_date is not None:
            y, m = self.start_date.year, str(self.start_date.month).zfill(2)
            d = str(self.start_date.day).zfill(2)
            s += f"{y}/{m}/{d} "
            if self.end_datetime is not None:
                s += "- "
        if self.end_datetime is not None:
            y = self.end_datetime.year
            m = str(self.end_datetime.month).zfill(2)
            d = str(self.end_datetime.day).zfill(2)
            h = str(self.end_datetime.hour).zfill(2)
            mm = str(self.end_datetime.minute).zfill(2)
            s += f"{y}/{m}/{d} at {h}:{mm} "
        if self.start_date is None and self.end_datetime is None:
            s += "None "
        s += ">"
        return s

    def get_json_data(self) -> dict[str, Any]:
        """Compute the json dict to store data on file."""
        data = {"start": None, "end": None, "completed": self.completed}
        if self.start_date is not None:
            data["start"] = self.start_date.isoformat()
        if self.end_datetime is not None:
            data["end"] = self.end_datetime.isoformat()
        data["fixed_by_children_dates"] = self.fixed_by_children_dates
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load the data from a json dict."""
        if "fixed_by_children_dates" in data:
            self.set_fixed_by_children_dates(
                    data["fixed_by_children_dates"], unsaved_changes=False)
        if data["start"] is not None:
            self.set_start_date(
                    date.fromisoformat(data["start"]),
                    unsaved_changes=False)
        if data["end"] is not None:
            self.set_end_datetime(
                    datetime.fromisoformat(data["end"]),
                    unsaved_changes=False)
        if "completed" in data:
            self.set_completed(data["completed"], unsaved_changes=False)

    @unsaved_changes
    def set_fixed_by_children_dates(
            self, fixed: bool, unsaved_changes: bool = True) -> None:
        """Set the 'fixed_by_children_dates' attribute.

        Parameters
        ----------
        fixed: bool
            The new value for the attribute.
        unsaved_changes: bool
            If True and the new value is different than the old one,
            unsaved changes are noted to user.
        """
        if fixed is self.fixed_by_children_dates:
            return
        self.fixed_by_children_dates = fixed
        if unsaved_changes:
            self._logger.info(f"'fixed_by_children_dates' set to '{fixed}'.")
            return True

    def is_date_within(self, dt: date) -> bool:
        """Return true if given date is within dates.

        If only start or end date is enabled, return true if same.
        Otherwise return False.
        """
        if self.start_date is None and self.end_datetime is None:
            return False
        if self.start_date is None and self.end_datetime is not None:
            return datetime_to_date(self.end_datetime) == dt
        if self.start_date is not None and self.end_datetime is None:
            return self.start_date == dt
        # both are not None
        return self.start_date <= dt <= datetime_to_date(self.end_datetime)

    @property
    def completed(self) -> bool:
        """True if due date was completed."""
        return self._completed

    @unsaved_changes
    def set_completed(
            self, completed: bool, unsaved_changes: bool = True) -> None:
        """Set if due date is completed or not."""
        if completed is self.completed:
            return
        self._completed = completed
        self.completed_changed.emit()
        if unsaved_changes:
            self._logger.info(f"Setting {self} to completed = {completed}")
            return True

    @property
    def start_date(self) -> date | None:
        """The start date of the card."""
        return self._start_date

    @unsaved_changes
    def set_start_date(
            self, dt: date, unsaved_changes: bool = True,
            ) -> None:
        """Set the start date."""
        if self._start_date == dt:
            return
        self._start_date = dt
        self.dates_changed.emit()
        if unsaved_changes:
            self._logger.info(f"Setting start date to {dt}")
            return True

    @property
    def end_datetime(self) -> datetime | None:
        """The end date and time of the card."""
        return self._end_date

    @unsaved_changes
    def set_end_datetime(
            self, dt: datetime, unsaved_changes: bool = True,
            ) -> None:
        """Set the end date and time."""
        if self._end_date == dt:
            return
        self._end_date = dt
        self.dates_changed.emit()
        if unsaved_changes:
            self._logger.info(f"Setting end datetime to {dt}.")
            return True


class DatesPickerDialog(QFrame):
    """The dates picker dialog."""

    set_dates = Signal()
    remove_dates = Signal()
    fixed_by_children_changed = Signal()

    def __init__(
            self, dates, *args, enable_fixed_by_children_chkbox=True,
            **kwargs,
            ) -> None:
        super().__init__(*args, objectName="black_dialog", **kwargs)
        self.dates = dates
        self.last_valid_start_date = None
        self.last_valid_end_date = None  # datetime.today()
        self.enable_fixed_by_children_chkbox = enable_fixed_by_children_chkbox
        if self.dates is not None:
            self.last_valid_start_date = self.dates.start_date
            self.last_valid_end_date = self.dates.end_datetime
        self.setup_ui()

    def setup_ui(self) -> None:
        """Setup the dates picker dialog UI."""
        self.setLayout(QVBoxLayout())
        self.setFixedWidth(450)
        self.setFixedHeight(600)
        self.setup_shortcuts()
        self.setup_header()
        self.setup_calendar()
        self.setup_start_date_widgets()
        self.setup_end_date_widgets()
        self.setup_fixed_by_children_chkbox()
        self.setup_buttons()
        self.setup_initial_state()

    def setup_fixed_by_children_chkbox(self) -> None:
        """Setup the fixed by children chkbox."""
        self.fixed_by_children_line = QFrame(parent=self)
        self.fixed_by_children_line.setLayout(QHBoxLayout())
        self.fixed_by_children_chkbox = QCheckBox(
                parent=self.fixed_by_children_line)
        self.fixed_by_children_line.layout().addWidget(
                self.fixed_by_children_chkbox)
        self.fixed_by_children_description = QLabel(
                parent=self.fixed_by_children_line,
                text="Dates fixed by children",
                )
        self.fixed_by_children_description.setAlignment(
                Qt.AlignLeft)
        self.fixed_by_children_line.layout().addWidget(
                self.fixed_by_children_description)
        self.fixed_by_children_chkbox.checkStateChanged.connect(
                self.fixed_by_children_chkbox_clicked)
        if self.dates is not None:
            self.fixed_by_children_chkbox.setChecked(
                self.dates.fixed_by_children_dates)
        else:
            # true by default
            self.fixed_by_children_chkbox.setChecked(True)
        self.fixed_by_children_line.layout().addStretch()
        if self.enable_fixed_by_children_chkbox:
            self.layout().addWidget(self.fixed_by_children_line)
        else:
            # disable it and put it to unchecked state
            self.fixed_by_children_chkbox.setChecked(False)
            self.fixed_by_children_chkbox.setEnabled(False)

    def fixed_by_children_chkbox_clicked(self, *args) -> None:
        """The fixed by children chkbox has been clicked."""
        fixed = self.fixed_by_children_chkbox.isChecked()
        if self.dates is not None:
            self.dates.set_fixed_by_children_dates(fixed)
        self.fixed_by_children_changed.emit()
        # disable or all lines
        self.end_date_lineedit.setEnabled(not fixed)
        self.end_date_chkbox.setEnabled(not fixed)
        self.end_time_lineedit.setEnabled(not fixed)
        self.start_date_lineedit.setEnabled(not fixed)
        self.start_date_chkbox.setEnabled(not fixed)
        self.calendar.setEnabled(not fixed)

    def setup_end_date_widgets(self):
        """Setup the end date line."""
        self.end_date_title = QLabel(
                "Due date", parent=self, objectName="small_transparent_label",
                )
        self.end_date_title.setFixedHeight(__LABEL_HEIGHT_SMALL__)
        self.end_date_title.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.end_date_title)
        # checkbox + lineedits
        self.end_date_line = QWidget(parent=self)
        self.end_date_line.setLayout(QHBoxLayout())
        self.layout().addWidget(self.end_date_line)
        self.end_date_chkbox = QCheckBox(parent=self.end_date_line)
        self.end_date_chkbox.checkStateChanged.connect(self.toggle_end_date)
        self.end_date_chkbox.setFixedWidth(30)
        self.end_date_line.layout().addWidget(self.end_date_chkbox)
        self.end_date_lineedit = FocusAwareLineEdit(parent=self.end_date_line)
        self.end_date_lineedit.setEnabled(True)
        self.end_date_lineedit.textEdited.connect(self.end_date_changed)
        self.end_date_lineedit.focus_changed.connect(
                self.focus_changed_on_lineedits)
        self.end_date_lineedit.setPlaceholderText("DD/MM/YYYY")
        self.end_date_lineedit.setFixedWidth(125)
        self.end_date_lineedit.setMaxLength(10)
        self.end_date_line.layout().addWidget(
                self.end_date_lineedit, alignment=Qt.AlignLeft)
        self.end_time_lineedit = FocusAwareLineEdit(parent=self.end_date_line)
        self.end_time_lineedit.setFixedWidth(90)
        self.end_time_lineedit.setMaxLength(5)
        self.end_time_lineedit.setPlaceholderText("HH:MM")
        self.end_date_line.layout().addWidget(
                self.end_time_lineedit, alignment=Qt.AlignLeft)

    def setup_initial_state(self) -> None:
        """Setup the dates picker initial state."""
        if self.last_valid_end_date is not None:
            self.end_date_chkbox.setChecked(True)
            self.set_end_date_lineedits_from_datetime(
                    self.last_valid_end_date)
            self.calendar.setSelectedDate(
                    self.datetime_to_qdate(self.last_valid_end_date))
        if self.dates is not None:
            if self.dates.start_date is not None:
                self.start_date_chkbox.setChecked(True)
                self.set_start_date_lineedit_from_date(
                        self.dates.start_date)

    def toggle_end_date(self) -> None:
        """Called when the end date checkbox is clicked."""
        self._update_calendar_selection_mode()
        if self.end_date_chkbox.isChecked():
            # enable the line edit unless fixed by children
            if not self.fixed_by_children_chkbox.isChecked():
                self.end_date_lineedit.setEnabled(True)
                self.end_time_lineedit.setEnabled(True)
            if self.last_valid_end_date is not None:
                self.set_end_date_lineedits_from_datetime(
                        self.last_valid_end_date)
            else:
                selected = self.qdate_to_datetime(
                        self.calendar.selectedDate())
                self.last_valid_end_date = selected
                self.set_end_date_lineedits_from_datetime(selected)
            if not self.fixed_by_children_chkbox.isChecked():
                self.end_date_lineedit.setFocus()
            self.calendar.moving_start = False
            if self.start_date_chkbox.isChecked():
                self.calendar.setSelectedDate(
                        date_to_qdate(self.get_end_date()))
        else:
            # disable lineedit
            self.end_date_lineedit.setEnabled(False)
            self.end_time_lineedit.setEnabled(False)
            # remove text from it
            dt = self.get_end_datetime()
            if dt is not None:
                self.last_valid_end_date = dt
            self.end_date_lineedit.setText("")
            self.end_time_lineedit.setText("")
            self.calendar.moving_start = True
            if self.start_date_chkbox.isChecked():
                self.calendar.setSelectedDate(
                        date_to_qdate(self.get_start_date()))

    def end_date_changed(self, *args) -> None:
        """The end date changed inside the lineedit."""
        # edit the calendar
        if not self.end_date_chkbox.isChecked():
            # nothing to do
            return
        dt = self.get_end_date()
        if dt is None:
            return
        self.calendar.setSelectedDate(date_to_qdate(dt))

    def start_date_changed(self, *args) -> None:
        """The start date changed inside the corresponding lineedit."""
        # edit the calendar
        if not self.start_date_chkbox.isChecked():
            return
        dt = self.get_start_date()
        if dt is None:
            return
        self.calendar.setSelectedDate(date_to_qdate(dt))

    def _update_calendar_selection_mode(self):
        if self.end_date_chkbox.isChecked() and (
                self.start_date_chkbox.isChecked()):
            self.calendar.setSelectionMode(2)
        else:
            self.calendar.setSelectionMode(QCalendarWidget.SingleSelection)

    def set_end_date_lineedits_from_datetime(self, dt: datetime) -> None:
        """Set the end date lineedits from a datetime object."""
        self.set_end_date_lineedit_from_date(date(dt.year, dt.month, dt.day))
        h = str(dt.hour).zfill(2)
        mm = str(dt.minute).zfill(2)
        self.end_time_lineedit.setText(f"{h}:{mm}")

    def set_end_date_lineedit_from_date(self, dt: date) -> None:
        """Set the end date lineedit from a date object."""
        d = str(dt.day).zfill(2)
        m = str(dt.month).zfill(2)
        y = dt.year
        self.end_date_lineedit.setText(f"{d}/{m}/{y}")

    def setup_start_date_widgets(self):
        """Setup the start dates line."""
        # start date title
        self.start_date_title = QLabel(
                "Start date", parent=self,
                objectName="small_transparent_label",
                )
        self.start_date_title.setFixedHeight(__LABEL_HEIGHT_SMALL__)
        self.start_date_title.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.start_date_title)
        # checkbox + lineedit
        self.start_date_line = QWidget(parent=self)
        self.layout().addWidget(self.start_date_line)
        self.start_date_line.setLayout(QHBoxLayout())
        self.start_date_chkbox = QCheckBox(parent=self.start_date_line)
        self.start_date_chkbox.checkStateChanged.connect(
                self.toggle_start_date)
        self.start_date_chkbox.setFixedWidth(30)
        self.start_date_line.layout().addWidget(self.start_date_chkbox)
        self.start_date_lineedit = FocusAwareLineEdit(
                parent=self.start_date_line)
        self.start_date_lineedit.textEdited.connect(self.start_date_changed)
        self.start_date_lineedit.setEnabled(False)
        self.start_date_lineedit.focus_changed.connect(
                self.focus_changed_on_lineedits)
        self.start_date_lineedit.setPlaceholderText("DD/MM/YYYY")
        self.start_date_lineedit.setFixedWidth(125)
        self.start_date_lineedit.setMaxLength(10)
        self.start_date_line.layout().addWidget(
                self.start_date_lineedit, alignment=Qt.AlignLeft)

    def focus_changed_on_lineedits(self) -> None:
        """Called when focus changed on line edits."""
        if self.start_date_lineedit.hasFocus():
            # highlight the label text
            set_widget_font(
                    self.start_date_title, color=__PALETTE__["purple"])
            self.calendar.moving_start = True
            self.calendar.setSelectedDate(
                    date_to_qdate(self.get_start_date()))
        elif not self.calendar.hasFocus():
            # start date lost focus and not focusing on calendar
            set_widget_font(
                    self.start_date_title, color=__PALETTE__["white"])
            self.calendar.moving_start = False
        if self.end_date_lineedit.hasFocus() or (
                self.end_time_lineedit.hasFocus()):
            # highlight the label text
            set_widget_font(
                    self.end_date_title, color=__PALETTE__["purple"])
            self.calendar.moving_start = False
            self.calendar.setSelectedDate(
                    date_to_qdate(self.get_end_date()))
        elif not self.calendar.hasFocus():
            set_widget_font(self.end_date_title, color=__PALETTE__["white"])
            self.calendar.moving_start = True

    def toggle_start_date(self) -> None:
        """Called when the start date checkbox is clicked."""
        self._update_calendar_selection_mode()
        if self.start_date_chkbox.isChecked():
            # enable the line edit unless fixed by children
            if not self.fixed_by_children_chkbox.isChecked():
                self.start_date_lineedit.setEnabled(True)
            if self.last_valid_start_date is not None:
                self.set_start_date_lineedit_from_date(
                        self.last_valid_start_date)
            else:
                today = date.today()
                if today == self.get_end_date():
                    # move end date to tomorrow
                    self.set_end_date_lineedit_from_date(
                            today + timedelta(1))
                    self.end_date_changed()
                self.set_start_date_lineedit_from_date(today)
                self.calendar.moving_start = True
                self.start_date_changed()
            if not self.fixed_by_children_chkbox.isChecked():
                self.start_date_lineedit.setFocus()
            self.calendar.setSelectedDate(
                    date_to_qdate(self.get_start_date()))
        else:
            # disable lineedit
            self.start_date_lineedit.setEnabled(False)
            # remove text from it
            dt = self.get_start_date()
            if dt is not None:
                self.last_valid_start_date = dt
            self.start_date_lineedit.setText("")
            self.calendar.moving_start = False
            if self.end_date_chkbox.isChecked():
                self.calendar.setSelectedDate(
                        date_to_qdate(self.get_end_date()))

    def set_start_date_lineedit_from_date(self, date: datetime) -> None:
        """Set the start date lineedit from a datetime object."""
        d = str(date.day).zfill(2)
        m = str(date.month).zfill(2)
        y = date.year
        self.start_date_lineedit.setText(f"{d}/{m}/{y}")

    def setup_shortcuts(self):
        """Setup the keyboard shortcuts."""
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self)
        self.enter_shortcut.activated.connect(self.set_dates)
        self.return_shortcut = QShortcut(Qt.Key_Return, self)
        self.return_shortcut.activated.connect(self.set_dates)
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(self.hide)

    def setup_calendar(self) -> None:
        """Setup the dialog calendar."""
        self.calendar = RangeCalendar(parent=self)
        self.calendar.selectionChanged.connect(self.selected_date_changed)
        self.layout().addWidget(self.calendar)

    def selected_date_changed(self) -> None:
        """The selected date was changed by user."""
        start = self.start_date_chkbox.isChecked()
        end = self.end_date_chkbox.isChecked()
        if start and not end:
            # set the start date
            self.set_start_date_lineedit_from_date(
                    qdate_to_date(self.calendar.selectedDate()))
        elif end and not start:
            # set the end date
            hh, mm = self.get_end_time()
            if hh is None:
                now = datetime.now()
                hh, mm = now.hour, now.minute
            self.set_end_date_lineedits_from_datetime(
                    self.qdate_to_datetime(
                        self.calendar.selectedDate(), hour=hh, minute=mm))
        if start and end:
            if self.calendar.moving_start:
                # we're moving the start date
                self.set_start_date_lineedit_from_date(
                        qdate_to_date(self.calendar.selectedDate()))
            else:
                # set the end date
                hh, mm = self.get_end_time()
                if hh is None:
                    now = datetime.now()
                    hh, mm = now.hour, now.minute
                self.set_end_date_lineedits_from_datetime(
                        self.qdate_to_datetime(
                            self.calendar.selectedDate(), hour=hh, minute=mm))

    def qdate_to_datetime(self, qdate: QDate, **kwargs) -> datetime:
        """Convert a qdate and other time variables to a datetime object."""
        return datetime(qdate.year(), qdate.month(), qdate.day(), **kwargs)

    def datetime_to_qdate(self, dt: datetime) -> QDate:
        """Convert a datetime object to a QDate object."""
        return QDate(dt.year, dt.month, dt.day)

    def setup_buttons(self) -> None:
        """Setup dialog buttons."""
        self.save_button = QPushButton(
                parent=self, text="Save", objectName="purple_btn",
                )
        self.save_button.setFixedHeight(
                __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__)
        self.layout().addWidget(self.save_button)
        self.save_button.clicked.connect(self.set_dates)
        self.remove_button = QPushButton(
                parent=self, text="Remove", objectName="transparent_btn",
                )
        self.remove_button.setFixedHeight(
                __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__)
        self.layout().addWidget(self.remove_button)
        self.remove_button.clicked.connect(self.remove_dates)

    def setup_header(self):
        """Setup the dialog header."""
        self.header = QFrame(parent=self)
        self.header.setLayout(QHBoxLayout())
        self.layout().addWidget(self.header, alignment=Qt.AlignTop)

        self.title_label = QLabel(
                parent=self.header, text="Dates",
                )
        set_widget_font(self.title_label, bold=True)
        self.title_label.setFixedHeight(__LABEL_HEIGHT_SMALL__)
        self.title_label.setAlignment(Qt.AlignLeft)
        self.header.layout().addWidget(self.title_label)
        # today button
        self.today_button = QPushButton(
                text="today", parent=self.header, objectName="transparent_btn")
        self.today_button.setFixedWidth(75)
        self.header.layout().addWidget(self.today_button, Qt.AlignRight)
        self.today_button.clicked.connect(self.select_today)
        # close button
        self.close_button = SmallCloseButton(parent=self.header)
        self.header.layout().addWidget(
                self.close_button, alignment=Qt.AlignRight)
        self.close_button.clicked.connect(self.hide)

    def select_today(self) -> None:
        """Select today's date in the calendar."""
        dt = datetime.now()
        today = QDate(dt.year, dt.month, dt.day)
        self.calendar.setSelectedDate(today)

    def get_start_date(self) -> date | None:
        """Get the start date.

        Returns
        -------
        date: A date object representing the start date.
        None: If the text contained in the correspondint lineedit
        is not valid.
        """
        string = self.start_date_lineedit.text()
        y, m, d = self._parse_date_string(string)
        if y is None:
            return None
        return date(y, m, d)

    def _parse_date_string(self, string: str) -> tuple[int | None]:
        """Parse a date string DD/MM/YYYY."""
        if not string:
            return None, None, None
        split = string.split("/")
        if len(split) != 3:
            return None, None, None
        try:
            return (
                int(split[2]),  # year
                int(split[1]),  # month
                int(split[0]),  # day
                )
        except Exception:
            # somehow unable to parse the string
            return None, None, None

    def _parse_time_string(self, string: str) -> tuple[int | None]:
        """Parse a time string HH:MM."""
        if not string:
            return None, None
        split = string.split(":")
        try:
            return (int(split[0]), int(split[1]))
        except Exception:
            # cannot parse the string
            return None, None

    def get_end_date(self) -> date:
        """Get the end date."""
        string = self.end_date_lineedit.text()
        y, m, d = self._parse_date_string(string)
        if y is None:
            return None
        return date(y, m, d)

    def get_end_datetime(self) -> datetime:
        """Get the end datetime."""
        date = self.get_end_date()
        if date is None:
            return None
        hh, mm = self.get_end_time()
        if hh is None:
            return None
        return datetime(date.year, date.month, date.day, hour=hh, minute=mm)

    def get_end_time(self) -> tuple[int | None]:
        """Get the end time."""
        string = self.end_time_lineedit.text()
        hh, mm = self._parse_time_string(string)
        if hh is None:
            return None, None
        return hh, mm

    def hideEvent(self, *args, **kwargs) -> None:
        """Called when hiding widget."""
        for shortcut in [
                self.enter_shortcut, self.return_shortcut,
                self.escape_shortcut]:
            shortcut.setEnabled(False)
        return super().hideEvent(*args, **kwargs)

    def showEvent(self, *args, **kwargs) -> None:
        """Called when showing widget."""
        # disable full edit dialog escape shortcut
        for shortcut in [
                self.enter_shortcut, self.return_shortcut,
                self.escape_shortcut]:
            shortcut.setEnabled(True)
        # set last valid start date in case it changed on dates object
        self.update_from_dates()
        return super().showEvent(*args, **kwargs)

    def update_from_dates(self) -> None:
        """Update the dates picker dialog using Dates object."""
        if self.dates is None:
            return
        self.fixed_by_children_chkbox.setChecked(
                self.dates.fixed_by_children_dates)
        if self.dates.start_date is not None:
            self.last_valid_start_date = self.dates.start_date
            self.start_date_chkbox.setChecked(True)
            self.set_start_date_lineedit_from_date(
                    self.dates.start_date)
            if not self.dates.fixed_by_children_dates:
                self.start_date_lineedit.setFocus()
        if self.dates.end_datetime is not None:
            self.last_valid_end_date = self.dates.end_datetime
            self.end_date_chkbox.setChecked(True)
            self.set_end_date_lineedits_from_datetime(
                    self.dates.end_datetime)
            if not self.dates.fixed_by_children_dates:
                self.end_date_lineedit.setFocus()


class DatesButton(QFrame):
    """The dates button."""

    clicked = Signal()

    def __init__(self, *args, text: str = "", **kwargs):
        super().__init__(*args, objectName="gray_btn", **kwargs)
        self.setup_ui()
        self.setText(text)

    def setup_ui(self) -> None:
        """Setup the dates button UI."""
        # basically a label and a status tag
        self.setLayout(QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.text_label = QLabel(parent=self)
        self.layout().addWidget(self.text_label)
        self.status_tag = QLabel(parent=self)
        self.layout().addWidget(self.status_tag)
        self.status_tag.hide()

    def setText(self, text: str) -> None:
        """Set the label text."""
        self.text_label.setText(text)

    def set_due_soon(self) -> None:
        """Set the status tag to 'due soon'."""
        self.status_tag.setText("Due soon")
        self.set_status_color("light_brown")
        self.status_tag.show()

    def set_overdue(self) -> None:
        """Set the status tag to 'overdue'."""
        self.status_tag.setText("Overdue")
        self.set_status_color("dark_red")
        self.status_tag.show()

    def set_status_color(self, color: str) -> None:
        """Set the status tag color."""
        self.status_tag.setStyleSheet(
                f"""QLabel {{
                        background-color: {__PALETTE__[color]};
                        color: {__PALETTE__['white']};
                        border-radius: 8px;
                        padding: 2px;
                    }}""",
                )

    def set_completed(self) -> None:
        """Set the status tag to 'completed'."""
        self.status_tag.setText("Completed")
        self.set_status_color("green")
        self.status_tag.show()

    def hide_status_tag(self) -> None:
        """Hides the status tag."""
        self.status_tag.hide()

    def mousePressEvent(self, event: QMouseEvent) -> Any:
        """Called when the mouse is pressed inside widget."""
        if event.buttons() == Qt.LeftButton:
            self.clicked.emit()
            return
        return super().mousePressEvent(event)


class FullEditDialogDatesField(QFrame):
    """The dates field for the full edit dialog."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.card = self.parent().card
        self.setup_ui()
        self.card.card_updated.connect(
                self.update_button_and_label_text)

    def setup_ui(self):
        """Setup the dates field UI."""
        self.setLayout(QHBoxLayout())
        # for now, put an empty label as icon
        self.icon_label = QFrame(parent=self)
        self.icon_label.setFixedWidth(
                __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__)
        self.layout().addWidget(self.icon_label)
        self.non_icon_widget = QFrame(parent=self)
        self.layout().addWidget(self.non_icon_widget)
        self.non_icon_widget.setLayout(QVBoxLayout())
        self.title_label = QLabel(parent=self.non_icon_widget, text="Dates")
        self.title_label.setFixedHeight(__LABEL_HEIGHT_SMALL__)
        self.non_icon_widget.layout().addWidget(self.title_label)
        # the dates button which, when clicked, makes the dialog appear
        self.dates_line = QWidget(parent=self.non_icon_widget)
        self.dates_line.setLayout(QHBoxLayout())
        self.non_icon_widget.layout().addWidget(self.dates_line)

        self.completed_chkbox = QCheckBox(parent=self.dates_line)
        if self.card.dates is not None:
            self.completed_chkbox.setChecked(self.card.dates.completed)
        self.completed_chkbox.checkStateChanged.connect(
                self.set_completed)
        self.completed_chkbox.setFixedWidth(20)
        self.dates_line.layout().addWidget(self.completed_chkbox)
        self.dates_button = DatesButton(
                parent=self.dates_line, text="")
        # self.dates_button.setFixedHeight(
        #         __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__)
        self.dates_button.clicked.connect(self.open_dates_dialog)
        self.dates_line.layout().addWidget(
                self.dates_button, alignment=Qt.AlignLeft)

    def open_dates_dialog(self) -> None:
        """Open the dates picker dialog."""
        ntb = self.parent().parent().parent().non_title_buttons
        ntb.open_card_dates_dialog(under_widget=self.dates_button)

    # implementing both show and show event so that date is
    # updated when saving on calendar or when showing the first time
    def showEvent(self, *args, **kwargs) -> Any:
        """Show the dates field."""
        # update the button and label text
        self.update_button_and_label_text()
        return super().showEvent(*args, **kwargs)

    def show(self, *args, update: bool = True, **kwargs) -> Any:
        """Show the dates field."""
        # update the button and label text
        if update:
            self.update_button_and_label_text()
        return super().show(*args, **kwargs)

    def set_completed(self, *args, **kwargs) -> None:
        """Called when the completed checkbox is clicked."""
        self.card.dates.set_completed(self.completed_chkbox.isChecked())
        self.update_button_and_label_text()

    def update_button_and_label_text(self) -> None:
        """Update the button and the label text according to the dates."""
        dates = self.card.dates
        if dates.start_date is None and dates.end_datetime is None:
            self.hide()
            return
        self.completed_chkbox.setChecked(dates.completed)
        self.show(update=False)
        if dates.start_date is None and dates.end_datetime is not None:
            # only end date
            self.title_label.setText("Due date")
            datestr = self._get_friendly_date_text(dates.end_datetime)
            timestr = self._get_time_text(dates.end_datetime)
            self._set_status_tag()
            self.dates_button.setText(datestr + " " + timestr)
            return
        if dates.start_date is not None and dates.end_datetime is None:
            # only start date
            self.title_label.setText("Start date")
            datestr = self._get_friendly_date_text(dates.start_date)
            self.dates_button.setText(datestr)
            return
        # both start and end date is not None
        self.title_label.setText("Dates")
        enddatestr = self._get_friendly_date_text(dates.end_datetime)
        endtimestr = self._get_time_text(dates.end_datetime)
        startdatestr = self._get_friendly_date_text(dates.start_date)
        self._set_status_tag()
        self.dates_button.setText(
                startdatestr + "-" + enddatestr + " " + endtimestr)

    def _set_status_tag(self):
        if self.card.dates.completed:
            self.dates_button.set_completed()
        else:
            now = datetime.now()
            delta = self.card.dates.end_datetime - now
            seconds = delta.total_seconds()
            if seconds < 86400 and seconds >= 0:
                self.dates_button.set_due_soon()
            elif seconds < 0:
                self.dates_button.set_overdue()
            else:
                self.dates_button.hide_status_tag()

    def _get_time_text(self, dt: datetime) -> str:
        """Return the time as string."""
        return f"at {str(dt.hour).zfill(2)}:{str(dt.minute).zfill(2)}"

    def _get_friendly_date_text(self, dt: date | datetime) -> str:
        """Return a friendly text for the given date or datetime object."""
        # check if date is yesterday
        today = datetime.today()
        if today.year == dt.year:
            if today.month == dt.month:
                if today.day == dt.day + 1:
                    return "yesterday"
                if today.day == dt.day:
                    return "today"
                if today.day == dt.day - 1:
                    return "tomorrow"
            return f"{__MONTHS__[dt.month - 1]} {dt.day}"
        return f"{__MONTHS__[dt.month - 1]} {dt.day}, {dt.year}"


__HOVER_COLORS__ = {
        "dark_red": "red",
        "medium_dark_gray": "gray",
        "brown": "light_brown",
        "green": "light_green",
        }
__NON_HOVER_COLORS__ = {
        "red": "dark_red",
        "gray": "medium_dark_gray",
        "light_brown": "brown",
        "light_green": "green",
        }


class DatesIndicator(BaseIndicator):
    """The indicator representing the due dates."""

    clicked = Signal()
    entered = Signal()
    left = Signal()

    def __init__(self, dates: Dates, *args, text: str = "", **kwargs):
        self._current_color = "medium_dark_gray"  # default
        self.dates = dates
        self.clickable = False
        self.white_icon = get_icon_pixmap("dates")
        self.red_icon = get_icon_pixmap("dates_color")
        super().__init__(*args, **kwargs)
        self.setText(text)

    def setup_text_label(self) -> None:
        """Setup the indicator text label and checkbox."""
        # add checkbox on left of text label
        self.checkbox = QCheckBox(parent=self, objectName="indicator_checkbox")
        self.checkbox.checkStateChanged.connect(self.checkbox_clicked)
        # self.checkbox.setStyleSheet(
        #         f"""QCheckbox#indicator_checkbox {{
        #                 color: __PALETTE__['dark_gray'];
        #                 border: 1px solid __PALETTE__['gray'];
        #                 border-radius: 1px;
        #             }}""")
        self.checkbox.hide()
        self.layout().addWidget(self.checkbox)
        # icon label
        self.icon_label = HeightForWidthLabel(
                height_factor=1.09,
                parent=self)
        self.icon_label.setScaledContents(True)
        self.icon_label.setPixmap(self.red_icon)
        self.icon_label.setFixedWidth(15)
        self.icon_label.show()
        self.layout().addWidget(self.icon_label)
        super().setup_text_label()
        self.set_normal()

    def set_color(self, *args, **kwargs) -> None:
        """Set indicator color."""
        super().set_color(*args, **kwargs)
        # for some reason, checkboxes can only be customized this way...
        palette = self.checkbox.palette()
        palette.setColor(QPalette.Window, QColor(__PALETTE__["gray"]))
        palette.setColor(QPalette.Base, QColor(__PALETTE__["dark_gray"]))
        self.checkbox.setPalette(palette)

    def set_due_soon(self) -> None:
        """Set the status tag to 'due soon'."""
        self._current_color = "brown"
        self.set_color(__PALETTE__[self._current_color])
        self.icon_label.setPixmap(self.white_icon)
        if self.underMouse():
            self.set_hover_style()
        else:
            self.set_original_style()

    def set_overdue(self) -> None:
        """Set the status tag to 'overdue'."""
        self._current_color = "dark_red"
        self.set_color(__PALETTE__[self._current_color])
        self.icon_label.setPixmap(self.white_icon)
        if self.underMouse():
            self.set_hover_style()
        else:
            self.set_original_style()

    def set_completed(self) -> None:
        """Set the status tag to 'completed'."""
        self._current_color = "green"
        self.set_color(__PALETTE__[self._current_color])
        self.checkbox.setChecked(True)
        self.icon_label.setPixmap(self.red_icon)
        if self.underMouse():
            self.set_hover_style()
        else:
            self.set_original_style()

    def set_normal(self) -> None:
        """Set the normal status."""
        self._current_color = "medium_dark_gray"
        self.set_color(__PALETTE__[self._current_color])
        self.icon_label.setPixmap(self.red_icon)
        if self.underMouse():
            self.set_hover_style()
        else:
            self.set_original_style()

    def mousePressEvent(self, event: QMouseEvent) -> Any:
        """Called when the mouse is pressed inside widget."""
        if event.buttons() == Qt.LeftButton and self.clickable:
            self.clicked.emit()
        return super().mousePressEvent(event)

    def checkbox_clicked(self, *args) -> None:
        """Called when clicking on checkbox."""
        self.dates.set_completed(
                self.checkbox.isChecked())
        self.update()

    def enterEvent(self, event) -> Any:
        """Called when mouse enters the widget."""
        self.set_hover_style()
        self.entered.emit()

    def set_hover_style(self) -> None:
        """Set the hovered style sheet."""
        if self._current_color in __HOVER_COLORS__:
            # departing from a non-hover state
            self._current_color = __HOVER_COLORS__[self._current_color]
        self.set_color(__PALETTE__[self._current_color])
        self.checkbox.show()
        self.icon_label.hide()

    def leaveEvent(self, event) -> Any:
        """Called when mouse leaves the widget."""
        if self._current_color not in __NON_HOVER_COLORS__:
            return
        self.set_original_style()
        self.left.emit()

    def set_original_style(self) -> None:
        """Set the original style sheet."""
        if self._current_color not in __HOVER_COLORS__:
            # we're departing from a hover state
            self._current_color = __NON_HOVER_COLORS__[self._current_color]
        self.set_color(__PALETTE__[self._current_color])
        if not self.checkbox.isChecked():
            self.checkbox.hide()
            self.icon_label.show()
        else:
            self.checkbox.show()
            self.icon_label.hide()

    def update(self) -> None:
        """Update the dates indicator."""
        if self.dates is None:
            return
        if self.dates.completed is not (
                self.checkbox.isChecked()):
            # match the completed status with checkbox status
            self.checkbox.setChecked(self.dates.completed)
            return  # avoid infinite recursion
        if self.dates.end_datetime is not None or (
                self.dates.start_date is not None):
            self.show()
        else:
            # both dates are none
            self.hide()
            return
        today = date.today()
        text = ""
        if self.dates.start_date is not None:
            if self.dates.end_datetime is None:
                self.clickable = False
                delta = (self.dates.start_date - today).total_seconds()
                if delta < 0:
                    prefix = "Started "
                else:
                    prefix = "Starts "
                text += prefix
            text += (f"{__MONTHS__[self.dates.start_date.month - 1]} "
                     f"{self.dates.start_date.day}")
            if self._need_specify_year():
                text += f" {self.dates.start_date.year}"
            if self.dates.end_datetime is not None:
                text += " - "
        if self.dates.end_datetime is not None:
            # set a clickable range
            self.clickable = True
            text += (
                     f"{__MONTHS__[self.dates.end_datetime.month - 1]} "
                     f"{self.dates.end_datetime.day}")
            if self._need_specify_year():
                text += f" {self.dates.end_datetime.year}"
        self.update_status()
        self.setText(text)

    def update_status(self) -> None:
        """Set the dates indicator status."""
        if self.dates.completed:
            self.set_completed()
            return
        if self.dates.end_datetime is None:
            self.set_normal()
            return
        delta = (self.dates.end_datetime - datetime.now()
                 ).total_seconds()
        if delta <= 0:
            self.set_overdue()
            return
        if delta > 0 and delta <= 86400:
            self.set_due_soon()
            return
        self.set_normal()

    def _need_specify_year(self) -> bool:
        """Return True if we need to specify year when printing the dates."""
        today = date.today()
        if self.dates.start_date is not None:
            if self.dates.start_date != today.year:
                return True
            if self.dates.end_datetime is not None:
                if self.dates.end_datetime.year != self.dates.start_date.year:
                    return True
        if self.dates.end_datetime is not None:
            if self.dates.end_datetime.year != today.year:
                return True
            if self.dates.start_date is not None:
                if self.dates.start_date.year != self.dates.end_datetime.year:
                    return True
        return False
