"""The duration module."""
import logging
from typing import Any, Sequence, Union

from PySide6.QtCore import QObject, Qt, Signal
from PySide6.QtGui import QIntValidator, QShortcut
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QLineEdit, QPushButton, QVBoxLayout,
        )

from ..custom_widgets import SmallCloseButton
from ..mixins import BaseIndicator
from ..routines import get_app, m_to_hm, unsaved_changes
from ..styles_constants import __PALETTE__


class DurationObject:
    """Mixin class for objects with durations."""

    duration_changed = Signal()

    def __init__(
            self, *args, fixed_by_children_duration: bool = True,
            **kwargs) -> None:
        self._duration = None
        self.duration_indicator = None
        self.fixed_by_children_duration = fixed_by_children_duration

    def setup_ui(self) -> None:
        """Setup the durations part of the UI."""
        self.setup_duration_indicator()

    def setup_duration_indicator(self) -> None:
        """Setup the durations indicator."""
        self.duration_indicator = DurationIndicator(
                self.duration, parent=self)
        self.duration_indicator.hidden.connect(self.indicator_hidden)
        self.duration_indicator.shown.connect(self.indicator_shown)
        self.duration_changed.connect(self.update_duration_indicator)

    @property
    def duration(self):
        """The duration object for this object."""
        return self._duration

    @unsaved_changes
    def set_duration(
            self, duration: int = 0,
            load_from: dict[str, Any] = None,
            unsaved_changes: bool = True,
            ) -> None:
        """Set the duration."""
        if self.duration is None:
            self._duration = Duration(parent=self)
            self.duration.duration_changed.connect(self.duration_changed)
            if self.duration_indicator is not None:
                self.duration_indicator.duration = self.duration
        self.duration.set_duration(
                duration, unsaved_changes=unsaved_changes)
        if load_from is not None:
            self.duration.load_json_data(load_from)
        if self.duration_indicator is not None:
            self.update_duration_indicator()
        if unsaved_changes:
            self._logger.info(
                    f"Setting duration of '{self.duration}' for '{self}'.")
            return True

    @unsaved_changes
    def remove_duration(self) -> None:
        """Remove the duration."""
        if self.duration is None:
            return
        self.set_duration(0)
        self.duration_changed.emit()
        self._logger.info(f"Removing duration from {self}.")
        if self.duration_indicator is not None:
            self.update_duration_indicator()
        return True

    def has_duration(self) -> bool:
        """Return true if duration > 0."""
        if self.duration is None:
            return False
        if self.duration.duration is None:
            return False
        if self.duration.duration == 0:
            return False
        return True

    def get_indicators(self) -> Sequence[QObject]:
        """Get the duration indicator."""
        return [self.duration_indicator]

    def update_indicators(self) -> None:
        """Update the indicators."""
        self.update_duration_indicator()

    def update_duration_indicator(self) -> None:
        """Update the duration indicator."""
        if self.duration_indicator is None:
            return
        if self.duration is None:
            self.duration_indicator.hide()
            return
        self.duration_indicator.update()

    def get_children_duration(self):
        """Get the children durations."""
        raise NotImplementedError(
                f"{self.__class__} not implemented.")

    def any_children_has_duration(self) -> bool:
        """Return true if any children has durations."""
        for dur in self.get_children_duration():
            if dur is None:
                continue
            if dur.duration == 0:
                continue
            return True
        return False

    def get_duration_setter_dialog(self):
        """Get the duration setter dialog."""
        raise NotImplementedError(
                f"{self.__class__} not implemented.")

    def update_duration(self) -> None:
        """Called to update the duration."""
        if not self.has_duration():
            return
        if not self.fixed_by_children_duration:
            # this object is not fixed by its children duration
            # no need to update
            return
        if not self.any_children_has_duration():
            # no need to update
            return
        self._logger.info(
                f"Updating duration of {self} based on children duration.")
        self.set_duration(
                self.total_children_duration(), unsaved_changes=False)

    def total_children_duration(self) -> int:
        """Compute the total duration of all children."""
        dur = 0
        for duration in self.get_children_duration():
            if duration is None:
                continue
            dur += duration.duration
        return dur

    def update_duration_setter_dialog(self) -> None:
        """Update the duration setter dialog."""
        setter_dialog = self.get_duration_setter_dialog()
        if setter_dialog is not None:
            setter_dialog.update_from_duration()

    def showEvent(self, *args, **kwargs) -> None:
        """Called when showing the widget."""
        self.update_duration()
        self.update_duration_indicator()

    def get_json_data(self) -> dict[str, Any]:
        """Get the json data part for the duration."""
        data = {"duration": None}
        if self.duration is not None:
            data["duration"] = self.duration.get_json_data()
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load the json data for the duration part of the object."""
        if "duration" not in data:
            return
        if data["duration"] is None:
            return
        self.set_duration(load_from=data["duration"], unsaved_changes=False)


class Duration(QObject):
    """A duration for a board object or subobject."""

    duration_changed = Signal()

    def __init__(
            self, *args,
            **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._logger = logging.getLogger("Duration")
        self._logger.setLevel(get_app().loglevel)
        self._duration = 0  # total duration in minutes

    def __repr__(self) -> str:
        """The duration representation."""
        return f"< Duration: {self.duration} min >"

    @property
    def duration(self) -> int:
        """The duration in minutes."""
        return self._duration

    @unsaved_changes
    def set_duration(
            self, duration: int, unsaved_changes: bool = True,
            ) -> Union[None, True]:
        """Set the duration in minutes.

        Parameters
        ----------
        duration: int
            The duration.
        unsaved_changes: bool, optional
            If True, unsaved changes are recored.
        """
        if duration == self.duration:
            return
        if duration < 0:
            raise ValueError("Cannot have negative duration.")
        self._duration = duration
        self.duration_changed.emit()
        if unsaved_changes:
            self._logger.info(
                    f"Setting duration to {duration} min.")
            return True

    def get_json_data(self) -> dict[str, Any]:
        """Get the json dict to save duration data."""
        return {
                "duration": self.duration,
                }

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load data from json dict."""
        if "duration" in data:
            self.set_duration(data["duration"], unsaved_changes=False)


class DurationSetterDialog(QFrame):
    """A duration setter dialog."""

    save = Signal()
    remove = Signal()
    cancel = Signal()

    def __init__(self, duration: Duration, *args, **kwargs) -> None:
        super().__init__(*args, objectName="dark_grey_dialogs", **kwargs)
        self.duration = duration
        self.setup_ui()
        self.setup_shortcuts()

    def setup_shortcuts(self) -> None:
        """Setup the keyboard shortcuts."""
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self)
        self.enter_shortcut.activated.connect(self._save)
        self.return_shortcut = QShortcut(Qt.Key_Return, self)
        self.return_shortcut.activated.connect(self._save)
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(self._cancel)

    def setup_ui(self) -> None:
        """Setup the duration setter dialog UI."""
        self.setLayout(QVBoxLayout())
        self.setup_header()
        self.setup_lineedits_ui()
        self.setup_buttons()
        self.adjustSize()

    def setup_buttons(self) -> None:
        """Setup the duration setter dialog buttons."""
        self.buttons = QFrame(parent=self)
        self.buttons.setLayout(QHBoxLayout())
        self.layout().addWidget(self.buttons)

        self.save_button = QPushButton(
                parent=self.buttons, text="Save", objectName="purple_btn")
        self.save_button.clicked.connect(self._save)
        self.buttons.layout().addWidget(self.save_button)

        self.remove_button = QPushButton(
                parent=self.buttons, text="Remove",
                objectName="gray_btn_centered",
                )
        self.remove_button.clicked.connect(self._remove)
        self.buttons.layout().addWidget(self.remove_button)

    def setup_lineedits_ui(self) -> None:
        """Setup the lineedits UI."""
        # hours and minutes lineedits + labels
        self.lineedits = QFrame(parent=self)
        self.lineedits.setLayout(QHBoxLayout())
        self.layout().addWidget(self.lineedits)

        self.hours_lineedit = QLineEdit(parent=self.lineedits)
        self.hours_lineedit.setValidator(
                QIntValidator(0, 99, parent=self.lineedits))
        self.hours_lineedit.setFixedWidth(30)
        self.lineedits.layout().addWidget(self.hours_lineedit)
        self.hours_label = QLabel(text="h", parent=self.lineedits)
        self.hours_label.setAlignment(Qt.AlignLeft | Qt.AlignBottom)
        self.lineedits.layout().addWidget(self.hours_label)

        self.minutes_lineedit = QLineEdit(parent=self.lineedits)
        self.minutes_lineedit.setValidator(
                QIntValidator(0, 59, parent=self.lineedits))
        self.minutes_lineedit.setFixedWidth(30)
        self.lineedits.layout().addWidget(self.minutes_lineedit)
        self.minutes_label = QLabel(text="m", parent=self.lineedits)
        self.minutes_label.setAlignment(Qt.AlignLeft | Qt.AlignBottom)
        self.lineedits.layout().addWidget(self.minutes_label)

    def setup_header(self) -> None:
        """Setup the duration setter dialog header."""
        self.header = QFrame(parent=self)
        self.header.setLayout(QHBoxLayout())
        self.layout().addWidget(self.header)
        self.title_label = QLabel(
                parent=self.header, text="Set duration")
        self.title_label.setAlignment(Qt.AlignCenter)
        self.header.layout().addWidget(self.title_label)
        self.close_button = SmallCloseButton(parent=self.header)
        self.header.layout().addWidget(self.close_button)
        self.close_button.clicked.connect(self._cancel)

    def init_from_duration_object(self, obj: DurationObject) -> None:
        """Initialize the dialog from a duration object."""
        if not isinstance(obj, DurationObject):
            raise TypeError("object should be an instance of DurationObject")
        self.enable_lineedits(not obj.any_children_has_duration())
        if obj.has_duration():
            self.duration = obj.duration
            self.update_from_duration()
        else:
            self.update_from_duration(obj.total_children_duration())

    def _cancel(self) -> None:
        """Cancel the duration setter dialog."""
        self.cancel.emit()
        self.hide()

    def _save(self) -> None:
        """Save the duration."""
        self.save.emit()
        self.hide()

    def _remove(self) -> None:
        """Remove the duration."""
        self.remove.emit()
        self.hide()

    def get_duration(self) -> int:
        """Get the total duration in minutes."""
        h = int(self.hours_lineedit.text())
        m = int(self.minutes_lineedit.text())
        return h * 60 + m

    def update_from_duration(self, minutes: int = None) -> None:
        """Set the time in lineedits based on the duration object."""
        hours = 0
        if minutes is None:
            if self.duration is None:
                minutes = 0
            else:
                minutes = self.duration.duration
        if minutes >= 60:
            hours = minutes // 60
            minutes -= hours * 60
        self.hours_lineedit.setText(str(hours).zfill(2))
        self.minutes_lineedit.setText(str(minutes).zfill(2))

    def enable_lineedits(self, enable: bool) -> None:
        """Enable or disable the hours and minutes line edits."""
        self.hours_lineedit.setEnabled(enable)
        self.minutes_lineedit.setEnabled(enable)

    def showEvent(self, *args, **kwargs) -> None:
        """Called when showing dialog."""
        for shortcut in [
                self.enter_shortcut, self.return_shortcut,
                self.escape_shortcut]:
            shortcut.setEnabled(True)
        return super().showEvent(*args, **kwargs)

    def hideEvent(self, *args, **kwargs) -> None:
        """Called when hiding dialog."""
        for shortcut in [
                self.enter_shortcut, self.return_shortcut,
                self.escape_shortcut]:
            shortcut.setEnabled(False)
        return super().hideEvent(*args, **kwargs)


class DurationIndicator(BaseIndicator):
    """The generic duration indicator."""

    def __init__(self, duration: Duration, *args, **kwargs) -> None:
        self.duration = duration
        super().__init__(*args, **kwargs)
        self.set_color(__PALETTE__["gray"])

    def update(self) -> None:
        """Update the duration indicator."""
        if self.duration is None:
            self.hide()
            return
        minutes = self.duration.duration
        if minutes == 0:
            # don't show a 0 minutes duration
            self.hide()
            return
        hours, minutes = m_to_hm(minutes)
        if not hours:
            text = f"Dur: {minutes}m"
        elif not minutes:
            text = f"Dur: {hours}h"
        else:
            text = f"Dur: {hours}h{str(minutes).zfill(2)}m"
        self.setText(text)
        self.show()
