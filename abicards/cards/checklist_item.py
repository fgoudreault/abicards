"""The checklist item module."""
import logging
from typing import Any, Sequence

from PySide6.QtCore import QPoint, QRect, Qt, Signal
from PySide6.QtGui import QShortcut
from PySide6.QtWidgets import (
        QCheckBox, QFrame, QHBoxLayout, QLabel, QPushButton, QVBoxLayout,
        QWidget,
        )

from .convert import ConvertToDialog, ConvertibleObject
from .dates import DatableObject, Dates, DatesPickerDialog
from .duration import DurationObject, DurationSetterDialog
from .routines import get_index_of_hovering_widget_above_list
from ..custom_widgets import DotsButton, MinimalSizeTextEdit, SmallCloseButton
from ..mixins import IndicatedObject
from ..routines import get_loglevel, unsaved_changes
from ..styles_constants import (
        __BUTTON_HEIGHT_MEDIUM__,
        __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__,
        __PALETTE__,
        )


class ChecklistItem(
        QWidget, DatableObject, DurationObject, IndicatedObject,
        ConvertibleObject,
        ):
    """A checklist item."""

    has_dates_indicator = True
    checklist_item_updated = Signal()

    def __init__(self, *args, title: str = "", **kwargs):
        QWidget.__init__(self, *args, **kwargs)
        DatableObject.__init__(self, *args, **kwargs)
        DurationObject.__init__(
                self, *args, fixed_by_children_duration=False, **kwargs)
        IndicatedObject.__init__(self, *args, **kwargs)
        ConvertibleObject.__init__(self, *args, **kwargs)
        self.checklist = self.parent()
        self.card = self.checklist.card
        self.card_list = self.card.card_list
        self.board = self.card.board
        self._title = title
        self._checked = False
        self.dates_picker_dialog = None
        self.full_edit_card_dialog_widget = None
        self._logger = logging.getLogger("ChecklistItem")
        self._logger.setLevel(get_loglevel())
        self.duration_changed.connect(self.checklist_item_updated)
        self.checklist_item_updated.connect(self.update_indicators)
        self.completed_changed.connect(self.checklist_item_updated)

    def __repr__(self) -> str:
        """The checklist item string representation."""
        return f"< ChecklistItem: '{self.title}' in '{self.checklist}' >"

    def can_be_converted(self) -> bool:
        """Return True if this item can be converted to another one."""
        # a checklist item can always be converted since it has no children
        return True

    def get_indicators(self) -> Sequence[QWidget]:
        """Get the list of indicators."""
        return (DatableObject.get_indicators(self) +
                DurationObject.get_indicators(self))

    def get_children_dates(self) -> Sequence[Dates]:
        """No children dates for checklist items."""
        return []

    def get_dates_picker_dialog(self) -> DatesPickerDialog:
        """Get the dates picker dialog for this checklist item."""
        return self.dates_picker_dialog

    def update_indicators(self) -> None:
        """Update all indicators."""
        DatableObject.update_indicators(self)
        DurationObject.update_indicators(self)

    def setup_indicators(self, *args, **kwargs) -> None:
        """Setup the checklist item indicators."""
        self.setup_dates_indicator()
        self.setup_duration_indicator()
        IndicatedObject.setup_indicators(self, *args, **kwargs)

    @property
    def title(self):
        """The checklist item title."""
        return self._title

    @unsaved_changes
    def set_title(self, title: str, unsaved_changes: bool = True) -> None:
        """Set the checklist item title."""
        og = self.title
        if self.title == title:
            return
        self._title = title
        if unsaved_changes:
            self._logger.info(
                    f"Changing checklist item '{og}' => '{title}'.")
            return True

    @property
    def checked(self) -> bool:
        """Return true if this item is checked."""
        return self._checked

    @unsaved_changes
    def set_checked(self, checked: bool, unsaved_changes: bool = True) -> None:
        """Set checked or unchecked."""
        if checked is self._checked:
            return
        self._checked = checked
        if self.dates is not None:
            self.dates.set_completed(
                    self.checked, unsaved_changes=unsaved_changes)
        self.checklist_item_updated.emit()
        if unsaved_changes:
            self._logger.info(
                    f"Setting '{self.title}' checklist item to "
                    f"checked = {checked}")
            return True

    def set_dates(self, *args, **kwargs) -> None:
        """Set the dates for this checklist item."""
        rtn = DatableObject.set_dates(self, *args, **kwargs)
        if self.dates is not None:
            self.dates.dates_changed.connect(self.checklist_item_updated)
            self.dates.set_completed(self.checked)
        self.checklist_item_updated.emit()
        self.update_dates_indicator()
        return rtn

    def get_json_data(self) -> dict[str, Any]:
        """Return the json data for this checklist item for storage."""
        data = DatableObject.get_json_data(self)
        data.update({"title": self.title, "checked": self.checked})
        data.update(DurationObject.get_json_data(self))
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load checklist item from json object."""
        self.set_title(data["title"], unsaved_changes=False)
        if "checked" in data:
            self.set_checked(data["checked"], unsaved_changes=False)
        DatableObject.load_json_data(self, data)
        DurationObject.load_json_data(self, data)

    def delete(self) -> None:
        """Delete the checklist item."""
        self.checklist.remove_checklist_item(self)
        fd = self.checklist.full_edit_card_dialog_widget
        if fd is not None:
            fd.remove_checklist_item_widget(self)


__CHECKLIST_ITEM_HEIGHT__ = 30


class FullEditCardDialogChecklistItem(QFrame):
    """The full dialog checklist item field.

    - A checkbox
    - A label
    - An edit button.

    This widget is draggable in order to reorder within a checklist
    or to change to another checklist.
    """

    checked = Signal()
    unchecked = Signal()

    def __init__(
            self, full_edit_card_dialog, checklist_item: ChecklistItem,
            *args, **kwargs):
        super().__init__(
                *args, objectName="FullEditCardDialogChecklistItem",
                **kwargs)
        self._logger = logging.getLogger("FullEditCardDialogChecklistItem")
        self.checklist_item = checklist_item
        self.checklist_item.full_edit_card_dialog_widget = self
        self.checklist = self.checklist_item.parent()
        self.checklist_widget = self.parent().parent().parent()
        self.full_edit_card_dialog = full_edit_card_dialog
        self.editor = None
        self.ghost_item = None
        self.setup_ui()

    def __repr__(self) -> str:
        """The repr string."""
        return f"< FullEditCardDialogChecklistItem: title = '{self.title}' >"

    @property
    def title(self) -> str:
        """The checklist item title."""
        return self.checklist_item.title

    @property
    def subdialogs(self) -> Sequence:
        """The list of subdialogs."""
        return [self.editor] + self.title_label.subdialogs

    def toggle_edit_item(self, *args, **kwargs) -> None:
        """Toggle the edit checklist item menu."""
        self.title_label.toggle_edit_item(*args, **kwargs)

    @property
    def edit_item_dialog(self):
        """The edit checklist item dialog."""
        return self.title_label.edit_item_dialog

    def pop_from_parent(self, newparent):
        """Pop from the parent checklist."""
        self.place_ghost_item()
        if self in self.checklist_widget.checklist_item_widgets:
            self.checklist_widget.checklist_item_widgets.remove(self)
        self.setParent(newparent)
        self.set_dragging_style_sheet()

    def place_ghost_item(self):
        """Place a ghost item in lieu of the checklist item when dragging."""
        if self.ghost_item is None:
            self.ghost_item = QFrame(
                    parent=self.parent(),
                    objectName="ghost_checklist_item")
        self.ghost_item.setFixedSize(self.size())
        self.ghost_item.parent().layout().replaceWidget(
                self, self.ghost_item)
        self.ghost_item.show()

    def remove_ghost_item(
            self, return_ghost_item_index: bool = False,
            ) -> bool | None:
        """Remove the ghost item.

        Parameters
        ----------
        return_ghost_item_index: bool, optional
            If True, return the index of the ghost item in its current parent.

        Returns
        -------
        int: If 'return_ghost_item_index' is True.
        None: otherwise or if ghost item does not exists.
        """
        if self.ghost_item is None:
            return
        self.setParent(self.ghost_item.parent())
        rtn = None
        if return_ghost_item_index:
            rtn = self.parent().layout().indexOf(self.ghost_item)
        self.parent().layout().replaceWidget(self.ghost_item, self)
        self.ghost_item.hide()
        return rtn

    def handle_drag_move(self, target_rect: QRect):
        """Called when dragging the checklist item at each move event."""
        # detect above which checklist we are on top of
        local_target = target_rect.translated(
                self.parent().mapToGlobal(QPoint(0, 0)))
        for checklist in self.full_edit_card_dialog.checklist_fields:
            checklist_geo = checklist.rect().translated(
                    checklist.mapToGlobal(QPoint(0, 0)))
            if not checklist_geo.intersects(local_target):
                continue
            # we're above this checklist
            break
        else:
            self.move(target_rect.topLeft())
            return
        # check what would be the idx of the checklist item
        idx = get_index_of_hovering_widget_above_list(
                target_rect, self, checklist.checklist_item_widgets)
        # insert ghost item in its stead
        old_parent = self.ghost_item.parent()
        old_parent.layout().removeWidget(
                self.ghost_item)
        self.ghost_item.setParent(checklist.checklist_items_container)
        self.ghost_item.parent().layout().insertWidget(
                idx, self.ghost_item)
        old_parent.adjustSize()
        old_parent.parent().adjustSize()
        self.ghost_item.parent().adjustSize()
        self.ghost_item.parent().parent().adjustSize()
        self.set_dragging_style_sheet()

    @unsaved_changes
    def put_into_new_parent(self):
        """Put back the checklist item into new checklist."""
        # get index of ghost item
        old = self.checklist_widget
        idx = self.remove_ghost_item(return_ghost_item_index=True)
        # new parent set when removing ghost. change checklist item from
        # actual checklists
        self.checklist.remove_checklist_item(self.checklist_item)
        self.checklist_widget = self.parent().parent().parent()
        self.checklist = self.checklist_widget.checklist
        self.checklist.insert_checklist_item(
                idx, self.checklist_item,
                )
        self.checklist_widget.checklist_item_widgets.insert(idx, self)
        self.checklist_widget.update_progressbar()
        # reconnect checked/unchecked slots to new parent
        self.checked.disconnect()
        self.checked.connect(self.checklist_widget.update_progressbar)
        self.unchecked.disconnect()
        self.unchecked.connect(self.checklist_widget.update_progressbar)
        old.update_progressbar()
        # change checklist
        self._logger.info(
                f"Inserting checklist item '{self.title}' into checklist "
                f"'{self.checklist_widget.checklist.title}'.")
        if self.underMouse():
            self.set_hovered_style_sheet()
        else:
            self.set_original_style_sheet()
        return True

    def setup_ui(self):
        """Setup checklist item ui."""
        self.setLayout(QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.set_original_style_sheet()
        # self.setFixedHeight(__CHECKLIST_ITEM_HEIGHT__)
        # first item is the checkbox
        self.setup_checkbox_ui()
        self.non_checkbox_widgets = QFrame(parent=self)
        self.non_checkbox_widgets.setLayout(QVBoxLayout())
        self.non_checkbox_widgets.setContentsMargins(0, 0, 0, 0)
        self.non_checkbox_widgets.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self.non_checkbox_widgets)
        # second item is the label for the list item
        self.setup_title_ui()
        # setup indicators
        self.checklist_item.setup_indicators(
                parent=self, layout=self.non_checkbox_widgets.layout())
        # set checkbox state after label is created
        self.checkbox.setChecked(self.checklist_item.checked)

    def setup_title_ui(self) -> None:
        """Setup the title label ui."""
        self.title_label = ChecklistItemFrame(
                self.full_edit_card_dialog,
                self.checklist_item,
                parent=self)
        # self.title_label.setFixedHeight(self.height())
        self.title_label.released.connect(self.edit_checklist_item)
        self.title_label.delete.connect(self.delete)
        self.non_checkbox_widgets.layout().addWidget(
                self.title_label, Qt.AlignLeft)

    def setup_checkbox_ui(self) -> None:
        """Setup the checkbox ui."""
        self.chkbx_container = QFrame(parent=self)
        self.chkbx_container.setLayout(QVBoxLayout())
        self.layout().addWidget(self.chkbx_container, Qt.AlignLeft)
        self.checkbox = QCheckBox(parent=self)
        self.chkbx_container.setFixedWidth(
                __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__)
        self.checkbox.checkStateChanged.connect(self.checkbox_checked)
        self.chkbx_container.layout().addWidget(
                self.checkbox)
        self.chkbx_container.layout().addStretch()

    def edit_checklist_item(self):
        """Edit checklist item."""
        if self.editor is None:
            self.editor = FullEditDialogChecklistItemEditor(
                    self.full_edit_card_dialog,
                    parent=self)
            self.editor.save.connect(self.save_edits)
            self.editor.cancel.connect(self.hide_editor)
        self.non_checkbox_widgets.hide()
        self.checklist_widget.add_item_btn.hide()
        self.layout().removeWidget(self.non_checkbox_widgets)
        self.layout().addWidget(self.editor, Qt.AlignLeft)
        self.editor.set_title(self.checklist_item.title)
        self.editor.show()
        self.editor.setFocus()

    def save_edits(self) -> None:
        """Save the edits from the editor."""
        self.checklist_item.set_title(self.editor.get_title())
        self.title_label.update()
        self.hide_editor()

    def hide_editor(self) -> None:
        """Hide the checklist item editor."""
        self.editor.hide(from_parent=True)
        self.layout().removeWidget(self.editor)
        self.layout().addWidget(self.non_checkbox_widgets, Qt.AlignLeft)
        self.checklist_widget.add_item_btn.show()
        self.non_checkbox_widgets.show()

    def is_checked(self):
        """Return true if item is checked."""
        return self.checkbox.isChecked()

    def checkbox_checked(self) -> None:
        """Called when checkbox is checked or unchecked."""
        if self.checkbox.isChecked():
            self.title_label.strike_text()
            self.checklist_item.set_checked(True)
            self.checked.emit()
        else:
            self.title_label.unstrike_text()
            self.checklist_item.set_checked(False)
            self.unchecked.emit()

    def delete(self):
        """Delete the checklist item."""
        self.checklist_widget.delete_checklist_item(self)

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing the widget."""
        # self.checklist_item.update_dates()
        # self.checklist_item.update_dates_indicator()
        return super().showEvent(*args, **kwargs)

    def enterEvent(self, *args, **kwargs) -> Any:
        """Called when entering the checklist item frame."""
        self.set_hovered_style_sheet()
        return super().enterEvent(*args, **kwargs)

    def leaveEvent(self, *args, **kwargs) -> Any:
        """Called when mouse leaves the checklist item frame."""
        self.set_original_style_sheet()
        return super().leaveEvent(*args, **kwargs)

    def set_hovered_style_sheet(self):
        """Set the hovered style sheet."""
        self.setStyleSheet(
                f"""QFrame#FullEditCardDialogChecklistItem {{
                        background-color: {__PALETTE__['gray']};
                        border: 1px solid transparent;
                        border-radius: 5px;
                    }}
                """)

    def set_original_style_sheet(self):
        """Set the passive style sheet."""
        self.setStyleSheet(
                f"""QFrame#FullEditCardDialogChecklistItem {{
                        background-color: {__PALETTE__['dark_gray']};
                        border: 1px solid transparent;
                        border-radius: 5px;
                    }}
                """)

    def set_dragging_style_sheet(self):
        """Set the dragging style sheet."""
        self.setStyleSheet(
                f"""QFrame#FullEditCardDialogChecklistItem {{
                        background-color: {__PALETTE__['dark_gray']};
                        border: 1px solid {__PALETTE__['green']};
                        border-radius: 5px;
                    }}
                """)


class ChecklistItemFrame(QFrame):
    """A checklist item frame which contains the title and edit btn."""

    released = Signal()
    delete = Signal()

    def __init__(
            self, full_card_edit_dialog,
            checklist_item: ChecklistItem, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.full_card_edit_dialog = full_card_edit_dialog
        self.edit_item_dialog = None
        self.checklist_item = checklist_item
        self.setup_ui()

    @property
    def subdialogs(self) -> Sequence:
        """The list of subdialogs."""
        if self.edit_item_dialog is None:
            return []
        return [self.edit_item_dialog] + self.edit_item_dialog.subdialogs

    def update(self):
        """Update the item label."""
        self.title_label.setPlainText(self.checklist_item.title)

    def get_title(self) -> str:
        """Get the title of the item as set on its label."""
        return self.title_label.toPlainText()

    def mouseReleaseEvent(self, event) -> Any:
        """Called when mouse button is released above the checklist item."""
        self.released.emit()
        return super().mouseReleaseEvent(event)

    def setup_ui(self):
        """Setup the checklist item ui (without checkbox."""
        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(10, 0, 10, 0)
        self.setStyleSheet(
                """QFrame { background-color: transparent;
                            border: 0px transparent }""")
        self.setLayout(self.layout)
        self.title_label = MinimalSizeTextEdit(
                parent=self)
        self.title_label.setAttribute(Qt.WA_TransparentForMouseEvents, True)
        self.title_label.setPlainText(self.checklist_item.title)
        self.title_label.setReadOnly(True)
        self.layout.addWidget(self.title_label)
        # edit btn
        self.edit_btn = DotsButton(parent=self)
        self.edit_btn.clicked.connect(self.toggle_edit_item)
        self.edit_btn.setFixedWidth(20)
        self.layout.addWidget(self.edit_btn)

    def strike_text(self):
        """Strike text of checklist item."""
        font = self.title_label.font()
        font.setStrikeOut(True)
        self.title_label.setFont(font)

    def unstrike_text(self):
        """Unstrike the text of checklist item."""
        font = self.title_label.font()
        font.setStrikeOut(False)
        self.title_label.setFont(font)

    def toggle_edit_item(self):
        """Open/close edit dialog."""
        if self.edit_item_dialog is None:
            self.edit_item_dialog = EditItemDialog(
                    self,
                    self.full_card_edit_dialog,
                    self.checklist_item,
                    parent=self.full_card_edit_dialog.full_edit_card_widget,
                    )
            self.edit_item_dialog.delete.connect(self.delete_item)
        # move the dialog below the edit btn
        self.edit_item_dialog.place_dialog_under(
                self.edit_item_dialog, self.edit_btn,
                origin="bottom_right")
        if self.edit_item_dialog.isVisible():
            self.edit_item_dialog.hide()
        else:
            self.edit_item_dialog.show()
            self.edit_item_dialog.raise_()

    def delete_item(self):
        """Delete the checklist item."""
        self.edit_item_dialog.hide()
        self.edit_item_dialog.destroy()
        self.delete.emit()


class EditItemDialog(QFrame):
    """Dialog class for the edit checklist item dialog."""

    delete = Signal()

    def __init__(
            self, checklist_item_widget,
            full_card_edit_dialog, checklist_item: ChecklistItem,
            *args, **kwargs,
            ):
        super().__init__(
                *args, objectName="dark_grey_dialogs",
                **kwargs)
        self.checklist_item_widget = checklist_item_widget
        self.dates_picker_dialog = None
        self.duration_setter_dialog = None
        self.convert_to_dialog = None
        self.checklist_item = checklist_item
        self.full_card_edit_dialog = full_card_edit_dialog
        self.setup_ui()

    @property
    def subdialogs(self) -> Sequence:
        """The checklist item edit dialog's subdialogs."""
        subs = [self.dates_picker_dialog, self.duration_setter_dialog,
                self.convert_to_dialog,
                ]
        if self.convert_to_dialog is not None:
            subs += self.convert_to_dialog.subdialogs
        return subs

    def setup_ui(self):
        """Setup the dialog ui."""
        # title with x button
        self.setLayout(QVBoxLayout())
        self.setup_header()
        self.setup_actions()
        self.adjustSize()

    def setup_actions(self):
        """Setup the edit dialog actions."""
        # set dates action
        self.dates_button = QPushButton(
                text="Dates", parent=self, objectName="light_gray_btn_center",
                )
        self.dates_button.clicked.connect(self.open_dates_picker_dialog)
        self.dates_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.dates_button)
        # set duration action
        self.duration_button = QPushButton(
                text="Duration", parent=self,
                objectName="light_gray_btn_center",
                )
        self.duration_button.clicked.connect(self.open_duration_setter_dialog)
        self.duration_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.duration_button)
        # convert to action
        self.convert_button = QPushButton(
                text="Convert", parent=self,
                objectName="light_gray_btn_center",
                )
        self.convert_button.clicked.connect(self.open_convert_to_dialog)
        self.convert_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.convert_button)
        # delete action
        self.delete_button = QPushButton(
                "Delete", parent=self,
                objectName="red_btn",
                )
        self.delete_button.setFixedHeight(
                __BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.delete_button)
        self.delete_button.clicked.connect(self.delete)

    def setup_header(self):
        """Setup the dialog header ui."""
        # dialog title + close btn
        self.title_container = QFrame(parent=self)
        self.layout().addWidget(self.title_container)
        self.title_container_layout = QHBoxLayout()
        self.title_container.setLayout(self.title_container_layout)
        # dialog title
        self.title_label = QLabel(text="Item actions", parent=self)
        font = self.title_label.font()
        font.setBold(True)
        self.title_label.setFont(font)
        self.title_container_layout.addWidget(self.title_label)

        # close btn
        self.close_button = SmallCloseButton(parent=self)
        self.close_button.clicked.connect(self.hide)
        self.title_container_layout.addWidget(self.close_button)
        self.close_button.setFixedSize(15, 15)

    def open_dates_picker_dialog(self) -> None:
        """Open the dates picker dialog."""
        if self.dates_picker_dialog is None:
            self.dates_picker_dialog = DatesPickerDialog(
                    self.checklist_item.dates,
                    parent=self.full_card_edit_dialog.full_edit_card_widget,
                    enable_fixed_by_children_chkbox=False,
                    )
            self.checklist_item.dates_picker_dialog = self.dates_picker_dialog
            self.dates_picker_dialog.set_dates.connect(
                    self.set_dates)
            self.dates_picker_dialog.remove_dates.connect(self.remove_dates)
        # place dialog under dates button and adjust whole window if necessary
        if self.dates_picker_dialog.isVisible():
            self.dates_picker_dialog.hide()
        else:
            self.place_dialog_under(
                    self.dates_picker_dialog, self.dates_button)
            self.dates_picker_dialog.show()

    def set_dates(self) -> None:
        """Set the checklist items dates from the dates picker."""
        self.checklist_item.set_dates(
                start_date=self.dates_picker_dialog.get_start_date(),
                end_datetime=self.dates_picker_dialog.get_end_datetime(),
                )
        self.hide_all()

    def remove_dates(self) -> None:
        """Remove the dates from the checklist item."""
        self.checklist_item.remove_dates()
        self.hide_all()

    def open_duration_setter_dialog(self) -> None:
        """Open the duration setter dialog."""
        if self.duration_setter_dialog is None:
            self.duration_setter_dialog = DurationSetterDialog(
                    self.checklist_item.duration,
                    parent=self.full_card_edit_dialog.full_edit_card_widget,
                    )
            self.checklist_item.duration_setter_dialog = (
                    self.duration_setter_dialog)
            self.duration_setter_dialog.save.connect(self.set_duration)
            self.duration_setter_dialog.remove.connect(self.remove_duration)
        if self.duration_setter_dialog.isVisible():
            self.duration_setter_dialog.hide()
        else:
            self.duration_setter_dialog.update_from_duration()
            self.place_dialog_under(
                self.duration_setter_dialog, self.duration_button)
            self.duration_setter_dialog.show()

    def set_duration(self) -> None:
        """Set the checklist item duration from the duration setter."""
        self.checklist_item.set_duration(
                self.duration_setter_dialog.get_duration())
        self.hide_all()

    def remove_duration(self) -> None:
        """Remove the duration of the checklist item."""
        self.checklist_item.remove_duration()
        self.hide_all()

    def open_convert_to_dialog(self) -> None:
        """Open the convert to dialog."""
        if self.convert_to_dialog is None:
            self.convert_to_dialog = ConvertToDialog(
                    "checklist_item", self.checklist_item,
                    parent=self.full_card_edit_dialog.full_edit_card_widget)
            self.convert_to_dialog.converted.connect(
                    self.hide_after_convert)
        self.convert_to_dialog.setEnabled(
                self.checklist_item.can_be_converted())
        if self.convert_to_dialog.isVisible():
            self.convert_to_dialog.hide()
        else:
            self.place_dialog_under(
                    self.convert_to_dialog, self.convert_button)
            self.convert_to_dialog.show()

    def hide_all(self) -> None:
        """Hide all dialogs and self."""
        self.hide()
        if self.duration_setter_dialog is not None:
            self.duration_setter_dialog.hide()
        if self.dates_picker_dialog is not None:
            self.dates_picker_dialog.hide()
        if self.convert_to_dialog is not None:
            self.convert_to_dialog.hide()

    def hide_after_convert(self) -> None:
        """Hide all dialogs and self and the full card edit."""
        self.hide_all()
        self.full_card_edit_dialog.hide()

    def place_dialog_under(
            self, dialog, widget, origin="bottom_left") -> None:
        """Move given dialog under given widget."""
        geo = widget.geometry()
        if origin == "bottom_left":
            og = geo.bottomLeft()
        elif origin == "bottom_right":
            og = geo.bottomRight()
        else:
            raise NotImplementedError(origin)
        og = widget.parent().mapToGlobal(og)
        newx = og.x() - dialog.width()
        fd = self.full_card_edit_dialog
        og_rel = fd.full_edit_card_widget.mapFromGlobal(
                    QPoint(newx, og.y()))
        dialog.move(og_rel)
        # adjust parent size if necessary
        if og_rel.y() + dialog.height() > (
                self.full_card_edit_dialog.full_edit_card_widget.height()):
            self.full_card_edit_dialog.full_edit_card_widget.setFixedHeight(
                    og_rel.y() + dialog.height())


class FullEditDialogChecklistItemEditor(QFrame):
    """The full edit dialog checklist item editor.

    Basically a text edit with a save and close button.
    """

    save = Signal()
    cancel = Signal()

    def __init__(self, full_edit_card_dialog, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.full_edit_card_dialog = full_edit_card_dialog
        self.setup_ui()

    def setup_ui(self) -> None:
        """Setup the editor UI."""
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.text_edit = MinimalSizeTextEdit(
                parent=self, disable_enter=True)
        self.text_edit.setStyleSheet(
                f"""QTextEdit {{
                        background-color: {__PALETTE__['dark_gray']};
                        border: 3px solid {__PALETTE__['purple']};
                        border-radius: 8px;
                        }}
                """)
        self.text_edit.pressed_enter.connect(self.save)
        self.layout().addWidget(self.text_edit)
        # shortcuts
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(self.cancel)
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self)
        self.enter_shortcut.activated.connect(self.save)
        self.return_shortcut = QShortcut(Qt.Key_Return, self)
        self.return_shortcut.activated.connect(self.save)
        self.setup_buttons()

    def setup_buttons(self) -> None:
        """Setup the editor buttons."""
        self.buttons_container = QFrame(parent=self)
        self.buttons_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.buttons_container)
        self.save_button = QPushButton(
                text="Save", parent=self.buttons_container,
                objectName="purple_btn",
                )
        self.save_button.setFixedSize(75, __BUTTON_HEIGHT_MEDIUM__)
        self.buttons_container.layout().addWidget(
                self.save_button, alignment=Qt.AlignLeft)
        self.save_button.clicked.connect(self.save)
        self.cancel_button = SmallCloseButton(parent=self.buttons_container)
        self.buttons_container.layout().addWidget(
                self.cancel_button)
        self.buttons_container.layout().addStretch()
        self.cancel_button.clicked.connect(self.cancel)

    def get_title(self):
        """Get the title stored in text edit."""
        return self.text_edit.toPlainText()

    def set_title(self, text: str) -> None:
        """Set the title in text edit."""
        self.text_edit.setPlainText(text)

    def setFocus(self) -> None:
        """Set the focus on current widget."""
        self.text_edit.setFocus()
        self.text_edit.selectAll()

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing widget."""
        self.escape_shortcut.setEnabled(True)
        self.return_shortcut.setEnabled(True)
        self.enter_shortcut.setEnabled(True)
        # need to disable the escape from full dialog
        self.full_edit_card_dialog.escape_shortcut.setEnabled(False)
        return super().showEvent(*args, **kwargs)

    def hideEvent(self, *args, **kwargs) -> Any:
        """Called when hiding widget."""
        self.escape_shortcut.setEnabled(False)
        self.return_shortcut.setEnabled(False)
        self.enter_shortcut.setEnabled(False)
        # need to reenable the escape from full dialog
        self.full_edit_card_dialog.escape_shortcut.setEnabled(True)
        return super().hideEvent(*args, **kwargs)

    def hide(self, *args, from_parent: bool = False, **kwargs) -> None:
        """Hide the checklist item editor."""
        if not from_parent:
            # need to hide using parent function
            self.parent().hide_editor()
            return
        return super().hide(*args, **kwargs)
