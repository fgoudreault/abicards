"""The card-related routines submodule."""
from typing import Sequence

from PySide6.QtCore import QObject, QRect


def get_index_of_hovering_widget_above_list(
        target_rect: QRect,
        widget: QObject,
        lst: Sequence[QObject],
        ) -> int:
    """Compute the index of a widget if it would be in the list it hovers.

    This routine assumes every widget in the list is currently shown.

    Parameters
    ----------
    target_rect: QRect
        The target geometry where the widget is currently trying to move.
    widget: QObject
        The hovering widget.
    lst: Sequence[QObject]
        The list of widgets under the hovering widget.
    """
    cy = widget.parent().mapToGlobal(target_rect.center()).y()
    cys = [cy]
    for item in lst:
        cys.append(
                item.parent().mapToGlobal(item.geometry().center()).y()
                )
    sort = list(sorted(cys))
    return sort.index(cy)
