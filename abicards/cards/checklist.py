"""Checklist module."""
import logging
from typing import Any, Sequence

from PySide6.QtCore import QPoint, QRect, Qt, Signal
from PySide6.QtGui import QShortcut
from PySide6.QtWidgets import (
        QFrame, QGridLayout, QHBoxLayout, QLabel, QLineEdit, QProgressBar,
        QPushButton, QVBoxLayout, QWidget,
        )

from .checklist_item import ChecklistItem, FullEditCardDialogChecklistItem
from .convert import ConvertToDialog, ConvertibleObject
from .dates import DatableObject, Dates, DatesPickerDialog
from .duration import Duration, DurationObject, DurationSetterDialog
from .routines import get_index_of_hovering_widget_above_list
from ..custom_widgets import (
        DotsButton, HeightForWidthLabel, MinimalSizeTextEdit, SmallCloseButton,
        )
from ..mixins import IndicatedObject
from ..routines import (
        build_qss, get_icon_pixmap, parse_qss,
        set_widget_font, unsaved_changes,
        )
from ..styles_constants import (
        __BUTTON_HEIGHT_BIG__, __BUTTON_HEIGHT_MEDIUM__,
        __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__,
        __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__,
        __LABEL_HEIGHT_SMALL__,
        __PALETTE__,
        )


class Checklist(
        QWidget, DatableObject, DurationObject, IndicatedObject,
        ConvertibleObject,
        ):
    """A checklist object."""

    has_dates_indicator = True
    checklist_updated = Signal()

    def __init__(self, card, *args, **kwargs):
        self.card = card
        self.card_list = card.card_list
        QWidget.__init__(self, *args, **kwargs)
        DatableObject.__init__(self, *args, **kwargs)
        DurationObject.__init__(
                self, *args, fixed_by_children_duration=True, **kwargs)
        IndicatedObject.__init__(
                self, *args, indicator_alignment="right", **kwargs)
        self._title = ""
        self._logger = logging.getLogger("Checklist")
        self.checklist_items = []
        self.checked_items_hidden = False
        self.dates_picker_dialog = None
        self.board = self.card.board
        self.full_edit_card_dialog_widget = None
        self.completed_changed.connect(self.checklist_updated)
        self.duration_changed.connect(self.checklist_updated)

    def __repr__(self) -> str:
        """The checklist string representation."""
        return f"< Checklist: '{self.title}' in card '{self.card.title}' >"

    def can_be_converted(self) -> bool:
        """Return true if checklist can be converted."""
        if self.checklist_items:
            return False
        return True

    @property
    def title(self) -> str:
        """The checklist title."""
        return self._title

    @unsaved_changes
    def set_title(self, title: str, unsaved_changes: bool = True):
        """Set the checklist title."""
        if self.title == title:
            return
        og = self.title
        self._title = title
        if unsaved_changes:
            self._logger.info(
                    f"Setting checklist title '{og}' => '{self.title}'.")
            return True

    @property
    def nchecked_items(self) -> int:
        """The number of checked items."""
        n = 0
        for item in self.checklist_items:
            if item.checked:
                n += 1
        return n

    def get_children_dates(self) -> Sequence[Dates]:
        """Get the list of children dates."""
        return [c.dates for c in self.checklist_items]

    def get_children_duration(self) -> Sequence[Duration]:
        """Get the list of children duration."""
        return [c.duration for c in self.checklist_items]

    def get_dates_picker_dialog(self) -> DatesPickerDialog:
        """Get the dates picker dialog."""
        return self.dates_picker_dialog

    def get_indicators(self) -> Sequence[QWidget]:
        """Get the list of indicators."""
        return (DatableObject.get_indicators(self) +
                DurationObject.get_indicators(self))

    def update_indicators(self) -> None:
        """Update all indicators."""
        DatableObject.update_indicators(self)
        DurationObject.update_indicators(self)

    def set_dates(self, *args, **kwargs) -> Any:
        """Set the due dates for this checklist."""
        rtn = DatableObject.set_dates(self, *args, **kwargs)
        if self.dates is not None:
            self.dates.dates_changed.connect(self.checklist_updated)
        self.checklist_updated.emit()
        return rtn

    def get_json_data(self) -> dict[str, Any]:
        """Return json data for this checklist for storage."""
        data = DatableObject.get_json_data(self)
        data.update(DurationObject.get_json_data(self))
        data.update({
            "title": self.title,
            "checklist_items": [
                it.get_json_data() for it in self.checklist_items],
            "checked_items_hidden": self.checked_items_hidden,
            })
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load checklist from a json object."""
        self.set_title(data["title"], unsaved_changes=False)
        if "checklist_items" in data:
            for cli_data in data["checklist_items"]:
                self.add_checklist_item(
                        load_data=cli_data, unsaved_changes=False)
        if "checked_items_hidden" in data:
            self.set_checked_items_hidden(
                    data["checked_items_hidden"],
                    unsaved_changes=False)
        DatableObject.load_json_data(self, data)
        DurationObject.load_json_data(self, data)

    @unsaved_changes
    def add_checklist_item(
            self, *args,
            load_data: dict[str, Any] = None,
            unsaved_changes: bool = True,
            **kwargs) -> None:
        """Add a checklist item."""
        item = ChecklistItem(*args, parent=self, **kwargs)
        if load_data is not None:
            item.load_json_data(load_data)
        item.checklist_item_updated.connect(self.checklist_updated)
        item.checklist_item_updated.connect(self.update_duration)
        item.checklist_item_updated.connect(self.update_dates)
        item.checklist_item_updated.connect(self.update_indicators)
        self.checklist_items.append(item)
        if unsaved_changes:
            self._logger.info(
                    f"Adding checklist item named '{item.title}' to "
                    f"checklist '{self.title}'.")
            return True

    def delete(self) -> None:
        """Delete the checklist."""
        self.checklist_updated.emit()
        fd = self.full_edit_card_dialog_widget
        if fd is not None:
            fd.remove_checklist_widget()

    @unsaved_changes
    def insert_checklist_item(
            self, index: int, item: ChecklistItem) -> None:
        """Insert a checklist item."""
        self._logger.info(
                f"Inserting {item} into {self} at index {index}.")
        self.checklist_items.insert(index, item)
        self.checklist_updated.emit()
        return True

    @unsaved_changes
    def remove_checklist_item(self, item: ChecklistItem) -> None:
        """Delete a checklist item."""
        self.checklist_items.remove(item)
        self.checklist_updated.emit()
        self._logger.info(f"Removing '{item.title}' from '{self.title}'.")
        return True

    @unsaved_changes
    def set_checked_items_hidden(
            self, hidden: bool, unsaved_changes: bool = True) -> None:
        """Set the checked_items_hidden property."""
        if hidden is self.checked_items_hidden:
            return
        self.checked_items_hidden = hidden
        if unsaved_changes:
            self._logger.info(
                    f"Setting hidden checked items to {hidden} "
                    f"in '{self.title}'.")
            return True

    def remove_dates(self) -> None:
        """Remove the dates from the checklist."""
        DatableObject.remove_dates(self)
        self.checklist_updated.emit()

    def setup_indicators(self, *args, **kwargs) -> None:
        """Setup the checklist indicators."""
        self.setup_dates_indicator()
        self.setup_duration_indicator()
        IndicatedObject.setup_indicators(self, *args, **kwargs)


__ADD_ITEM_TEXT_EDIT_QSS__ = f"""
    MinimalSizeTextEdit#add_item_text_edit {{
        background-color: rgb(30, 30, 30);
        border: 3px solid {__PALETTE__['purple']};
        border-radius: 5px;
        }}
"""


class CustomColorProgressBar(QProgressBar):
    """Progress bar with custom colors depending of value."""

    def __init__(self, *args, **kwargs):
        super().__init__(
                *args, objectName="custom_color_progress_bar", **kwargs)
        self.colors = None
        self.style_sheets = None

    def set_colors(self, colors: dict[float | int, str]) -> None:
        """Set custom colors dict.

        The custom colors is a dict whose keys are the lower bound
        upon which the color is set. For exemple, {1: 'green'} whould
        set all values >= 1 to the color green while {1: 'green', 20: 'blue'}
        would set all values beteen >= 1 and < 20 to green and all
        values >20 as blue.
        """
        # sort colors with their values
        keys = sorted(colors.keys())
        self.colors = {key: colors[key] for key in keys}

    def setValue(self, value: float | int):
        """Set the progressbar value and update color."""
        if self.colors is None:
            return super().setValue(value)
        # set new style
        currcolor = None
        for val, color in self.colors.items():
            if value >= val:
                currcolor = color
        ss = self.styleSheet()
        objectname, ssdict = parse_qss(ss)
        if not objectname:
            objectname = "QProgressBar#custom_color_progress_bar::chunk"
        ssdict["background"] = currcolor
        newss = build_qss(objectname, ssdict)
        self.setStyleSheet(newss)
        return super().setValue(value)


class FullEditDialogChecklistField(QFrame):
    """Widget class associated to a checklist."""

    def __init__(
            self, full_edit_card_dialog, checklist: Checklist,
            *args, **kwargs) -> None:
        super().__init__(
                *args,
                objectName="FullEditCardDialogChecklist",
                **kwargs)
        self.checklist = checklist
        self.checklist_icon = get_icon_pixmap("checklist")
        self.checklist.full_edit_card_dialog_widget = self
        self.checklist_item_widgets = []
        self.full_edit_card_dialog = full_edit_card_dialog
        self.card = self.full_edit_card_dialog.card
        self.board = self.full_edit_card_dialog.board
        self.setup_ui()
        self.add_item_widget = None
        self.add_button = None
        self.add_item_button = None
        self.cancel_button = None
        self.edit_dialog = None
        self.ghost_checklist = None

    def __repr__(self) -> str:
        """The string representation of the widget."""
        return f"< FullEditDialogChecklistField: {self.checklist} >"

    @property
    def checked_items_hidden(self):
        """Return true if we hide checked items."""
        return self.checklist.checked_items_hidden

    @property
    def nchecked_items(self) -> int:
        """Return the number of checked items."""
        return self.checklist.nchecked_items

    def pop_from_parent(self, newparent):
        """Pop from the parent full edit card dialog."""
        self.place_ghost_item()
        if hasattr(self.parent(), "checklist_fields"):
            if self in self.parent().checklist_fields:
                self.parent().checklist_fields.remove(self)
        self.setParent(newparent)
        self.set_dragging_style_sheet()

    def place_ghost_item(self):
        """Place a ghost item in lieu of the checklist when dragging."""
        if self.ghost_checklist is None:
            self.ghost_checklist = QFrame(
                    parent=self.parent(),
                    objectName="ghost",
                    )
        self.ghost_checklist.setFixedSize(self.size())
        self.ghost_checklist.parent().layout().replaceWidget(
                self, self.ghost_checklist)
        self.ghost_checklist.show()

    def remove_ghost_checklist(
            self, return_ghost_checklist_index: bool = False,
            ) -> bool | None:
        """Remove the ghost checklist.

        Parameters
        ----------
        return_ghost_checklist_index: bool, optional
            If True, return the index of the ghost checklist in its current
            parent.

        Returns
        -------
        int: If 'return_ghost_checklist_index' is True.
        None: otherwise or If ghost checklist does not exists.
        """
        if self.ghost_checklist is None:
            return
        self.setParent(self.ghost_checklist.parent())
        rtn = None
        if return_ghost_checklist_index:
            rtn = self.parent().layout().indexOf(self.ghost_checklist)
        self.parent().layout().replaceWidget(self.ghost_checklist, self)
        self.ghost_checklist.hide()
        return rtn

    def handle_drag_move(self, target_rect: QRect) -> None:
        """Called when dragging the checklist at each move event."""
        # check what would be the idx of the checklist item
        idx = get_index_of_hovering_widget_above_list(
                target_rect, self,
                self.ghost_checklist.parent().checklist_fields)
        offset = 2  # description + indicators field
        # insert ghost in its stead
        self.ghost_checklist.parent().layout().removeWidget(
                self.ghost_checklist)
        self.ghost_checklist.parent().layout().insertWidget(
                idx + offset, self.ghost_checklist)
        self.set_dragging_style_sheet()

    @unsaved_changes
    def put_into_new_parent(self):
        """Put back the checklist into new place."""
        # get index of ghost item
        idx = self.remove_ghost_checklist(return_ghost_checklist_index=True)
        # new parent set when removing ghost. change checklist item from
        offset = 2  # description + indicators field
        self.parent().layout().insertWidget(idx, self)
        self.parent().checklist_fields.insert(idx, self)
        self.card.checklists.remove(self.checklist)
        self.card.checklists.insert(idx - offset, self.checklist)
        # if self.underMouse():
        #     self.set_hovered_style_sheet()
        # else:
        self.set_original_style_sheet()
        return True

    def remove_checklist_widget(self) -> None:
        """Remove the checklist widget from parent."""
        self.full_edit_card_dialog.remove_checklist_widget(self)

    def setup_ui(self):
        """Setup the ui."""
        self.setLayout(QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.setContentsMargins(10, 0, 0, 0)
        self.setup_icon()
        self.contents = QFrame(parent=self)
        self.contents.setContentsMargins(0, 0, 0, 0)
        self.contents.setLayout(QVBoxLayout())
        self.contents.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self.contents)
        self.setup_header()
        self.checklist.setup_indicators(
                parent=self, layout=self.contents.layout())
        # progress bar ui
        self.setup_progress_bar_ui()
        # checklist items
        self.setup_checklist_items()
        self.setup_bottom_buttons()
        # if self.underMouse():
        #     self.set_hovered_style_sheet()
        # else:
        self.set_original_style_sheet()

    def setup_icon(self) -> None:
        """Setup the icon column."""
        self.icon_container = QFrame(parent=self)
        self.icon_container.setLayout(QVBoxLayout())
        self.icon_container.setContentsMargins(0, 20, 0, 0)
        self.icon_container.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self.icon_container)
        self.icon_label = HeightForWidthLabel(
                height_factor=1.13,
                parent=self.icon_container)
        self.icon_label.setPixmap(self.checklist_icon)
        self.icon_label.setScaledContents(True)
        self.icon_label.setFixedWidth(15)
        self.icon_container.layout().addWidget(self.icon_label)
        self.icon_container.layout().addStretch()

    def setup_bottom_buttons(self):
        """Setup the bottom buttons."""
        # add item button
        self.bottom_buttons_container = QFrame(parent=self)
        self.bottom_buttons_container.setLayout(QHBoxLayout())
        self.contents.layout().addWidget(self.bottom_buttons_container)
        self.add_item_btn = QPushButton(
                text="Add an item",
                parent=self.bottom_buttons_container,
                objectName="light_gray_btn_center",
                )
        self.bottom_buttons_container.layout().addWidget(
                self.add_item_btn, alignment=Qt.AlignLeft)
        self.add_item_btn.setFixedWidth(100)
        self.add_item_btn.clicked.connect(self.toggle_add_item_mode)

    def setup_progress_bar_ui(self):
        """Setup progress bar ui."""
        # left is percentage label for completion
        self.progress_bar_container = QFrame(parent=self)
        self.progress_bar_container.setContentsMargins(0, 0, 0, 0)
        self.progress_bar_container.setLayout(QHBoxLayout())
        self.contents.layout().addWidget(self.progress_bar_container)
        self.progress_percentage_label = QLabel(
                "0%", parent=self.progress_bar_container,
                )
        self.progress_percentage_label.setContentsMargins(0, 0, 0, 0)
        set_widget_font(
                self.progress_percentage_label, point_size=10)
        self.progress_percentage_label.setFixedSize(
                __FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__,
                __LABEL_HEIGHT_SMALL__,
                )
        self.progress_bar_container.layout().addWidget(
                self.progress_percentage_label, alignment=Qt.AlignLeft)
        self.progress_bar = CustomColorProgressBar(parent=self)
        self.reset_progressbar()
        self.progress_bar.setTextVisible(False)  # we have a specific label for
        self.progress_bar.setFixedHeight(10)
        self.progress_bar_container.layout().addWidget(self.progress_bar)

    def setup_checklist_items(self) -> None:
        """Setup the checklist items."""
        self.checklist_items_container = QFrame(parent=self)
        self.checklist_items_container.setLayout(QVBoxLayout())
        self.contents.layout().addWidget(self.checklist_items_container)
        for item in self.checklist.checklist_items:
            self.add_item(item)

    def setup_header(self) -> None:
        """Setup the header part of the UI."""
        self.header = QFrame(parent=self)
        self.contents.layout().addWidget(self.header)
        self.header.setLayout(QHBoxLayout())
        # now add title label
        # it is a button which opens as a text edit to change title
        self.title_widget = ChecklistTitleWidget(
                self.checklist, self.full_edit_card_dialog,
                parent=self.header,
                )
        self.title_widget.setText(self.checklist.title)
        self.header.layout().addWidget(self.title_widget)
        # hide checked items button (hidden if no items checked)
        self.hide_button = QPushButton(
                text="Hide checked items", parent=self.header,
                objectName="light_gray_btn_center")
        self.hide_button.clicked.connect(self.hide_checked_items)
        self.header.layout().addWidget(self.hide_button)
        self.hide_button.setFixedHeight(
                __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__)
        self.hide_button.hide()
        # edit button
        self.edit_button = DotsButton(parent=self.header)
        self.header.layout().addWidget(self.edit_button, Qt.AlignRight)
        self.edit_button.clicked.connect(self.toggle_edit_menu)
        self.edit_button.setFixedSize(40, 30)

    def toggle_add_item_mode(self):
        """Turn the add item mode on."""
        # turn into add item mode
        # replace the add item button with a textbox
        if self.add_item_widget is None:
            self.add_item_widget = AddItemWidget(
                    parent=self.bottom_buttons_container)
            self.add_item_widget.add_item.connect(self.user_add_item)
            self.add_item_widget.cancel_item.connect(self.cancel_item)
        if self.add_item_widget.isVisible():
            self.add_item_widget.hide()
            self.add_item_btn.show()
            self.bottom_buttons_container.layout().removeWidget(
                    self.add_item_widget)
            self.bottom_buttons_container.layout().addWidget(
                    self.add_item_btn, alignment=Qt.AlignLeft)
            return
        self.add_item_btn.hide()
        self.bottom_buttons_container.layout().removeWidget(self.add_item_btn)
        self.bottom_buttons_container.layout().addWidget(self.add_item_widget)
        self.add_item_widget.show()

    def add_item(self, item: ChecklistItem):
        """Add a checklist item widget to the checklist."""
        itmwidget = FullEditCardDialogChecklistItem(
                self.full_edit_card_dialog, item,
                parent=self.checklist_items_container)
        self.checklist_item_widgets.append(itmwidget)
        self.checklist_items_container.layout().addWidget(itmwidget)
        itmwidget.checked.connect(self.update_progressbar)
        itmwidget.unchecked.connect(self.update_progressbar)
        # itmwidget.edit.connect(self.edit_checklist_item)
        # check if checkbox is already checked upon creation
        self.update_hide_button()
        self.update_progressbar()

    def number_checked_items(self) -> int:
        """The number of checked items."""
        n_hidden = 0
        for widget in self.checklist_item_widgets:
            if widget.is_checked():
                n_hidden += 1
        return n_hidden

    def update_hide_button(self):
        """Update the hide button text and if we show it or not."""
        n_hidden = self.number_checked_items()
        if not n_hidden:
            # hide button
            self.hide_button.hide()
            return
        self.hide_button.show()
        if self.checklist.checked_items_hidden:
            self.hide_button.setText(
                    f"Show checked items ({n_hidden})")
        else:
            self.hide_button.setText("Hide checked items")

    def any_item_checked(self) -> bool:
        """Return True if at least 1 item is checked."""
        for item in self.checklist_item_widgets:
            if item.is_checked():
                return True
        return False

    def hide_checked_items(self, *args, invert_selection: bool = True) -> None:
        """Hide or unhide checked items."""
        if invert_selection:
            self.checklist.set_checked_items_hidden(
                    not self.checklist.checked_items_hidden)
        if not self.checklist.checked_items_hidden:
            # invert selection and show checkd items
            for widget in self.checklist_item_widgets:
                widget.show()
        else:
            # hide checked items
            for widget in self.checklist_item_widgets:
                if widget.is_checked():
                    widget.hide()
        self.update_hide_button()

    def update_progressbar(self):
        """Update the progress bar."""
        # update progressbar with +/- delta
        tot = len(self.checklist_item_widgets)
        self.hide_checked_items(invert_selection=False)
        self.update_hide_button()
        if tot == 0:
            self.reset_progressbar()
            self.progress_percentage_label.setText("0%")
            return
        self.progress_bar.setMaximum(tot)
        self.progress_bar.set_colors(
                {tot: __PALETTE__["green"], 0: __PALETTE__["purple"]})
        self.progress_bar.setValue(self.nchecked_items)
        # compute new percentage
        percentage = int(round(self.progress_bar.value() / tot * 100, 0))
        self.progress_percentage_label.setText(f"{percentage}%")

    def reset_progressbar(self):
        """Reset the progress bar."""
        self.progress_bar.setMinimum(0)
        self.progress_bar.setMaximum(1)
        self.progress_bar.setValue(0)

    def delete_checklist(self):
        """Delete the checklist."""
        self.parent().delete_checklist(self)
        if self.edit_dialog is None:
            return
        if self.edit_dialog.delete_checklist_dialog is None:
            return
        self.edit_dialog.delete_checklist_dialog.close()
        self.edit_dialog.close()

    def delete_checklist_item(
            self, item: FullEditCardDialogChecklistItem) -> None:
        """Delete the given checklist item from the checklist."""
        self.checklist.remove_checklist_item(item.checklist_item)
        self.remove_checklist_item_widget(item)

    def remove_checklist_item_widget(self, item) -> None:
        """Remove the checklist item widget."""
        if isinstance(item, ChecklistItem):
            for widget in self.checklist_item_widgets:
                if widget.checklist_item is item:
                    item = widget
                    break
            else:
                # this checklist item does not belong here
                return
        self.checklist_item_widgets.remove(item)
        item.hide()
        self.contents.layout().removeWidget(item)
        item.destroy()
        self.update_progressbar()
        self.checklist.update_dates()
        self.checklist.update_indicators()

    def user_add_item(self):
        """Add a checklist item."""
        itemname = self.add_item_widget.get_item_title()
        self.checklist.add_checklist_item(title=itemname)
        # add item widget
        self.add_item(self.checklist.checklist_items[-1])
        self.add_item_widget.reset_text()
        # self.cancel_item(reset_placeholder_text=True)
        self.add_item_widget.setFocus()
        self.update_progressbar()

    def cancel_item(self, reset_placeholder_text: bool = False):
        """Cancel the item."""
        # remove line edit
        if reset_placeholder_text:
            self.add_item_widget.reset_placeholder_text()
        self.toggle_add_item_mode()

    def toggle_edit_menu(self) -> None:
        """Open the edit menu."""
        if self.edit_dialog is None:
            self.edit_dialog = ChecklistEditDialog(
                    self.checklist,
                    self.full_edit_card_dialog,
                    parent=self.full_edit_card_dialog.full_edit_card_widget,
                    )
            self.edit_dialog.delete.connect(self.delete_checklist)
        self.edit_dialog.move(
                self.full_edit_card_dialog.full_edit_card_widget.mapFromGlobal(
                    self.edit_button.parent().mapToGlobal(
                        self.edit_button.geometry().topRight())))
        # if already visible, hide
        if self.edit_dialog.isVisible():
            self.edit_dialog.hide_all()
        else:
            self.edit_dialog.show()

    def enterEvent(self, *args, **kwargs) -> Any:
        """Called when entering checklist widget."""
        # self.set_hovered_style_sheet()
        return super().enterEvent(*args, **kwargs)

    def leaveEvent(self, *args, **kwargs) -> Any:
        """Called when mouse leaves the checklist widget."""
        self.set_original_style_sheet()
        return super().leaveEvent(*args, **kwargs)

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing widget."""
        self.checklist.update_dates()
        self.checklist.update_indicators()
        return super().showEvent(*args, **kwargs)

    # def set_hovered_style_sheet(self):
    #     """Set the hovered style sheet."""
    #     self.setStyleSheet(
    #             f"""QFrame#FullEditCardDialogChecklist {{
    #                     background-color: {__PALETTE__['medium_dark_gray']};
    #                     border: 1px solid transparent;
    #                     border-radius: 5px;
    #                 }}
    #             """)

    def set_original_style_sheet(self):
        """Set the passive style sheet."""
        self.setStyleSheet(
                f"""QFrame#FullEditCardDialogChecklist {{
                        background-color: {__PALETTE__['dark_gray']};
                        border: 1px solid transparent;
                        border-radius: 5px;
                    }}
                """)

    def set_dragging_style_sheet(self):
        """Set the dragging style sheet."""
        self.setStyleSheet(
                f"""QFrame#FullEditCardDialogChecklist {{
                        background-color: {__PALETTE__['dark_gray']};
                        border: 1px solid {__PALETTE__['green']};
                        border-radius: 5px;
                    }}
                """)

    def close_subdialogs(self) -> None:
        """Close subdialogs."""
        self.title_widget.cancel()
        if self.add_item_widget is not None:
            if self.add_item_widget.isVisible():
                self.toggle_add_item_mode()


class ChecklistTitleWidget(QFrame):
    """The checklist title widget.

    Acts as a button which changes to a lineedit when clicked and realeased.
    """

    def __init__(
            self, checklist: Checklist, full_card_edit_dialog,
            *args, **kwargs) -> None:
        self.checklist = checklist
        self.full_card_edit_dialog = full_card_edit_dialog
        super().__init__(*args, **kwargs)
        self.setup_ui()
        self.title_lineedit = None
        self.enter_shortcut = None
        self.return_shortcut = None
        self.escape_shortcut = None

    def setup_ui(self) -> None:
        """Setup the checklist title ui."""
        self.setLayout(QHBoxLayout())
        self.title_label = QLabel(parent=self)
        self.layout().addWidget(self.title_label)
        set_widget_font(self.title_label, bold=True)
        if self.underMouse():
            self.set_hovered_style()
        else:
            self.set_original_style()

    def save(self) -> None:
        """Save the new title."""
        self.checklist.set_title(self.title_lineedit.text())
        self.setText(self.checklist.title)
        self.toggle_edit_mode()

    def cancel(self) -> None:
        """Cancel title edits."""
        if self.title_lineedit is None:
            return
        self.toggle_edit_mode()

    def setText(self, *args, **kwargs) -> None:
        """Set the text of the title."""
        self.title_label.setText(*args, **kwargs)

    def toggle_edit_mode(self) -> None:
        """Enter or leave edit title mode."""
        if self.title_lineedit is None:
            self.title_lineedit = QLineEdit(parent=self)
        if self.title_lineedit.isVisible():
            self.title_lineedit.hide()
            self.layout().removeWidget(self.title_lineedit)
            self.layout().addWidget(self.title_label)
            self.disable_shortcuts()
            self.title_label.show()
            return
        self.enable_shortcuts()
        self.title_label.hide()
        self.title_lineedit.setText(self.checklist.title)
        self.title_lineedit.setFocus()
        self.title_lineedit.selectAll()
        self.layout().removeWidget(self.title_label)
        self.layout().addWidget(self.title_lineedit)
        self.title_lineedit.show()

    def enable_shortcuts(self) -> None:
        """Enable shortcuts."""
        if self.enter_shortcut is None:
            self.enter_shortcut = QShortcut(Qt.Key_Enter, self)
            self.enter_shortcut.activated.connect(self.save)
        self.enter_shortcut.setEnabled(True)
        if self.return_shortcut is None:
            self.return_shortcut = QShortcut(Qt.Key_Return, self)
            self.return_shortcut.activated.connect(self.save)
        self.return_shortcut.setEnabled(True)
        if self.escape_shortcut is None:
            self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
            self.escape_shortcut.activated.connect(self.cancel)
        # need to disable parent's shortcut
        self.full_card_edit_dialog.escape_shortcut.setEnabled(False)
        self.escape_shortcut.setEnabled(True)

    def disable_shortcuts(self) -> None:
        """Disable shortcuts."""
        self.enter_shortcut.setEnabled(False)
        self.return_shortcut.setEnabled(False)
        self.escape_shortcut.setEnabled(False)
        self.full_card_edit_dialog.escape_shortcut.setEnabled(True)

    def mouseReleaseEvent(self, event, *args, **kwargs) -> Any:
        """Called when mouse is released."""
        self.toggle_edit_mode()
        return super().mouseReleaseEvent(event, *args, **kwargs)

    def enterEvent(self, *args, **kwargs) -> None:
        """Called when mouse enters widget."""
        self.set_hovered_style()
        return super().enterEvent(*args, **kwargs)

    def leaveEvent(self, *args, **kwargs) -> None:
        """Called when mouse leaves widget."""
        self.set_original_style()
        return super().leaveEvent(*args, **kwargs)

    def set_hovered_style(self):
        """Set the hovered style sheet."""
        self.setStyleSheet(
                f"""QFrame {{
                        background-color: {__PALETTE__['gray']};
                        border: 1px solid transparent;
                        border-radius: 5px;
                    }}
                """)

    def set_original_style(self):
        """Set the passive style sheet."""
        self.setStyleSheet(
                f"""QFrame {{
                        background-color: {__PALETTE__['dark_gray']};
                        border: 1px solid transparent;
                        border-radius: 5px;
                    }}
                """)


class AddItemWidget(QFrame):
    """Widget to add an item to a checklist."""

    add_item = Signal()
    cancel_item = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setup_ui()

    def set_item_title(self, title: str) -> None:
        """Set the item title."""
        self.text_edit.setPlainText(title)

    def get_item_title(self) -> str:
        """Get the checklist item title."""
        return self.text_edit.toPlainText()

    def reset_placeholder_text(self) -> None:
        """Reset the placeholder text."""
        self.text_edit.setPlaceholderText("Add an item")

    def reset_text(self) -> None:
        """Reset the text of the widget."""
        self.text_edit.setText("")

    def setFocus(self, *args, **kwargs) -> None:
        """Set the focus on the text edit."""
        self.text_edit.setFocus(*args, **kwargs)

    def setup_ui(self):
        """Setup the add item widget ui."""
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.setup_text_edit()
        self.setup_buttons()

    def setup_text_edit(self):
        """Setup the text edit."""
        # put text edit inside a colored frame
        self.text_edit = MinimalSizeTextEdit(
                    parent=self, objectName="add_item_text_edit",
                    disable_enter=True)
        self.text_edit.setStyleSheet(__ADD_ITEM_TEXT_EDIT_QSS__)
        self.text_edit.setPlainText("")
        self.reset_placeholder_text()
        self.text_edit.setFocus()
        self.text_edit.pressed_enter.connect(self.add_item)
        self.text_edit.pressed_escape.connect(self.cancel_item)
        self.layout.addWidget(self.text_edit)

    def setup_buttons(self):
        """Setup the buttons."""
        # multiple buttons -> place them in a sub layout
        self.buttons_container = QFrame(parent=self)
        self.buttons_layout = QHBoxLayout()
        self.buttons_container.setLayout(self.buttons_layout)
        self.layout.addWidget(self.buttons_container)
        self.add_btn = QPushButton(
                text="Add", parent=self.buttons_container,
                objectName="purple_btn",
                )
        self.add_btn.setFixedWidth(50)
        self.add_btn.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.add_btn.clicked.connect(self.add_item)
        self.buttons_layout.addWidget(self.add_btn)
        self.cancel_btn = QPushButton(
                text="Cancel", parent=self.buttons_container,
                objectName="transparent_btn",
                )
        self.cancel_btn.clicked.connect(self.cancel_item)
        self.cancel_btn.setFixedSize(75, __BUTTON_HEIGHT_MEDIUM__)
        self.buttons_layout.addWidget(self.cancel_btn)
        self.buttons_layout.addStretch()


class ChecklistEditDialog(QFrame):
    """Edit checklist dialog."""

    delete = Signal()

    def __init__(
            self, checklist, full_card_edit_dialog, *args, **kwargs) -> None:
        super().__init__(*args, objectName="dark_grey_dialogs", **kwargs)
        self.checklist = checklist
        self.full_card_edit_dialog = full_card_edit_dialog
        self.delete_checklist_dialog = None
        self.dates_picker_dialog = None
        self.duration_setter_dialog = None
        self.convert_to_dialog = None
        self.setup_ui()

    @property
    def subdialogs(self) -> Sequence:
        """The checklist edit subdialogs."""
        subs = [
                self.delete_checklist_dialog, self.dates_picker_dialog,
                self.duration_setter_dialog,
                self.convert_to_dialog,
                ]
        if self.convert_to_dialog is not None:
            subs += self.convert_to_dialog.subdialogs
        return subs

    def setup_ui(self) -> None:
        """Setup the edit checklist dialog ui."""
        self.setLayout(QVBoxLayout())
        self.setup_header()
        self.setup_buttons()

    def setup_buttons(self) -> None:
        """Setup the edit menu buttons."""
        self.dates_button = QPushButton(
                text="Dates", parent=self,
                objectName="light_gray_btn_center",
                )
        self.dates_button.clicked.connect(self.open_dates_picker_dialog)
        self.dates_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.dates_button)
        # duration
        self.duration_button = QPushButton(
                text="Duration", parent=self,
                objectName="light_gray_btn_center",
                )
        self.duration_button.clicked.connect(self.open_duration_setter_dialog)
        self.duration_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.duration_button)
        # convert
        self.convert_button = QPushButton(
                text="Convert", parent=self,
                objectName="light_gray_btn_center",
                )
        self.convert_button.clicked.connect(self.open_convert_to_dialog)
        self.convert_button.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
        self.layout().addWidget(self.convert_button)
        # delete
        self.delete_button = QPushButton(
                text="Delete", parent=self,
                objectName="red_btn")
        self.delete_button.clicked.connect(
                self.show_delete_checklist_dialog,
                )
        self.layout().addWidget(self.delete_button)
        self.delete_button.setFixedHeight(
                __BUTTON_HEIGHT_MEDIUM__)

    def setup_header(self) -> None:
        """Setup the checklist edit dialog ui header."""
        # title + close button
        self.header_container = QFrame(parent=self)
        self.layout().addWidget(self.header_container)
        self.header_container.setLayout(QHBoxLayout())
        self.title_label = QLabel(
                text="Edit checklist", parent=self.header_container)
        self.header_container.layout().addWidget(self.title_label)
        set_widget_font(self.title_label, bold=True)
        self.header_container.layout().addStretch()
        # close btn
        self.close_button = SmallCloseButton(parent=self)
        self.close_button.clicked.connect(self.hide)
        self.header_container.layout().addWidget(self.close_button)

    def show_delete_checklist_dialog(self):
        """Delete the checklist."""
        # create a prompt to confirm
        if self.delete_checklist_dialog is None:
            self.delete_checklist_dialog = DeleteChecklistDialog(
                    parent=self.parent(),
                    )
            self.delete_checklist_dialog.delete_confirm.connect(
                    self.delete)
        self.delete_checklist_dialog.show()
        # move dialog under the delete button
        self.delete_checklist_dialog.set_checklist_title(self.checklist.title)
        abs_pos = self.delete_button.mapToGlobal(
                QPoint(-self.delete_checklist_dialog.width(), 0))
        newpos = self.parent().mapFromGlobal(abs_pos)
        self.delete_checklist_dialog.move(
                newpos.x(), newpos.y() + self.delete_button.height() + 3)

    def open_dates_picker_dialog(self) -> None:
        """Open the dates picker dialog."""
        if self.dates_picker_dialog is None:
            self.dates_picker_dialog = DatesPickerDialog(
                    self.checklist.dates,
                    parent=self.parent())
            self.checklist.dates_picker_dialog = self.dates_picker_dialog
            self.dates_picker_dialog.set_dates.connect(
                    self.set_dates)
            self.dates_picker_dialog.remove_dates.connect(
                    self.remove_dates)
            self.dates_picker_dialog.fixed_by_children_changed.connect(
                    self.checklist.update_dates)
        # place dialog under dates button and adjust whole window if necessary
        if self.dates_picker_dialog.isVisible():
            self.dates_picker_dialog.hide()
        else:
            self._place_dialog_under(
                    self.dates_picker_dialog, self.dates_button)

    def open_convert_to_dialog(self) -> None:
        """Open the convert dialog."""
        if self.convert_to_dialog is None:
            self.convert_to_dialog = ConvertToDialog(
                    "checklist", self.checklist,
                    parent=self.parent(),
                    )
            self.convert_to_dialog.converted.connect(
                    self.hide_after_convert)
        self.convert_to_dialog.setEnabled(
                self.checklist.can_be_converted())
        if self.convert_to_dialog.isVisible():
            self.convert_to_dialog.hide()
        else:
            self._place_dialog_under(
                    self.convert_to_dialog, self.convert_button)

    def _place_dialog_under(
            self, dialog, widget, origin="bottom_left") -> None:
        """Move given dialog under given widget."""
        geo = widget.geometry()
        if origin == "bottom_left":
            og = geo.bottomLeft()
        elif origin == "bottom_right":
            og = geo.bottomRight()
        else:
            raise NotImplementedError(origin)
        og = widget.parent().mapToGlobal(og)
        newx = og.x() - dialog.width()
        fd = self.full_card_edit_dialog
        og_rel = fd.full_edit_card_widget.mapFromGlobal(
                    QPoint(newx, og.y()))
        dialog.move(og_rel)
        # adjust parent size if necessary
        if og_rel.y() + dialog.height() > (
                self.full_card_edit_dialog.full_edit_card_widget.height()):
            self.full_card_edit_dialog.full_edit_card_widget.setFixedHeight(
                    og_rel.y() + dialog.height())
        dialog.show()

    def open_duration_setter_dialog(self) -> None:
        """Open the duration setter dialog."""
        # open dialog
        if self.duration_setter_dialog is None:
            # check if any children has duration.
            self.duration_setter_dialog = DurationSetterDialog(
                    self.checklist.duration,
                    parent=self.full_card_edit_dialog.full_edit_card_widget,
                    )
            self.checklist.duration_setter_dialog = (
                    self.duration_setter_dialog)
            self.duration_setter_dialog.save.connect(self.set_duration)
            self.duration_setter_dialog.remove.connect(self.remove_duration)
        self.duration_setter_dialog.init_from_duration_object(self.checklist)
        if self.duration_setter_dialog.isVisible():
            self.duration_setter_dialog.hide()
        else:
            self._place_dialog_under(
                self.duration_setter_dialog, self.duration_button)

    def set_dates(self) -> None:
        """Set the dates to the checklist."""
        self.checklist.set_dates(
                start_date=self.dates_picker_dialog.get_start_date(),
                end_datetime=self.dates_picker_dialog.get_end_datetime(),
                )
        self.hide_all()

    def hide_after_convert(self) -> None:
        """Hide self after converting."""
        self.hide_all()
        self.full_card_edit_dialog.hide()

    def hide_all(self) -> None:
        """Hide self and all dialogs."""
        self.hide()
        for dialog in self.subdialogs:
            if dialog is None:
                continue
            dialog.hide()

    def set_duration(self) -> None:
        """Set the checklist item duration from the duration setter."""
        duration = self.duration_setter_dialog.get_duration()
        self.checklist.set_duration(duration)
        self.hide_all()

    def remove_duration(self) -> None:
        """Remove the duration of the checklist item."""
        self.checklist.remove_duration()
        self.hide_all()

    def remove_dates(self) -> None:
        """Remove the dates from the checklist."""
        self.checklist.remove_dates()
        self.hide_all()

    def showEvent(self, *args, **kwargs):
        """Called when showing the widget."""
        self.adjustSize()
        self.convert_button.setEnabled(self.checklist.can_be_converted())
        return super().showEvent(*args, **kwargs)


class DeleteChecklistDialog(QFrame):
    """Class for the delete checklist dialog."""

    delete_confirm = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(
                *args, objectName="dark_grey_dialogs",
                **kwargs)
        self.setup_ui()

    def set_checklist_title(self, title: str):
        """Set the checklist title."""
        self.title_label.setText(
                f"Delete '{title}' checklist?")

    def setup_ui(self):
        """Setup the dialog ui."""
        # title with x button
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        self.title_label = QLabel(text="Delete checklist?", parent=self)
        self.layout.addWidget(self.title_label, 0, 0, 1, 1)
        self.close_button = SmallCloseButton(parent=self)
        self.close_button.clicked.connect(self.hide)
        self.layout.addWidget(self.close_button, 0, 1, 1, 1)
        # big delete confirm button
        self.delete_button = QPushButton(
                "Confirm delete checklist", parent=self,
                objectName="red_btn",
                )
        self.delete_button.setFixedSize(
                300, __BUTTON_HEIGHT_BIG__)
        self.layout.addWidget(self.delete_button, 1, 0, 1, 2)
        self.delete_button.released.connect(self.delete)
        self.adjustSize()
        self.show()

    def delete(self):
        """Delete the checklist."""
        self.hide()
        self.delete_confirm.emit()


class AddChecklistDialog(QFrame):
    """Dialog to add a checklist when "checklist" button is clicked."""

    add = Signal()
    cancel = Signal()

    def __init__(self, full_edit_card_dialog, *args, **kwargs):
        super().__init__(
                *args,
                objectName="dark_grey_dialogs", **kwargs)
        self.full_edit_card_dialog = full_edit_card_dialog
        self.setup_ui()
        self.add.connect(self.hide)

    def setup_ui(self):
        """Setup the ui."""
        # simple ui with dialog title, some labels and the line edit
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        self.add_checklist_dialog_title_label = QLabel(
                text="Add checklist", parent=self)
        self.add_checklist_dialog_title_label.setStyleSheet(
                "text-align: left",
                )
        self.layout.addWidget(
                self.add_checklist_dialog_title_label, 0, 0, 1, 1)
        # x button to close
        self.close_button = SmallCloseButton(parent=self)
        self.close_button.clicked.connect(self.hide)
        self.layout.addWidget(self.close_button, 0, 1, 1, 1)
        # title label
        self.title_label = QLabel(
             text="Title", parent=self, objectName="small_transparent_label",
             )
        self.title_label.setFixedHeight(__LABEL_HEIGHT_SMALL__)
        self.layout.addWidget(self.title_label, 1, 0, 1, 2)
        # line edit
        self.title_lineedit = QLineEdit(
                parent=self, objectName="dark_gray_background_purple_border")
        self.layout.addWidget(self.title_lineedit, 2, 0, 1, 2)
        self.title_lineedit.setFixedSize(300, 50)
        self.title_lineedit.setText("Checklist")
        # add btn
        self.add_btn = QPushButton(
                parent=self, objectName="purple_btn", text="Add",
                )
        self.add_btn.clicked.connect(self.add)
        self.add_btn.setFixedSize(
                100, __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__)
        self.layout.addWidget(self.add_btn, 3, 0, 1, 1, Qt.AlignLeft)
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self)
        self.enter_shortcut.activated.connect(self.add)
        self.return_shortcut = QShortcut(Qt.Key_Return, self)
        self.return_shortcut.activated.connect(self.add)
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(self.hide)
        self.adjustSize()

    def get_checklist_title(self):
        """Get the checklist title."""
        return self.title_lineedit.text()

    def focus_on_lineedit(self):
        """Focus on the lineedit."""
        self.title_lineedit.selectAll()
        self.title_lineedit.setFocus()

    def hide(self, *args, **kwargs) -> Any:
        """Cancel create checklist."""
        self.cancel.emit()
        self.escape_shortcut.setEnabled(False)
        self.return_shortcut.setEnabled(False)
        self.enter_shortcut.setEnabled(False)
        self.full_edit_card_dialog.escape_shortcut.setEnabled(True)
        return super().hide(*args, **kwargs)

    def show(self, *args, **kwargs) -> Any:
        """Show create checklist."""
        self.full_edit_card_dialog.escape_shortcut.setEnabled(False)
        self.escape_shortcut.setEnabled(True)
        self.return_shortcut.setEnabled(True)
        self.enter_shortcut.setEnabled(True)
        return super().show(*args, **kwargs)
