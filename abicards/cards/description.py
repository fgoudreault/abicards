"""The card description submodule."""
from typing import Any

from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QTextOption
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QPushButton, QVBoxLayout,
        QWidget,
        )

from ..custom_widgets import (
        HeightForWidthLabel, HoverableClickableFrame, MinimalSizeTextEdit,
        )
from ..routines import get_icon_pixmap, set_widget_font
from ..styles_constants import (
        __BUTTON_HEIGHT_BIG__,
        __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__,
        __PALETTE__,
        )


__EMPTY_CARD_DESCRIPTION_BUTTON_QSS__ = """
    QPushButton#empty_card_description_button {
        background-color: rgb(30, 30, 30);
        color: #FFFFFF;
        border: 3px solid transparent;
        border-radius: 8px;
        border-style: outset;
        text-align: top left;
    }
    QPushButton#empty_card_description_button:hover {
        background-color: rgb(20, 20, 20);
}
"""


class FullEditDialogDescriptionField(QFrame):
    """Holds the card description field with its icon."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, objectName="description_field", **kwargs)
        self.card = self.parent().card
        self.setup_ui()

    def setup_ui(self) -> None:
        """Setup the ui."""
        self.setContentsMargins(10, 10, 0, 0)
        self.setLayout(QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.setStyleSheet(
            f"""QFrame#description_field {{
                    background-color: {__PALETTE__['medium_dark_gray']};
                    border-radius: 10px;
            }}"""
            )
        self.setup_icon_ui()
        self.setup_contents_ui()

    def setup_contents_ui(self) -> None:
        """Setup the non-icon contents UI."""
        self.contents = QFrame(parent=self)
        self.contents.setContentsMargins(0, 0, 0, 0)
        self.contents.setLayout(QVBoxLayout())
        self.contents.layout().setContentsMargins(0, 0, 10, 10)
        self.layout().addWidget(self.contents)
        self.setup_title_and_edit_ui()
        self.setup_description_field_ui()

    def setup_description_field_ui(self) -> None:
        """Setup the description field ui."""
        # non-editable description field -> shows the current description
        self.empty_description_button = QPushButton(
                "Add a more detailed description...",
                parent=self, objectName="empty_card_description_button",
                )
        self.empty_description_button.setMinimumHeight(100)
        self.empty_description_button.setStyleSheet(
                __EMPTY_CARD_DESCRIPTION_BUTTON_QSS__)
        self.empty_description_button.clicked.connect(self.edit_description)
        # the card description editor
        self.card_description_editor = CardDescriptionEditor(parent=self)
        self.card_description_editor.hide()
        self.card_description_editor.done_editing.connect(self.done_editing)
        self.card_description_editor.cancel_editing.connect(
                self.hide_editor)
        # not placed in layout upon init
        # the card description label
        self.card_description_button = CardDescriptionButton(
                text=self.card.description,
                parent=self,
                )
        # self.card_description_button.setMinimumHeight(100)
        self.card_description_button.clicked.connect(self.edit_description)
        if self.card.description == "":
            self.contents.layout().addWidget(
                    self.empty_description_button)
            self.card_description_button.hide()
        else:
            self.contents.layout().addWidget(
                    self.card_description_button)
            self.empty_description_button.hide()
            self.card_description_button.show()

    def setup_title_and_edit_ui(self) -> None:
        """Setup the description title and edit button UI."""
        self.title_and_edit_container = QFrame(parent=self.contents)
        self.title_and_edit_container.setContentsMargins(0, 0, 0, 0)
        self.title_and_edit_container.setLayout(QHBoxLayout())
        self.title_and_edit_container.layout().setContentsMargins(0, 0, 0, 0)
        # self.title_and_edit_container.setFixedHeight(__BUTTON_HEIGHT_BIG__)
        self.contents.layout().addWidget(self.title_and_edit_container)
        self.description_title_label = QLabel(
                "Description", parent=self.contents,
                objectName="full_card_edit_dialog_subtitle",
                )
        set_widget_font(
                self.description_title_label,
                color=__PALETTE__["light_gray"])
        # self.description_title_label.setFixedHeight(
        #         __BUTTON_HEIGHT_BIG__)
        self.description_title_label.setAlignment(Qt.AlignLeft)
        self.title_and_edit_container.layout().addWidget(
                self.description_title_label)
        self.title_and_edit_container.layout().addStretch()
        # edit button (which is hidden if no description)
        self.edit_button = QPushButton(
                "Edit", parent=self, objectName="gray_btn")
        # self.edit_button.setFixedHeight(
        #         __BUTTON_HEIGHT_BIG__)
        self.edit_button.clicked.connect(self.edit_description)
        self.title_and_edit_container.layout().addWidget(
                self.edit_button)
        if self.card.description == "":
            self.edit_button.hide()

    def setup_icon_ui(self) -> None:
        """Setup the icon UI."""
        self.icon_container = QFrame(parent=self)
        self.icon_container.setContentsMargins(0, 0, 0, 0)
        self.icon_container.setLayout(QVBoxLayout())
        self.icon_container.layout().setContentsMargins(0, 2, 0, 0)
        self.layout().addWidget(self.icon_container)
        self.icon_label = HeightForWidthLabel(
                height_factor=1.128712871,
                parent=self.icon_container,
                )
        self.icon_label.setFixedWidth(15)
        self.icon_pixmap = get_icon_pixmap("description")
        self.icon_label.setPixmap(self.icon_pixmap)
        self.icon_label.setScaledContents(True)
        self.icon_container.layout().addWidget(self.icon_label)
        self.icon_container.layout().addStretch()

    def edit_description(self):
        """Edit the card description."""
        if self.empty_description_button.isVisible():
            self.empty_description_button.hide()
            self.contents.layout().replaceWidget(
                    self.empty_description_button,
                    self.card_description_editor,
                    )
        elif self.card_description_button.isVisible():
            self.card_description_button.hide()
            self.contents.layout().replaceWidget(
                    self.card_description_button,
                    self.card_description_editor)
        self.edit_button.hide()
        self.card_description_editor.show()

    def hide_editor(self):
        """Hide the editor."""
        if self.card.description == "":
            # show empty description button
            self.contents.layout().replaceWidget(
                    self.card_description_editor,
                    self.empty_description_button,
                    )
            self.empty_description_button.show()
        else:
            self.contents.layout().replaceWidget(
                    self.card_description_editor,
                    self.card_description_button,
                    )
            self.edit_button.show()
            self.card_description_button.setText(self.card.description)
            self.card_description_button.show()
        self.card_description_editor.hide()

    def done_editing(self):
        """Called when we're done editing."""
        # get text and replace with appropriate widget
        desc = self.card_description_editor.get_description()
        self.card.set_description(desc)
        self.hide_editor()


class CardDescriptionButton(HoverableClickableFrame):
    """The card description button containing the actual description text."""

    def __init__(self, *args, text: str = "", **kwargs) -> None:
        super().__init__(
                *args, objectName="card_description_button", **kwargs)
        self.entered.connect(self.set_hovered_style)
        self.left.connect(self.set_original_style)
        self.setup_ui(text)

    def setup_ui(self, text: str) -> None:
        """Set the card description button UI."""
        self.setLayout(QHBoxLayout())
        self.description_label = QLabel(
                parent=self, text=text)
        self.description_label.setWordWrap(True)
        self.description_label.setStyleSheet(
                f"""QLabel {{
                       border: 0px;
                       color: {__PALETTE__['white']};
                       background-color: transparent;
                       text-align: top-left;
                    }}
                """
                )
        self.layout().addWidget(self.description_label)

    def show(self, *args, **kwargs) -> Any:
        """Show the card description button."""
        if self.underMouse():
            self.set_hovered_style()
        else:
            self.set_original_style()
        return super().show(*args, **kwargs)

    def text(self, *args, **kwargs) -> str:
        """The text contained in the description label."""
        return self.description_label.text(*args, **kwargs)

    def setText(self, *args, **kwargs) -> None:
        """Set the description text."""
        self.description_label.setText(*args, **kwargs)

    def set_hovered_style(self) -> None:
        """Set the hovered style."""
        self.setStyleSheet(
                f"""QFrame {{
                        background-color: {__PALETTE__['gray']};
                        border-radius: 8px;
                    }}
                """
                )

    def set_original_style(self) -> None:
        """Set the original style."""
        self.setStyleSheet(
                """QFrame {{
                        background-color: transparent;
                        border-radius: 8px;
                    }}
                """
                )


class CardDescriptionEditor(QFrame):
    """The card description editor."""

    done_editing = Signal()
    cancel_editing = Signal()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.setup_ui()
        self.card = self.parent().card

    def setup_ui(self):
        """Setup the ui."""
        self.setLayout(QVBoxLayout())
        # toolbox
        self.card_description_editor_toolbox = CardDescriptionEditorToolBox(
                parent=self)
        self.card_description_editor_toolbox.setFixedHeight(
                __BUTTON_HEIGHT_BIG__)
        self.layout().addWidget(self.card_description_editor_toolbox)
        # text edit
        self.card_description_text_edit = MinimalSizeTextEdit(parent=self)
        self.card_description_text_edit.fixed_minimum_height = 200
        self.card_description_text_edit.setWordWrapMode(
                QTextOption.WrapAtWordBoundaryOrAnywhere)
        self.layout().addWidget(self.card_description_text_edit)
        # buttons
        self.setup_card_description_editor_buttons()

    def show(self, *args, **kwargs) -> Any:
        """Upon show, update the text edit content with card description."""
        self.card_description_text_edit.setPlainText(
                self.card.description)
        self.card_description_text_edit.setFocus()
        return super().show(*args, **kwargs)

    def get_description(self):
        """Get the description written in the editor."""
        return self.card_description_text_edit.toPlainText()

    def setup_card_description_editor_buttons(self):
        """Setup the card editor buttons."""
        self.card_editor_buttons = QWidget(parent=self)
        self.layout().addWidget(self.card_editor_buttons)
        self.card_editor_buttons.setLayout(QHBoxLayout())
        self.card_editor_save_button = QPushButton(
                parent=self.card_editor_buttons,
                objectName="card_edit_save_btn",
                text="Save",
                )
        self.card_editor_save_button.setFixedHeight(
                __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__)
        self.card_editor_save_button.clicked.connect(
                self.done_editing)
        self.card_editor_buttons.layout().addWidget(
                self.card_editor_save_button)
        # cancel button
        self.card_editor_cancel_button = QPushButton(
                parent=self.card_editor_buttons,
                objectName="transparent_btn",
                text="Cancel",
                )
        self.card_editor_buttons.layout().addWidget(
                self.card_editor_cancel_button)
        self.card_editor_cancel_button.setFixedHeight(
                __FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__)
        self.card_editor_cancel_button.clicked.connect(self.cancel_editing)
        self.card_editor_buttons.layout().addStretch()


class CardDescriptionEditorToolBox(QFrame):
    """The card description editor toolbox."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setStyleSheet(f"background-color: {__PALETTE__['black']}")
