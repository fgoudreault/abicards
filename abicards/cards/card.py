"""A single card code."""
from typing import Any, Sequence

from PySide6.QtCore import QObject, QPoint, QSize, Signal
from PySide6.QtWidgets import (
        QFrame,
        QMessageBox,
        QVBoxLayout,
        QWidget,
        )

from .checklist import Checklist
from .convert import ConvertibleObject
from .dates import DatableObject, Dates, DatesPickerDialog
from .duration import Duration, DurationObject
from .full_card_edit_dialog import FullCardEditDialog
from .quick_edit_card_menu import QuickEditCardMenuDialog
from .routines import get_index_of_hovering_widget_above_list
from .title import TitleAndEditButton
from ..mixins import IndicatedObject
from ..routines import (
        build_qss, parse_qss,
        unsaved_changes,
        )
from ..styles_constants import (
        __PALETTE__,
        )


__CARD_QSS__ = """QFrame#card {
    color: white;
    border: 3px solid transparent;
    border-radius: 10px;
}
"""
__CARD_QSS_HOVER__ = f"""QFrame#card {{
    color: white;
    border: 3px solid {__PALETTE__['purple']};
    border-radius: 10px;
}}
"""


class Card(
        QFrame, DatableObject, DurationObject, IndicatedObject,
        ConvertibleObject,
        ):
    """Card class."""

    card_updated = Signal()
    entered = Signal()
    left = Signal()
    has_dates_indicator = True

    def __init__(self, *args, title: str = "", **kwargs) -> None:
        QFrame.__init__(self, *args, objectName="card", **kwargs)
        DatableObject.__init__(self)
        DurationObject.__init__(
                self, *args, fixed_by_children_duration=True, **kwargs)
        IndicatedObject.__init__(self)
        self.card_list = self.parent().parent().parent().parent()
        self._logger = self.card_list._logger
        self.board = self.card_list.parent()
        self.setup_ui(title=title)
        self.quick_edit_card_menu = None
        self.ghost_card = None
        self.full_card_edit_dialog = None
        self.description = ""
        self._color = __PALETTE__["dark_gray"]  # default color
        self.checklists = []
        self._set_initial_stylesheet()
        self.completed_changed.connect(self.card_updated)
        self.duration_changed.connect(self.card_updated)

    def __repr__(self) -> str:
        """The card object representation."""
        return f"< Card: title = {self.title} >"

    def can_be_converted(self) -> bool:
        """Return true if card can be converted into another type."""
        if self.checklists:
            return False
        return True

    def get_json_data(self) -> dict[str, Any]:
        """Return json data for this card for storage."""
        data = {
                "title": self.title,
                "description": self.description,
                "checklists": [c.get_json_data() for c in self.checklists],
                "color": self.color,
                }
        data.update(DatableObject.get_json_data(self))
        data.update(DurationObject.get_json_data(self))
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load card data from a json dict."""
        self.set_title(data["title"], from_user=False)
        if "color" in data:
            self.set_color(data["color"], from_user=False)
        if "description" in data:
            self.description = data["description"]
        if "checklists" in data:
            for checklist_data in data["checklists"]:
                self.add_checklist(
                        load_data=checklist_data, unsaved_changes=False)
        DatableObject.load_json_data(self, data)
        DurationObject.load_json_data(self, data)

    def set_dates(self, *args, unsaved_changes: bool = True, **kwargs) -> None:
        """Set the due dates for this card."""
        rtn = DatableObject.set_dates(
                self, *args, unsaved_changes=unsaved_changes, **kwargs)
        if self.dates is not None:
            self.dates.dates_changed.connect(self.card_updated)
        return rtn

    def pop_from_parent(self, new_parent: QObject):
        """Remove card from parent layout."""
        self.place_ghost_card()
        if self in self.card_list.cards:
            self.card_list.cards.remove(self)
        self.setParent(new_parent)

    @unsaved_changes
    def put_into_new_parent(self):
        """Put back card into new card list."""
        # get index of ghost card
        idx = self.remove_ghost_card(return_ghost_card_index=True)
        self.card_list.cards.insert(idx, self)
        self._logger.info(
                f"Inserting card '{self.title}' into list "
                f"'{self.card_list.title}'.")
        return True

    def _set_initial_stylesheet(self):
        self.setStyleSheet(__CARD_QSS__)

    def setStyleSheet(self, ss: str) -> None:
        """Set the style sheet."""
        # update with card color if needed
        if self.color is None:
            return QFrame.setStyleSheet(self, ss)
        name, qss = parse_qss(ss)
        qss["background-color"] = self.color
        newss = build_qss(name, qss)
        return QFrame.setStyleSheet(self, newss)

    @property
    def color(self) -> str | None:
        """The card color."""
        return self._color

    @unsaved_changes
    def set_color(self, color: str | None, from_user: bool = True) -> None:
        """Set the card color."""
        if self.color is None and color is None:
            return
        if self._color == color:
            return
        self._color = color
        self._set_initial_stylesheet()
        self.card_updated.emit()
        if from_user:
            self._logger.info(
                f"Setting new color '{color}' to card '{self.title}'.")
            return True

    @property
    def title(self) -> str:
        """The card title."""
        return self.title_and_edit_button.current_title

    @unsaved_changes
    def set_title(self, newtitle: str, from_user: bool = True) -> None:
        """Set title."""
        if self.title == newtitle:
            return  # do nothing
        self.title_and_edit_button.set_text(newtitle)
        if from_user:
            # if we return true we truly have unsaved changes
            self._logger.info(
                f"Renaming card '{self.title}' => '{newtitle}'.")
            return True

    @unsaved_changes
    def set_description(self, description: str) -> None:
        """Set the card description."""
        if description == self.description:
            # no changes
            return
        self._logger.info(
                f"Setting description of card '{self.title}' to "
                f"'{description}'.")
        self.description = description
        return True

    def setup_ui(self, title: str = ""):
        """Setup the card UI."""
        # upon init -> only label with title
        self.setLayout(QVBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.title_and_edit_button = TitleAndEditButton(
                parent=self)
        self.title_and_edit_button.set_text(title)
        self.layout().addWidget(self.title_and_edit_button)

        DatableObject.setup_ui(self)
        DurationObject.setup_ui(self)
        IndicatedObject.setup_ui(self)

        self.title_and_edit_button.height_changed.connect(
                self.adjust_height)
        self.adjust_height()

    def adjust_height(self):
        """Adjust card height."""
        # do not use adjustSize here, for some reason, it makes the card too
        # wide when hiding the indicator_container
        self.resize(self.width(), self.minimumSizeHint().height())
        # self.card_list.cards_container_scroll.adjustSize()
        self.card_list.adjustSize()

    def get_indicators(self) -> Sequence[QWidget]:
        """Get the list of indicators."""
        return (DatableObject.get_indicators(self) +
                DurationObject.get_indicators(self))

    def indicator_hidden(self):
        """Called when one indicater has been hidden."""
        IndicatedObject.indicator_hidden(self)
        self.adjust_height()

    def indicator_shown(self):
        """Called when one indicator is shown."""
        IndicatedObject.indicator_shown(self)
        self.adjust_height()

    def update_indicators(self) -> None:
        """Update all indicators."""
        DatableObject.update_indicators(self)
        DurationObject.update_indicators(self)

    def get_children_dates(self) -> Sequence[Dates]:
        """Get the list of card children dates."""
        return [c.dates for c in self.checklists]

    def get_children_duration(self) -> Sequence[Duration]:
        """Get the list of children duration."""
        durs = []
        for checklist in self.checklists:
            if not checklist.has_duration():
                durs += checklist.get_children_duration()
            else:
                durs.append(checklist.duration)
        return durs

    def get_dates_picker_dialog(self) -> DatesPickerDialog:
        """Return the dates picker dialog."""
        fd = self.full_card_edit_dialog
        if fd is None:
            return None
        return fd.dates_picker_dialog

    def update_dates_indicator(self) -> None:
        """The dates indicator changed."""
        DatableObject.update_dates_indicator(self)
        if self.dates is None:
            self.adjust_height()
            return
        self.adjust_height()
        # also update the dates indicator on the full edit card if necessary
        fd = self.full_card_edit_dialog
        if fd is None:
            return
        df = fd.non_title_fields.dates_field
        df.update_button_and_label_text()

    def any_children_has_duration(self) -> bool:
        """Return true if any children has duration set."""
        for checklist in self.checklists:
            if checklist.any_children_has_duration():
                return True
        return super().any_children_has_duration()

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing the widget."""
        DatableObject.showEvent(self, *args, **kwargs)
        DurationObject.showEvent(self, *args, **kwargs)
        self.adjust_height()
        return QFrame.showEvent(self, *args, **kwargs)

    def hideEvent(self, *args, **kwargs):
        """Called when hiding the widget."""
        QFrame.hideEvent(self, *args, **kwargs)
        self.card_list.adjustSize()

    @unsaved_changes
    def add_checklist(
            self, title: str = "",
            load_data: dict[str, Any] = None,
            unsaved_changes: bool = True,
            ) -> None:
        """Add a checklist to the card."""
        checklist = Checklist(self, parent=self)
        checklist.set_title(title)
        if load_data is not None:
            checklist.load_json_data(load_data)
        checklist.checklist_updated.connect(self.card_updated)
        checklist.checklist_updated.connect(self.update_dates)
        checklist.checklist_updated.connect(self.update_duration)
        self.checklists.append(checklist)
        if self.full_card_edit_dialog is not None:
            self.full_card_edit_dialog.add_checklist_widget(checklist)
        if unsaved_changes:
            self._logger.info(
                    f"Adding checklist named '{title}' to card '{self.title}'."
                    )
            return True

    @unsaved_changes
    def delete_checklist(self, checklist: Checklist) -> None:
        """Delete a checklist."""
        self._logger.info(
            f"Deleting checklist named '{checklist.title}' from card"
            f" '{self.title}'.")
        self.checklists.remove(checklist)
        checklist.delete()
        return True

    def enterEvent(self, *args, **kwargs) -> Any:
        """Called when mouse enters."""
        if self.title_and_edit_button.title_label.isReadOnly():
            self.set_hover_style()
        self.entered.emit()
        return QFrame.enterEvent(self, *args, **kwargs)

    def set_hover_style(self) -> None:
        """Set the hover state qss."""
        self.setStyleSheet(__CARD_QSS_HOVER__)
        # update dates indicator because the checkbox
        # inherits the style for some reason...
        self.update_dates_indicator()

    def leaveEvent(self, *args, **kwargs) -> Any:
        """Called when mouse leaves."""
        self.set_original_style()
        self.left.emit()
        return QFrame.leaveEvent(self, *args, **kwargs)

    def set_original_style(self):
        """Reset to the original style sheet."""
        self.setStyleSheet(__CARD_QSS__)
        # update dates indicator because the checkbox
        # inherits the style for some reason...
        self.update_dates_indicator()

    def subdialogs_opened(self) -> bool:
        """Return true if subdialogs are opened."""
        if self.quick_edit_card_menu is None:
            return False
        return self.quick_edit_card_menu.isVisible()

    def close_subdialogs(self) -> None:
        """Close all subdialogs."""
        self.cancel_quick_edit()

    def quick_edit_card(self):
        """Open the quick edit card menu."""
        self._logger.info(f"Quick editing card '{self.title}'")
        # make quick edit popup menu appear
        # self.setFixedHeight(self.height())  # set fixed height for now
        if self.quick_edit_card_menu is None:
            # make the quick edit menu's parent to be the main app widget
            # this is so that it appears on top of everything
            # and is not cropped
            self.quick_edit_card_menu = QuickEditCardMenuDialog(
                    parent=self.board,
                    card=self)
        # disable the add card list form in card list if necessary
        self.card_list.init_add_card_button()
        # put a ghost card into card list in order to change this card's parent
        absolute_position = self.mapToGlobal(QPoint(0, 0))
        self.place_ghost_card()
        # switch parent to main widget so that it can be highlighted
        self.setParent(self.board)
        # not added to any layout -> need to move accordingly
        self.move(self.board.mapFromGlobal(absolute_position))
        self.show()

        self.set_original_style()
        self.title_and_edit_button.quick_edit(True)
        self.quick_edit_card_menu.show()
        # highlight this card and the quick edit menu
        self.board.highlight_widgets(
                self, self.quick_edit_card_menu)
        # make sure everything above overlay
        self.raise_()
        self.quick_edit_card_menu.raise_()

    def place_ghost_card(self):
        """Setup a ghost card."""
        if self.ghost_card is None:
            self.ghost_card = QFrame(parent=self.parent(), objectName="ghost")
            self.ghost_card.setStyleSheet("background-color: transparent")
        self.ghost_card.setFixedSize(self.size())
        self.card_list.cards_container.layout().replaceWidget(
                self, self.ghost_card)
        self.ghost_card.show()

    def handle_drag_move(self, target_rect):
        """Handle what happens when dragging this card."""
        # detect if rect covers any card list
        for item in self.parent().card_lists:
            # check center of target rect to determine
            # above which list we are
            if not item.geometry().contains(target_rect.center()):
                continue
            # we're above this card list
            break
        else:
            self.move(target_rect.topLeft())
            return
        # uncollapse card list if necessary
        if item.collapsed:
            item.collapse_cards()
        # check what would be the idx of the card
        idx = get_index_of_hovering_widget_above_list(
                target_rect, self, item.cards)
        # insert ghost card at given card index
        self.ghost_card.parent().layout().removeWidget(self.ghost_card)
        self.ghost_card.parent().parent().parent().parent().adjustSize()
        self.ghost_card.setParent(item.cards_container)
        self.ghost_card.parent().layout().insertWidget(
                idx, self.ghost_card)
        self.ghost_card.parent().parent().parent().parent().adjustSize()

    @unsaved_changes
    def delete_card(self, ask_user: bool = True) -> None:
        """Delete the card.

        Parameters
        ----------
        ask_user: bool, optional
            If True, a dialog opens to ask user to confirm.
        """
        # prompt for confirmation
        self.hide_quick_edit_menu()
        if ask_user:
            msgbox = QMessageBox(parent=self)
            msgbox.setStyleSheet(
                    f"""QMessageBox {{
                            background-color: {__PALETTE__['dark_gray']}
                        }}"""
                    )
            msgbox.setText(
                    "Are you sure you want to delete the card?\n"
                    f"'{self.title}'"
                    )
            msgbox.setWindowTitle("Confirm delete card")
            msgbox.setStandardButtons(QMessageBox.Yes | QMessageBox.Abort)
            msgbox.setDefaultButton(QMessageBox.Yes)
            btn = msgbox.exec()
            if btn == QMessageBox.Abort:
                # cancel
                return
        self._logger.info(f"Deleting card '{self.title}'.")
        self.card_list.delete_card(self)
        self.card_updated.emit()
        return True

    def delete(self, *args, **kwargs) -> Any:
        """Delete the card without asking user."""
        return self.delete_card(*args, ask_user=False, **kwargs)

    def cancel_quick_edit(self):
        """Cancel quick edits."""
        self.discard_changes()
        self.hide_quick_edit_menu()

    def hide_quick_edit_menu(self):
        """Hide the quick edit menu."""
        if self.quick_edit_card_menu is not None:
            self.quick_edit_card_menu.hide()
        self.title_and_edit_button.quick_edit(False)
        self.setMinimumSize(QSize(0, 0))
        self.setMaximumSize(QSize(100000, 100000))
        self.board.unhighlight()
        # readd card to previous card list
        self.remove_ghost_card()
        self.show()

    def remove_ghost_card(
            self, return_ghost_card_index: bool = False) -> bool | None:
        """Remove the ghost card.

        Parameters
        ----------
        return_ghost_card_index: bool, optional
            If True, this method also returns the index of the ghost card
            within the layout it was placed.

        Returns
        -------
        int: If 'return_ghost_card_index' is True.
        None: otherwise.
        """
        if self.ghost_card is None:
            return
        self.setParent(self.ghost_card.parent())
        self.card_list = self.parent().parent().parent().parent()
        to_return = None
        if return_ghost_card_index:
            to_return = self.parent().layout().indexOf(self.ghost_card)
        self.parent().layout().replaceWidget(self.ghost_card, self)
        self.ghost_card.hide()
        return to_return

    def setParent(self, newparent) -> None:
        """Set a new parent to card."""
        QFrame.setParent(self, newparent)
        from .card_list import CardList
        if isinstance(newparent.parent(), CardList):
            self.card_list = newparent.parent()

    def discard_changes(self):
        """Discard changes made to the card."""
        self.title_and_edit_button.discard_changes()

    def save_quick_edits(self) -> None:
        """Save quick edits and quit corresponding dialog."""
        if self.quick_edit_card_menu is None:
            return
        self._logger.info("Saving quick edits.")
        newtitle = self.title_and_edit_button.title
        self.set_title(newtitle)
        self.hide_quick_edit_menu()

    def mouseReleaseEvent(self, *args, **kwargs) -> Any:
        """Called on a mouse press."""
        if self.quick_edit_card_menu is not None:
            if self.quick_edit_card_menu.isVisible() and not (
                    self.quick_edit_card_menu.underMouse()):
                # dismiss
                return QFrame.mousePressEvent(self, *args, **kwargs)
        for indicator in self.indicators:
            if indicator.underMouse():
                # dismiss
                return QFrame.mousePressEvent(self, *args, **kwargs)
        # open edit menu
        self.toggle_full_edit()
        return QFrame.mousePressEvent(self, *args, **kwargs)

    def toggle_full_edit(self):
        """Open the full edit dialog."""
        if self.full_card_edit_dialog is None:
            self.full_card_edit_dialog = FullCardEditDialog(
                    parent=self.board, card=self)
            self.board.board_items.append(self.full_card_edit_dialog)
        self.board.center_widget(self.full_card_edit_dialog)
        if self.full_card_edit_dialog.isVisible():
            self.full_card_edit_dialog.hide()
            return
        self.full_card_edit_dialog.show()
        self.full_card_edit_dialog.raise_()

    def full_editing_done(self) -> None:
        """Called when full editing is done."""
        self.hide_full_card_edit_dialog()
        self.set_title(self.full_card_edit_dialog.title)

    def cancel_full_edit(self):
        """Cancel the full edit of the card."""
        self.hide_full_card_edit_dialog()

    def hide_full_card_edit_dialog(self):
        """Hide the full edit card dialog."""
        self.full_card_edit_dialog.hide()
