"""The card color submodule."""
from PySide6.QtCore import Qt, Signal
from PySide6.QtWidgets import (
        QFrame, QGridLayout, QLabel, QPushButton, QVBoxLayout,
        )

from ..custom_widgets import SmallCloseButton
from ..routines import build_qss, parse_qss, set_widget_font
from ..styles_constants import (
        __BUTTON_HEIGHT_MEDIUM__, __PALETTE__,
        )


class CardColorPickerDialog(QFrame):
    """The card color picker dialog."""

    set_color = Signal()

    def __init__(self, card, *args, **kwargs):
        super().__init__(*args, objectName="black_dialog", **kwargs)
        self.card = card
        self.setup_ui()

    def setup_ui(self):
        """Set dialog UI."""
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        self.setFixedSize(300, 300)
        self.dialog_title_label = QLabel(
                text="Choose card color", parent=self)
        self.dialog_title_label.setStyleSheet("text-align: left")
        set_widget_font(self.dialog_title_label, bold=True)
        self.layout.addWidget(self.dialog_title_label, 0, 0, 1, 1)
        # close button
        self.close_button = SmallCloseButton(parent=self)
        self.close_button.clicked.connect(self.hide)
        self.layout.addWidget(self.close_button, 0, 1, 1, 1, Qt.AlignRight)
        # color picker
        self.color_picker = ColorPickerContainer(parent=self)
        self.color_picker.color_selected.connect(self.set_color)
        self.layout.addWidget(
                self.color_picker, 1, 0, 1, 2)
        # done button
        self.done_button = QPushButton(
                text="Done", parent=self, objectName="purple_btn")
        self.layout.addWidget(self.done_button, 2, 0, 1, 1)
        self.done_button.clicked.connect(self.hide)

    def get_color(self):
        """Get the selected color."""
        return self.color_picker.get_selected_color()


class ColorPickerContainer(QFrame):
    """The color picke class."""

    color_selected = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.color_buttons = {}
        self.default_ss = """
            QPushButton#color_picker_btn {
                border-radius: 3px;
                border: 1px solid transparent;
            }
            """
        self.default_ss_hover = """
            QPushButton#color_picker_btn::hover {
                border: 1px solid white;
            }
            """
        self.currently_selected_color = None
        self.setup_ui()

    def setup_ui(self):
        """Setup the color picker ui."""
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        # self.layout.setContentsMargins(0, 0, 0, 0)
        # suggested colors buttons
        # https://www.refactoringui.com/previews/building-your-color-palette
        for color in [
                "dark_gray",
                "gray",
                "purple",
                "red",
                "brown",
                "green",
                ]:
            btn = QPushButton(parent=self, objectName="color_picker_btn")
            name, ss = parse_qss(self.default_ss)
            ss["background-color"] = __PALETTE__[color]
            hovername, sshover = parse_qss(self.default_ss_hover)
            totss = build_qss(name, ss) + build_qss(hovername, sshover)
            btn.setStyleSheet(totss)
            btn.setFixedHeight(__BUTTON_HEIGHT_MEDIUM__)
            self.layout.addWidget(btn)
            self.color_buttons[__PALETTE__[color]] = btn
            btn.setCheckable(True)
            btn.clicked.connect(self.color_button_clicked)

    def color_button_clicked(self):
        """Called when a color is selected."""
        for color, btn in self.color_buttons.items():
            if btn is self.sender():
                self.currently_selected_color = color
            else:
                btn.setChecked(False)
        self.color_selected.emit()

    def get_selected_color(self):
        """Return the selected color."""
        return self.currently_selected_color
