"""The card title submodule."""
from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QFont
from PySide6.QtWidgets import QFrame, QHBoxLayout, QVBoxLayout, QWidget

from ..custom_widgets import DotsButton, MinimalSizeTextEdit
from ..styles_constants import __PALETTE__


class TitleAndEditButton(QWidget):
    """Card title and edit button widget."""

    height_changed = Signal()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._logger = self.parent()._logger
        self.setup_ui()
        self.current_title = ""

    @property
    def title(self) -> str:
        """The card title."""
        return self.title_label.toPlainText()

    def discard_changes(self):
        """Discard changes to text."""
        self.set_text(self.current_title)

    def set_text(self, title: str) -> None:
        """Set the text of the text edit."""
        self.title_label.setPlainText(title)
        self.current_title = self.title

    def setup_ui(self):
        """Setup the card text editor ui."""
        self.layout = QHBoxLayout()
        self.setLayout(self.layout)
        # title
        self.setup_title_label()
        # edit button
        self.setup_quick_edit_button()

    def setup_title_label(self):
        """Setup the title label."""
        self.title_label = MinimalSizeTextEdit(  # QLabel(
                parent=self, disable_enter=True,
                )
        self.title_label.textChanged.connect(self.on_title_changed)
        self.title_label.setReadOnly(True)
        self.title_label.setStyleSheet("background-color: transparent")
        # set transparent to mouse events in order to be able to click it
        # and open full card menu
        self.title_label.setAttribute(Qt.WA_TransparentForMouseEvents, True)
        self.layout.addWidget(self.title_label)

    def setup_quick_edit_button(self) -> None:
        """Setup the quick edit button."""
        # place it inside a transparent label to make it look like it
        # is on the top right of the card
        self.quick_edit_button_container = QFrame(parent=self)
        # self.quick_edit_button_container.setStyleSheet(
        #         "background-color: blue")
        self.layout.addWidget(self.quick_edit_button_container)
        self.quick_edit_button_container_layout = QVBoxLayout()
        self.quick_edit_button_container.setLayout(
                self.quick_edit_button_container_layout)
        self.quick_edit_button_container.setContentsMargins(0, 0, 0, 0)
        self.quick_edit_button_container_layout.setContentsMargins(0, 0, 0, 0)
        self.quick_edit_button_container.setFixedWidth(20)
        self.quick_edit_button = DotsButton(
                # "...",
                parent=self.quick_edit_button_container,
                # objectName="quick_edit_card_button",
                )
        self.quick_edit_button.setFixedHeight(20)
        self.quick_edit_button_container_layout.addWidget(
                self.quick_edit_button, Qt.AlignTop)
        self.quick_edit_button_container_layout.addStretch()
        # self.layout.addWidget(self.quick_edit_button, Qt.AlignTop)
        self.quick_edit_button.clicked.connect(self.parent().quick_edit_card)

    def on_title_changed(self):
        """Adjust height of card."""
        self.setFixedHeight(
                self.title_label.height() + 15)
        self.height_changed.emit()

    def quick_edit(self, mode: bool) -> None:
        """Quick edit mode is turned on if mode = True."""
        if mode:
            self.title_label.setAttribute(
                    Qt.WA_TransparentForMouseEvents, False)
            self.quick_edit_button.hide()
            self.quick_edit_button.setEnabled(False)
            self.title_label.setReadOnly(False)
            self.title_label.setFocus()
            self.title_label.selectAll()
        else:
            self.title_label.setAttribute(
                    Qt.WA_TransparentForMouseEvents, True)
            self.title_label.deselect()
            self.quick_edit_button.show()
            self.title_label.setReadOnly(True)
            self.title_label.clearFocus()
            self.quick_edit_button.setEnabled(True)


__INACTIVE_TITLE_TEXTBOX_QSS__ = """
    QFrame#full_card_edit_dialog_text_edit_frame {
        background-color: transparent;
        border: 3px solid transparent;
        border-radius: 10px;
        }
    """
__ACTIVE_TITLE_TEXTBOX_QSS__ = """
    QFrame#full_card_edit_dialog_text_edit_frame {
        border: 3px solid #5E5DF0;
        background-color: rgb(40, 40, 40);
        border-radius: 10px;
        }
    """


# TODO: get rid of frame and subclass the text edit
# this might make it prettier by getting rid of the contents margins stuff
class FullCardEditDialogTitleTextEdit(QFrame):
    """Text edit class for the card title.

    Actually this is a QFrame which contains the text edit.
    """

    textChanged = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(
                *args, objectName="full_card_edit_dialog_text_edit_frame",
                **kwargs)
        self.setup_ui()
        self.setStyleSheet(__INACTIVE_TITLE_TEXTBOX_QSS__)
        self.previous_text = ""
        self.is_editing = False

    def setup_ui(self):
        """Setup the ui."""
        self.layout = QHBoxLayout()
        self.setLayout(self.layout)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setContentsMargins(0, 0, 0, 0)
        self.text_edit = MinimalSizeTextEdit(
            parent=self, objectName="full_card_edit_dialog_text_edit",
            disable_enter=True)
        self.text_edit.setStyleSheet(
                f"""MinimalSizeTextEdit#full_card_edit_dialog_text_edit {{
                        background-color: transparent;
                        color: {__PALETTE__['light_gray']};
                        }}
                """)
        self.text_edit.focused_in.connect(self.gained_focus)
        self.text_edit.focused_out.connect(self.lost_focus)
        self.layout.addWidget(self.text_edit)
        self.text_edit.setFontPointSize(15)
        self.text_edit.setFontWeight(QFont.Bold)
        self.text_edit.textChanged.connect(self.textChanged)
        self.text_edit.textChanged.connect(self.on_text_change)
        self.text_edit.pressed_enter.connect(self.done_editing)
        self.text_edit.pressed_escape.connect(self.discard_editing)
        self.text_edit.setReadOnly(True)

    def on_text_change(self):
        """Called when text has changed."""
        self.setFixedHeight(self.text_edit.height())

    def get_text(self) -> str:
        """Get the title."""
        return self.text_edit.toPlainText()

    def setPlainText(self, *args, **kwargs) -> None:
        """Set the text in the text edit."""
        self.text_edit.setPlainText(*args, **kwargs)
        self.previous_text = self.text_edit.toPlainText()

    def gained_focus(self) -> None:
        """Called when gained focus."""
        self.setStyleSheet(__ACTIVE_TITLE_TEXTBOX_QSS__)
        self.text_edit.setReadOnly(False)
        self.is_editing = True

    def lost_focus(self) -> None:
        """Called when lost focus."""
        self.setStyleSheet(__INACTIVE_TITLE_TEXTBOX_QSS__)
        self.text_edit.setReadOnly(True)
        self.is_editing = False

    def hide(self):
        """Hide the widget."""
        self.done_editing()
        super().hide()

    def done_editing(self):
        """Call this once editing is done."""
        self.lost_focus()
        self.previous_text = self.text_edit.toPlainText()
        self.parent().change_card_title(self.text_edit.toPlainText())

    def discard_editing(self):
        """Call this to discard editing."""
        self.text_edit.setPlainText(self.previous_text)
        self.lost_focus()

    def start_editing(self) -> None:
        """Start editing textbox."""
        self.text_edit.setFocus()
        # for some reason the signal does not trigger always
        self.gained_focus()
