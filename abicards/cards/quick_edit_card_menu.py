"""Card quick edit module."""
from typing import Any, Sequence

from PySide6.QtCore import QPoint, Qt
from PySide6.QtGui import QShortcut
from PySide6.QtWidgets import (
        QFrame, QPushButton,
        )

from .convert import ConvertToDialog


__QUICK_EDIT_BUTTON_HEIGHT__ = 30
__QUICK_EDIT_BUTTON_WIDTH__ = 100


class QuickEditCardMenuDialog(QFrame):  # (TransparentFramelessDialog):
    """Quick edit card menu.

    This is a collection of dialogs.
    Since we want rounded corner dialogs, each dialogs are accompanied
    by a transparent frameless one that acts as background.
    """

    _widget_spacing = 5

    def __init__(self, *args, card=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.card = card
        self.board = self.card.board
        self.convert_to_dialog = None
        # self.setStyleSheet("background-color: rgba(0, 255, 0, 100)")
        self.setup_ui()

    @property
    def subdialogs(self) -> Sequence:
        """The list of subdialogs."""
        dialogs = []
        if self.convert_to_dialog is not None:
            dialogs.append(self.convert_to_dialog)
            dialogs += self.convert_to_dialog.subdialogs
        return dialogs

    def setup_ui(self):
        """Setup the ui."""
        self.move(self.mapFromGlobal(self.card.mapToGlobal(QPoint(0, 0))))
        self.setMinimumWidth(
                self.card.width() + 2 * self._widget_spacing +
                __QUICK_EDIT_BUTTON_WIDTH__)
        self.setup_save_btn()
        self.setup_side_btns()
        self.setMinimumHeight(10000)  # huge but transparent anyway

    def setup_save_btn(self):
        """Setup save button."""
        self.save_button = QPushButton(
                parent=self,
                text="Save",
                objectName="card_edit_save_btn",
                )
        self.save_button.clicked.connect(self.card.save_quick_edits)
        self.return_shortcut = QShortcut(Qt.Key_Return, self.save_button)
        self.return_shortcut.activated.connect(
                  self.card.save_quick_edits)
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self.save_button)
        self.enter_shortcut.activated.connect(
                  self.card.save_quick_edits)
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(self.card.save_quick_edits)
        text_label = self.card.title_and_edit_button.title_label
        text_label.pressed_enter.connect(self.card.save_quick_edits)
        text_label.textChanged.connect(self.on_text_edit_changed)
        self.save_button.move(
                QPoint(
                    0,
                    self.card.height() + self._widget_spacing))

    def setup_side_btns(self) -> None:
        """Setup side buttons."""
        # convert button
        self.convert_button = QPushButton(
                parent=self, text="Convert", objectName="gray_btn")
        self.convert_button.clicked.connect(self.open_convert_to_dialog)
        self.convert_button.setFixedSize(
                __QUICK_EDIT_BUTTON_WIDTH__,
                __QUICK_EDIT_BUTTON_HEIGHT__,
                )
        self.convert_button.move(
                QPoint(self.card.width() + self._widget_spacing,
                       0)
                )
        # delete button
        self.delete_button = QPushButton(
                parent=self, text="Delete",
                objectName="gray_btn",
                )
        self.delete_button.clicked.connect(self.card.delete_card)
        self.delete_button.setFixedWidth(
                __QUICK_EDIT_BUTTON_WIDTH__)
        self.delete_button.setFixedHeight(
                __QUICK_EDIT_BUTTON_HEIGHT__)
        self.delete_button.move(
                QPoint(self.card.width() + self._widget_spacing,
                       self.convert_button.height() + self._widget_spacing)
                )

    def open_convert_to_dialog(self) -> None:
        """Open the convert to dialog."""
        if self.convert_to_dialog is None:
            self.convert_to_dialog = ConvertToDialog(
                    "card", self.card, parent=self.board)
            self.convert_to_dialog.converted.connect(self.hide)
        self.convert_to_dialog.setEnabled(self.card.can_be_converted())
        # place dialog under corresponding button
        abs_pos = self.convert_button.mapToGlobal(QPoint(0, 0))
        rel_pos = self.board.mapFromGlobal(abs_pos)
        xshift = -(
                self.convert_to_dialog.width() -
                self.convert_button.width()) / 2
        yshift = self.convert_button.height() + 5
        self.convert_to_dialog.move(rel_pos.x() + xshift, rel_pos.y() + yshift)
        if self.convert_to_dialog.isVisible():
            self.convert_to_dialog.hide()
        else:
            self.convert_to_dialog.show()
            self.convert_to_dialog.raise_()

    def on_text_edit_changed(self):
        """Called when text editor of card title changed its text."""
        # change size of text edit when input is changed
        # also move down the save btn
        self.save_button.move(
                self.save_button.x(),
                self.card.height() + self._widget_spacing,
                )

    def hide(self, *args, **kwargs) -> Any:
        """Hide quick edit menu."""
        # disable event filter
        self.escape_shortcut.setEnabled(False)
        self.return_shortcut.setEnabled(False)
        self.enter_shortcut.setEnabled(False)
        for dialog in self.subdialogs:
            if dialog is None:
                continue
            dialog.hide()
        return super().hide(*args, **kwargs)

    def show(self, *args, **kwargs) -> Any:
        """Show quick edit menu."""
        # install event filter
        self.escape_shortcut.setEnabled(True)
        self.return_shortcut.setEnabled(True)
        self.enter_shortcut.setEnabled(True)
        return super().show(*args, **kwargs)

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing the quick edit menu."""
        # move the save button at the bottom of card in case it changed size
        self.save_button.move(
                QPoint(0, self.card.height() + self._widget_spacing))
        return super().showEvent(*args, **kwargs)

    def mousePressEvent(self, event):
        """An event filter in order to cancel edits if click outside."""
        # check if mouse was clicked on a children
        # don't check on self as our area is huge
        for widget in self.children():
            if not hasattr(widget, "underMouse"):
                continue
            if widget.underMouse():
                return False
        # no widgets were under. close our menu
        self.card.cancel_quick_edit()
        return False
