"""The planer submodule."""
from datetime import date, timedelta
from typing import Any

from PySide6.QtCore import Qt
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QPushButton, QVBoxLayout,
        )

from .calendar import PlanerCalendar
from .day_planer import DayPlaner
from ..board import BaseBoardItem
from ..constants import __MONTHS__
from ..routines import (
        m_to_hm, qdate_to_date, set_widget_font,
        )


__TODAY__ = "Today"
__TOMORROW__ = "Tomorrow"
__YESTERDAY__ = "Yesterday"


class Planer(BaseBoardItem):
    """The planer widget."""

    background_color = "dark_purple"
    editable = False
    has_collision = True

    def __init__(self, *args, **kwargs) -> None:
        self.current_date = None
        super().__init__(*args, title="Planer", **kwargs)
        self.set_date(date.today())
        self.parent().board_item_updated.connect(self.update_day_planing)
        self.calendar = None

    def subdialogs_opened(self) -> bool:
        """Return true if subdialogs are opened."""
        if self.calendar is None:
            return False
        return self.calendar.isVisible()

    def close_subdialogs(self) -> None:
        """Close subdialogs."""
        if self.calendar is None:
            return
        self.calendar.hide()

    def close(self, *args, **kwargs) -> None:
        """Remove from board so that it is not saved."""
        self.parent().remove_planer()
        super().close(*args, **kwargs)

    def setup_ui(self) -> None:
        """Setup the planer ui."""
        super().setup_ui()
        self.setup_buttons()
        self.setup_infos_and_today()
        # day planner
        self.setup_day_planer()

    def setup_infos_and_today(self):
        """Setup the number of tasks and reset to today button."""
        self.info_and_today_container = QFrame(parent=self)
        self.info_and_today_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.info_and_today_container)
        # info labels
        self.info_container = QFrame(parent=self.info_and_today_container)
        self.info_container.setLayout(QVBoxLayout())
        self.info_and_today_container.layout().addWidget(self.info_container)
        # ntasks labels
        self.ntasks_label = QLabel(parent=self.info_container)
        self.ntasks_label.setAlignment(
                Qt.AlignBottom)
        self.info_container.layout().addWidget(self.ntasks_label)
        # duration label
        self.duration_label = QLabel(parent=self.info_container)
        self.info_container.layout().addWidget(self.duration_label)
        self.info_container.layout().addStretch()
        # today button
        self.today_btn = QPushButton(
                parent=self, objectName="transparent_btn",
                text=__TODAY__)
        self.today_btn.clicked.connect(self.set_today)
        self.info_and_today_container.layout().addWidget(self.today_btn)

    def setup_buttons(self) -> None:
        """Setup the buttons."""
        # navigating buttons
        self.buttons_container = QFrame(parent=self)
        self.buttons_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.buttons_container)
        # previous date button and label
        self.previous_date_btn = QPushButton(
                parent=self.buttons_container, text="<",
                objectName="transparent_btn")
        self.previous_date_btn.clicked.connect(self.previous_day)
        self.previous_date_btn.setFixedWidth(20)
        self.buttons_container.layout().addWidget(
                self.previous_date_btn)
        self.previous_date_label = QLabel(
                parent=self.buttons_container,
                )
        self.previous_date_label.setAlignment(
                Qt.AlignLeft | Qt.AlignVCenter)
        set_widget_font(self.previous_date_label, point_size=8)
        self.buttons_container.layout().addWidget(
                self.previous_date_label)

        # current date btn
        self.current_date_btn = QPushButton(
                parent=self.buttons_container, objectName="transparent_btn")
        set_widget_font(self.current_date_btn, bold=True)
        self.current_date_btn.clicked.connect(self.open_calendar)
        self.buttons_container.layout().addWidget(
                self.current_date_btn)

        # next date btn and label
        self.next_date_label = QLabel(parent=self.buttons_container)
        self.buttons_container.layout().addWidget(self.next_date_label)
        self.next_date_label.setAlignment(
                Qt.AlignRight | Qt.AlignVCenter)
        set_widget_font(self.next_date_label, point_size=8)

        self.next_date_btn = QPushButton(
                parent=self.buttons_container, text=">",
                objectName="transparent_btn")
        self.next_date_btn.setFixedWidth(20)
        self.buttons_container.layout().addWidget(
                self.next_date_btn)
        self.next_date_btn.clicked.connect(self.next_day)

    def setup_day_planer(self) -> None:
        """Setup the day planning list."""
        self.day_planer = DayPlaner(parent=self)
        self.layout().addWidget(self.day_planer)
        self.day_planer.nothing_on_schedule.connect(self.update_day_planing)
        self.day_planer.something_on_schedule.connect(self.update_day_planing)
        self.day_planer.hide()
        self.nothing_planed_label = QLabel(
                text=(
                    "Nothing planed!\nSet 'dates' in items to add them\n"
                    "to the planer."),
                parent=self)
        self.nothing_planed_label.setWordWrap(True)
        self.nothing_planed_label.setAlignment(Qt.AlignLeft)
        self.layout().addWidget(self.nothing_planed_label)

    def update_day_planing(self) -> None:
        """Update the day planing tool."""
        if not self.day_planer.tasks:
            self.day_planer.hide()
            self.nothing_planed_label.show()
        else:
            self.day_planer.show()
            self.nothing_planed_label.hide()
        self.update()
        self.adjustSize()

    def set_today(self) -> None:
        """Set today's date."""
        self.set_date(date.today())

    def set_date(self, dt: date) -> None:
        """Set the planer date."""
        today = date.today()
        self.current_date = dt
        if dt == today:
            self.current_date_btn.setText(__TODAY__)
        elif dt == today + timedelta(-1):
            self.current_date_btn.setText(__YESTERDAY__)
        elif dt == today + timedelta(1):
            self.current_date_btn.setText(__TOMORROW__)
        else:
            self.current_date_btn.setText(self.date_to_str(dt))
        self.next_date_label.setText(
                self.date_to_str(dt + timedelta(1)))
        self.previous_date_label.setText(
                self.date_to_str(dt + timedelta(-1)))
        self.day_planer.set_date(self.current_date)
        self.update()

    def update(self) -> None:
        """Update the planer info bar."""
        self.ntasks_label.setText(
                f"Number of tasks: {len(self.day_planer.tasks)}")
        self.duration_label.setText(
                f"Total duration: {self._duration_string()}")

    def _duration_string(self) -> str:
        """Get the duration string."""
        minutes = self.day_planer.duration
        h, m = m_to_hm(minutes)
        if h == 0:
            return f"{m}m"
        if m == 0:
            return f"{h}h"
        return f"{h}h{str(m).zfill(2)}m"

    def next_day(self):
        """Set the next day."""
        self.set_date(self.get_date() + timedelta(1))

    def previous_day(self) -> None:
        """Set the previous day."""
        self.set_date(self.get_date() + timedelta(-1))

    def get_date(self) -> date:
        """Get the current planer date."""
        return self.current_date

    def date_to_str(self, dt: date) -> str:
        """Convert a date to a string."""
        today = date.today()
        ext = "th"
        if dt.day in [1, 21, 31]:
            ext = "st"
        elif dt.day in [2, 22]:
            ext = "nd"
        string = f"{__MONTHS__[dt.month - 1]}, {dt.day}{ext}"
        if dt.year != today.year:
            # add year
            string += f" {dt.year}"
        return string

    def open_calendar(self) -> None:
        """Opens a calendar widget to select a date."""
        if self.calendar is None:
            self.calendar = PlanerCalendar(parent=self.parent())
            self.calendar.selectionChanged.connect(
                    self.calendar_selected_date_changed)
        if self.calendar.isVisible():
            # actually hide it
            self.calendar.hide()
            return
        self.calendar.setSelectedDate(self.get_date())
        # place calendar under btn
        bl = self.current_date_btn.parent().mapToGlobal(
                self.current_date_btn.geometry().bottomRight())
        self.calendar.move(self.parent().mapFromGlobal(bl))
        self.calendar.show()
        self.calendar.raise_()

    def calendar_selected_date_changed(self) -> None:
        """The calendar date was changed, this is called."""
        self.set_date(qdate_to_date(self.calendar.selectedDate()))

    def mousePressEvent(self, event) -> None:
        """Dismiss calendar if needed."""
        if self.calendar is None:
            return super().mousePressEvent(event)
        if self.calendar.underMouse():
            return super().mousePressEvent(event)
        if self.calendar.isVisible():
            self.calendar.hide()
        return super().mousePressEvent(event)

    def get_json_data(self) -> dict[str, Any]:
        """Get json data for storage."""
        data = super().get_json_data()
        data["date"] = self.get_date().isoformat()
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load stored data."""
        super().load_json_data(data)
        # if "date" in data:
        #     self.set_date(
        #             date.fromisoformat(data["date"]), unsaved_changes=False)

    def show(self, *args, **kwargs) -> Any:
        """Show the planer."""
        self.day_planer.update()
        return super().show(*args, **kwargs)
