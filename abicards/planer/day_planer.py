"""The day planer submodule."""
import math
from datetime import date
from typing import Any, Sequence

from PySide6.QtCore import Qt, Signal
from PySide6.QtWidgets import QFrame, QHBoxLayout, QLabel, QVBoxLayout

from ..cards import Card, CardList, Checklist, ChecklistItem, DurationObject
from ..styles_constants import __PALETTE__


class DayPlaner(QFrame):
    """The day planer inside the whole Planer widget."""

    nothing_on_schedule = Signal()
    something_on_schedule = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.current_date = None
        self.tasks = []
        self.day_planer_items = []
        self.parent().parent().board_item_updated.connect(
                self.update)
        self.setup_ui()

    @property
    def duration(self) -> int:
        """The total duration of all planing items."""
        m = 0
        for item in self.tasks:
            if not isinstance(item, DurationObject):
                continue
            if not item.has_duration():
                continue
            m += item.duration.duration
        return m

    def add_day_planer_item(
            self, item: Card | CardList | Checklist | ChecklistItem,
            ) -> None:
        """Add a day planer item from a card."""
        item = DayPlanerItem(item, parent=self)
        self.day_planer_items.append(item)
        self.layout().addWidget(item)
        item.adjustSize()
        self.adjustSize()

    def clear(self) -> None:
        """Clear the day planing items."""
        for item in self.day_planer_items:
            item.hide()
            self.layout().removeWidget(item)
            item.destroy()

    def setup_ui(self) -> None:
        """Setup day planer ui."""
        self.setLayout(QVBoxLayout())

    def set_date(self, dt: date) -> None:
        """Set the day planner's date."""
        self.current_date = dt
        self.update()
        self.parent().adjustSize()

    def update(self) -> None:
        """Update the day planing."""
        self.tasks = self.get_tasks()
        if not self.tasks:
            self.nothing_on_schedule.emit()
        else:
            self.something_on_schedule.emit()
        self.clear()
        for task in self.tasks:
            self.add_day_planer_item(task)

    def get_tasks(self) -> Sequence[Card]:
        """Get all tasks for current date."""
        board = self.parent().parent()
        tasks = []
        for card_list in board.card_lists:
            # check if cards within have dates
            card_in_tasks = False
            items_this_card_list_in_tasks = False
            checklist_this_card_list_in_tasks = False
            for card in card_list.cards:
                # check if the card have checklists with finer dates
                checklist_in_tasks = False
                items_in_tasks = False
                for checklist in card.checklists:
                    # check if checklist has items with finer dates
                    items_in_tasks = False
                    for item in checklist.checklist_items:
                        if item.dates is None:
                            continue
                        if item.dates.completed:
                            continue
                        if item.dates.is_date_within(self.current_date):
                            items_in_tasks = True
                            items_this_card_list_in_tasks = True
                            tasks.append(item)
                    if not items_in_tasks:
                        if checklist.dates is None:
                            continue
                        if not checklist.dates.is_date_within(
                                self.current_date):
                            continue
                        if checklist.dates.completed:
                            continue
                        checklist_in_tasks = True
                        checklist_this_card_list_in_tasks = True
                        tasks.append(checklist)
                if not any([items_in_tasks, checklist_in_tasks]):
                    if card.dates is None:
                        continue
                    if not card.dates.is_date_within(self.current_date):
                        continue
                    if card.dates.completed:
                        continue
                    card_in_tasks = True
                    tasks.append(card)
            if not any([card_in_tasks,
                        checklist_this_card_list_in_tasks,
                        items_this_card_list_in_tasks]):
                if card_list.dates is None:
                    continue
                if not card_list.dates.is_date_within(self.current_date):
                    continue
                if card_list.dates.completed:
                    continue
                tasks.append(card_list)
        return tasks

    def showEvent(self, *args, **kwargs):
        """Called when showing happens."""
        super().showEvent(*args, **kwargs)
        self.parent().adjustSize()


class DayPlanerItem(QFrame):
    """A day planer item inside the day planer widget."""

    entered = Signal()
    left = Signal()

    def __init__(
            self, item: Card | CardList | Checklist | ChecklistItem,
            *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.item = item
        if isinstance(item, Card):
            self.item_type = "card"
            # when hovering the card, it's like we hover this planing item
            self.item.entered.connect(self.set_hover_style)
            self.item.left.connect(self.set_original_style)
            self.item.card_updated.connect(self.update)
            self.entered.connect(item.set_hover_style)
            self.left.connect(item.set_original_style)
        elif isinstance(item, CardList):
            self.item.card_list_updated.connect(self.update)
            self.item_type = "card_list"
            # when hovering the dates indicator, highlight item
            self.item.dates_indicator.entered.connect(self.update)
            self.item.dates_indicator.left.connect(self.update)
            self.entered.connect(item.dates_indicator.set_hover_style)
            self.left.connect(item.dates_indicator.set_original_style)
        elif isinstance(item, Checklist):
            self.item.checklist_updated.connect(self.update)
            # when hovering the card, highlight the item
            self.item.card.entered.connect(self.set_hover_style)
            self.item.card.left.connect(self.set_original_style)
            self.item_type = "checklist"
            self.entered.connect(item.card.set_hover_style)
            self.left.connect(item.card.set_original_style)
        elif isinstance(item, ChecklistItem):
            self.item.checklist_item_updated.connect(self.update)
            self.item_type = "checklist_item"
            # when hovering the card, highlight the item
            self.item.card.entered.connect(self.set_hover_style)
            self.item.card.left.connect(self.set_original_style)
            self.entered.connect(item.card.set_hover_style)
            self.left.connect(item.card.set_original_style)
        else:
            raise TypeError(item)
        self.setup_ui()
        self.update()

    def update(self) -> None:
        """Update the day planner item."""
        self.title_label.setText(self.item.title)
        if self.need_highlight():
            self.set_hover_style()
            return
        self.set_original_style()

    def need_highlight(self) -> bool:
        """Check if we need to highlight the planing item."""
        if self.underMouse():
            return True
        if self.item_type == "card_list":
            return self.item.dates_indicator.underMouse()
        if self.item_type == "card":
            return self.item.underMouse()
        # if we hover the corresponding card, highlight the item
        return self.item.card.underMouse()

    def setup_ui(self) -> None:
        """Setup the day planer item ui."""
        self.setLayout(QHBoxLayout())
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.setContentsMargins(0, 0, 0, 0)
        self.setStyleSheet("QFrame { background-color: transparent }")
        self.layout().addStretch()
        self.title_label = QLabel(parent=self, text=self.item.title)
        self.title_label.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        self.layout().addWidget(self.title_label)
        self.set_original_style()
        self.set_width()
        self.set_height()

    def set_height(self) -> None:
        """Set the day planing item height based on its duration."""
        height = 30  # 1hr or less
        if not isinstance(self.item, DurationObject):
            self.setFixedHeight(height)
            return
        if self.item.duration is None:
            self.setFixedHeight(height)
            return
        if self.item.duration.duration <= 60:
            self.setFixedHeight(height)
            return
        # give height proportional to duration
        self.setFixedHeight(
                int(math.floor(height * self.item.duration.duration / 60)))

    def set_width(self):
        """Set the width according to item type."""
        if self.item_type == "card_list":
            self.title_label.setFixedWidth(260)
        elif self.item_type == "card":
            self.title_label.setFixedWidth(240)
        elif self.item_type == "checklist":
            self.title_label.setFixedWidth(220)
        elif self.item_type == "checklist_item":
            self.title_label.setFixedWidth(200)

    def set_original_style(self) -> None:
        """Set the original style qss."""
        self.title_label.setStyleSheet(
                f"""QLabel {{
                        background-color: {self.get_color()};
                        border-radius: 8px;
                        border: 3px solid transparent;
                        color: white;
                        }}
                """)

    def set_hover_style(self) -> None:
        """Set the hover style qss."""
        self.title_label.setStyleSheet(
                f"""QLabel {{
                        background-color: {self.get_color()};
                        border-radius: 8px;
                        border: 3px solid {__PALETTE__['purple']};
                        color: white;
                        }}
                """)

    def get_color(self) -> str:
        """Get the planing item color."""
        if self.item_type == "card_list":
            return __PALETTE__["black"]
        if self.item_type == "card":
            return self.item.color
        return __PALETTE__["medium_dark_gray"]

    def enterEvent(self, event) -> Any:
        """Called when entering the widget with mouse."""
        self.entered.emit()
        self.set_hover_style()
        return super().enterEvent(event)

    def leaveEvent(self, event) -> Any:
        """Callend when leaving the widget with mouse."""
        self.left.emit()
        self.set_original_style()
        return super().leaveEvent(event)

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing widget."""
        super().showEvent(*args, **kwargs)
        self.parent().adjustSize()
        self.parent().parent().adjustSize()
