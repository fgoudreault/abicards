"""The planer calendar popup submodule."""
from datetime import date

from PySide6.QtCore import QDate, Qt, Signal
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QPushButton, QVBoxLayout,
        )

from ..custom_widgets import RangeCalendar, SmallCloseButton
from ..routines import date_to_qdate
from ..styles_constants import __LABEL_HEIGHT_SMALL__, __PALETTE__


class PlanerCalendar(QFrame):
    """The planer calendar widget."""

    selectionChanged = Signal()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.setup_ui()

    def setup_ui(self):
        """Setup the UI."""
        self.setLayout(QVBoxLayout())
        self.setStyleSheet(
            f"""
            QFrame {{ background-color: {__PALETTE__['dark_gray']};
                      border-radius: 8px;
            }}
            """
            )
        # title and buttons
        self.title_container = QFrame(parent=self)
        self.title_container.setLayout(QHBoxLayout())
        self.title_label = QLabel(
                parent=self.title_container, text="Select a date")
        self.title_container.layout().addWidget(
                self.title_label)
        self.title_label.setFixedHeight(__LABEL_HEIGHT_SMALL__)
        self.title_label.setAlignment(Qt.AlignLeft)
        self.title_container.layout().addStretch()
        self.today_btn = QPushButton(
                text="today", parent=self.title_container,
                objectName="transparent_btn")
        self.today_btn.clicked.connect(self.set_today)
        self.title_container.layout().addWidget(
                self.today_btn, alignment=Qt.AlignRight)
        self.close_btn = SmallCloseButton(parent=self.title_container)
        self.close_btn.clicked.connect(self.hide)
        self.title_container.layout().addWidget(
                self.close_btn, alignment=Qt.AlignRight)
        self.layout().addWidget(self.title_container)

        # actual calendar
        self.calendar = RangeCalendar(parent=self)
        self.calendar.selectionChanged.connect(self.selectionChanged)
        self.layout().addWidget(self.calendar)

    def selectedDate(self) -> QDate:
        """Return the selected date."""
        return self.calendar.selectedDate()

    def set_today(self) -> None:
        """Set the calendar date to today."""
        self.calendar.setSelectedDate(date_to_qdate(date.today()))

    def setSelectedDate(self, *args) -> None:
        """Set the calendar's selected date."""
        self.calendar.setSelectedDate(*args)

    def mousePressEvent(self, event) -> None:
        """Called when mouse is pressed."""
        # implemented this dummy function to absorb click events
        # so that board widget does not dismiss it if we click
        # inside it but not on calendar.
        if self.underMouse():
            return
        return super().mousePressEvent(event)
