"""Global routines."""
import logging
import os
import time
from datetime import date, datetime
from typing import Any, Sequence

from PySide6.QtCore import QDate, QObject
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import (
        QApplication, QDialog, QFileDialog, QMainWindow, QMessageBox,
        QToolBar, QWidget,
        )


__ICON_PIXMAPS__ = {}


def get_app() -> QApplication:
    """Global app."""
    # return QApplication.instance()
    # qApp is a builtin when you import PySide
    return qApp  # noqa: F821


def get_loglevel() -> int:
    """Get the application loglevel."""
    try:
        return get_app().loglevel
    except AttributeError:
        # loglevel was not set or we are in testing env
        # set a debug level by default
        return logging.DEBUG


def get_main_window() -> QMainWindow | None:
    """Global function that returns the abicards main window."""
    # import here to avoid circular imports
    from .main_window import AbicardsMainWindow
    for widget in get_app().topLevelWidgets():
        if isinstance(widget, AbicardsMainWindow):
            return widget
    return None


def get_main_toolbar() -> QToolBar | None:
    """Get the main toolbar or None if main window does not exists."""
    window = get_main_window()
    if window is None:
        return None
    return get_main_window().toolbar


def get_main_widget() -> QWidget:
    """Global function that returns the main widget."""
    return get_main_window().abicards_widget


def get_icon_path(name: str) -> str:
    """Return the absolute path to an icon."""
    here = os.path.dirname(__file__)
    return os.path.join(here, "icons", f"{name}")


def get_icon_pixmap(name: str) -> QPixmap:
    """Return the pixmap associated with given icon name."""
    if name in __ICON_PIXMAPS__:
        return __ICON_PIXMAPS__[name]
    __ICON_PIXMAPS__[name] = QPixmap(get_icon_path(name))
    return __ICON_PIXMAPS__[name]


def get_abicards_repo_root() -> str:
    """Get the abicards repo root."""
    return os.path.dirname(
            os.path.dirname(
                    __file__))


def get_widget_background_color(widget: QObject) -> str:
    """Get the given widget background color."""
    name, attrs = parse_qss(widget.styleSheet())
    if "background-color" not in attrs:
        return None
    return attrs["background-color"]


def global_dialogs(obj: QObject = None) -> Sequence[QDialog]:
    """Return the list of all dialogs (instances of QDialog).

    Parameters
    ----------
    obj: QObject, optional
        If not None, specifies the top widget to start searching.
    """
    if obj is None:
        obj = get_main_window()
    dialogs = []
    for child in obj.children():
        if isinstance(child, QDialog):
            dialogs.append(child)
            continue
        # search subchildrent
        dialogs += global_dialogs(child)
    return dialogs


def unsaved_changes(func: callable) -> callable:
    """Method decorator that set unsaved_changes to true if sucess.

    When a specific function with this decorator is called,
    we set 'unsaved_changes' to true on the main window object
    if the underlying function returns True. If nothing is returned
    or something which is not True, we don't set unsaved changes to true.
    """
    def wrapped(*args, **kwargs) -> Any:
        res = func(*args, **kwargs)
        if res is not True:
            return res
        # set unsaved changes
        main_window = get_main_window()
        if main_window is None:
            # no main window exists
            return res
        main_window.unsaved_changes = True
        return res
    return wrapped


def notimplemented_feature(func: callable) -> callable:
    """Method decorator that warns user feature is not implemented yet.

    Opens a dialog to tell user that this feature is not implemented yet.
    Voids calling the function.
    """
    def wrapped(*args, **kwargs) -> Any:
        QMessageBox.information(
                get_main_window(),
                "Not implemented",
                """This feature is not implemented yet. Feel free to harass
                dev(s) to implement it ASAP.
                """,
                QMessageBox.Ok,
                )
        return
    return wrapped


def get_generic_file_dialog_for_loading_board(
        settings, parent) -> QFileDialog:
    """Create a generig file dialog in order to load a board."""
    last_dir = settings.get_value(
            "last_open_card_file_dir")
    last_base = settings.get_value(
            "last_open_card_file_basename")
    lastfile = os.path.join(last_dir, last_base)
    dialog = QFileDialog(
            parent=parent, caption="Save file at",
            directory=lastfile,
            )
    dialog.setDefaultSuffix(".json")
    return dialog


def parse_qss(qss: str) -> tuple[str, dict[str, str]]:
    """Parse a QSS string and return a dict of the values stored in it.

    Parameters
    ----------
    qss: str
        The qss string to parse.

    Returns
    -------
    tuple: First element is the objectName. Secont element is
           the key value pairs stored in the qss string.
    """
    qss.replace("\n", "")
    if not qss:
        # no style sheet
        return "", {}
    # get content inside curly braces
    content = qss.split("{")[-1].split("}")[0]
    # key values are separated with semi colons
    splits = content.split(";")
    data = {}
    for split in splits:
        # key values are separated by colons
        if not split.replace(" ", "").replace("\n", ""):
            continue
        key, value = split.split(":")
        key = key.replace("\n", "").strip(" ").replace("\n", "")
        data[key] = value.strip(" ")
    # now return the objectName along with dict
    name = qss.split("{")[0].replace(" ", "")
    return name, data


def build_qss(objectname: str, attrs: dict[str, Any]) -> str:
    """Build a QSS string from the object name and the dict of attributes.

    Parameters
    ----------
    objectname: str
        The objectname. Corresponds to everything before the curly braces.
    attrs: dict
        The dict of attributes for the qss.

    Returns
    -------
    str: The final qss string.
    """
    string = objectname
    string += " {"
    for key, val in attrs.items():
        string += f" {key}: {val};"
    string += " }"
    return string


def set_widget_font(
        widget: QWidget, bold: bool = False,
        point_size: int = None, color: str = None,
        ) -> None:
    """Set the font for the given widget."""
    font = widget.font()
    font.setBold(bold)
    if point_size is not None:
        font.setPointSize(point_size)
    if color is not None:
        # need to change ss
        name, ss = parse_qss(widget.styleSheet())
        if not name:
            name = widget.objectName()
            if name and not name.startswith("Q"):
                name = "#" + name
            else:
                name = widget.__class__.__name__
        ss["color"] = color
        widget.setStyleSheet(build_qss(name, ss))
    widget.setFont(font)


def hex_to_rgb(color: str) -> tuple[int]:
    """Convert a hex color string to rgb color."""
    c = color.strip("#")
    r, g, b = c[:2], c[2:4], c[4:]
    return int(r, base=16), int(g, base=16), int(b, base=16)


def qdate_to_date(qdate: QDate) -> date:
    """Convert a qdate to a date object."""
    return date(qdate.year(), qdate.month(), qdate.day())


def date_to_qdate(dt: date) -> QDate:
    """Convert a date to a qdate object."""
    return QDate(dt.year, dt.month, dt.day)


def date_to_datetime(dt: date) -> datetime:
    """Convert a date to a datetime."""
    return datetime(dt.year, dt.month, dt.day)


def datetime_to_date(dt: datetime) -> date:
    """Convert a datetime to a date (discards hrs, mins and secs)."""
    return date(dt.year, dt.month, dt.day)


def timeit(func: callable) -> callable:
    """Times a function."""
    def wrapped(*args, **kwargs):
        start = time.perf_counter()
        rtn = func(*args, **kwargs)
        end = time.perf_counter()
        s = end - start  # in seconds
        duration = f"{s} seconds"
        print(f"Function call '{func.__name__}' duration: {duration}.")
        return rtn
    return wrapped


def m_to_hm(m: int) -> tuple[int, int]:
    """Convert a total amount of minutes into a tuple of hours, minutes."""
    h = m // 60
    m -= h * 60
    return h, m
