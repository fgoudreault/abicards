"""Custom widgets."""
from typing import Any

from PySide6.QtCore import QDate, QPoint, QRect, Qt, Signal
from PySide6.QtGui import QColor, QPainter, QTextCursor
from PySide6.QtWidgets import (
        QApplication, QCalendarWidget, QFrame, QHBoxLayout,
        QLabel, QLineEdit, QPushButton, QTextEdit, QVBoxLayout,
        )

from .routines import get_icon_pixmap, hex_to_rgb, qdate_to_date
from .styles_constants import __PALETTE__


class FocusAwareLineEdit(QLineEdit):
    """A line edit with a signal when focus is changed."""

    focus_changed = Signal()

    def focusInEvent(self, *args, **kwargs) -> Any:
        """Triggered when focusing in this widget."""
        self.focus_changed.emit()
        return super().focusInEvent(*args, **kwargs)

    def focusOutEvent(self, *args, **kwargs) -> Any:
        """Triggered when focusing out this widget."""
        self.focus_changed.emit()
        return super().focusOutEvent(*args, **kwargs)


class SmallCloseButton(QPushButton):
    """A small close button."""

    def __init__(self, *args, **kwargs):
        super().__init__(
                *args, objectName="invisible_btn",
                **kwargs)
        self.passive_px = get_icon_pixmap("x_passive")
        self.hover_px = get_icon_pixmap("x_hover")
        self.click_px = get_icon_pixmap("x_click")
        self.setIcon(self.passive_px)
        self.setFixedSize(30, 30)

    def enterEvent(self, *args, **kwargs) -> Any:
        """Called when mouse starts hovering on button."""
        self.setIcon(self.hover_px)
        return super().enterEvent(*args, **kwargs)

    def leaveEvent(self, *args, **kwargs) -> Any:
        """Called when mouse stops hovering on button."""
        self.setIcon(self.passive_px)
        return super().leaveEvent(*args, **kwargs)

    def mousePressEvent(self, event, *args, **kwargs) -> Any:
        """Called when mouse click on button."""
        if event.buttons() == Qt.LeftButton:
            self.setIcon(self.click_px)
        return super().mousePressEvent(event, *args, **kwargs)


class MinimalSizeTextEdit(QTextEdit):
    """Minimal size text edit.

    This widget changes size depending on its content.
    Also has a feature for placeholder text.
    https://stackoverflow.com/a/54553625/6362595
    """

    focused_in = Signal()
    focused_out = Signal()
    pressed_enter = Signal()
    pressed_escape = Signal()
    height_changed = Signal()

    def __init__(self, *args, disable_enter: bool = False, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.textChanged.connect(self.on_text_changed)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.disabled_enter = disable_enter
        self.fixed_minimum_height = 0
        self.setContentsMargins(0, 0, 0, 0)

    def keyPressEvent(self, event):
        """Called when a key is pressed."""
        if not self.hasFocus():
            return super().keyPressEvent(event)
        if event.key() in (Qt.Key_Return, Qt.Key_Enter) and (
                self.disabled_enter):
            self.pressed_enter.emit()
            return
        if event.key() == Qt.Key_Escape:
            self.pressed_escape.emit()
            return
        return super().keyPressEvent(event)

    def set_fixed_minimum_height(self, minimum: int) -> None:
        """Set the fixed minimum height.

        This sets a different attribute such that when calling the
        setFixedHeight, we don't set lower than the given value.
        """
        self.fixed_minimum_height = minimum

    def on_text_changed(self) -> None:
        """Called when text changed."""
        QApplication.processEvents()  # force update to get good size
        size = self.document().size().toSize()
        newheight = max(self.fixed_minimum_height, size.height() + 5)
        self.setFixedHeight(newheight)
        self.height_changed.emit()

    def showEvent(self, event) -> None:
        """Called when we show this widget."""
        # adjust height size (initial)
        self.textChanged.emit()

    def deselect(self) -> None:
        """Deselect text."""
        cursor = self.textCursor()
        cursor.clearSelection()
        cursor.movePosition(QTextCursor.Start)
        self.setTextCursor(cursor)

    def focusInEvent(self, *args, **kwargs):
        """Called when gaining focus."""
        self.focused_in.emit()
        return super().focusInEvent(*args, **kwargs)

    def focusOutEvent(self, *args, **kwargs):
        """Called when losing focus."""
        self.focused_out.emit()
        return super().focusOutEvent(*args, **kwargs)


class MinimalSizeTextEditWithPlaceHolderText(
        MinimalSizeTextEdit):
    """Same thing as minimal size text edit but has placeholder text."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._placeholderText = ''
        self._placeholderVisible = False
        self.textChanged.connect(self.placeholderVisible)

    def placeholderVisible(self):
        """Return if the placeholder text is visible.

        And force update if required.
        """
        placeholder_currently_visible = self._placeholderVisible
        self._placeholderVisible = (
                self._placeholderText and self.document().isEmpty()
                and not self.hasFocus())
        if self._placeholderVisible != placeholder_currently_visible:
            self.viewport().update()
        return self._placeholderVisible

    def placeholderText(self):
        """Return text used as a placeholder."""
        return self._placeholderText

    def setPlaceholderText(self, text):
        """Set text to use as a placeholder."""
        self._placeholderText = text
        if self.document().isEmpty():
            self.viewport().update()

    def paintEvent(self, event):
        """Override the paint event to add the placeholder text."""
        if self.placeholderVisible():
            painter = QPainter(self.viewport())
            colour = self.palette().text().color()
            colour.setAlpha(128)
            painter.setPen(colour)
            painter.setClipRect(self.rect())
            margin = self.document().documentMargin()
            text_rect = self.viewport().rect().adjusted(margin, margin, 0, 0)
            painter.drawText(
                    text_rect,
                    Qt.AlignTop | Qt.TextWordWrap,
                    self.placeholderText())
        super(QTextEdit, self).paintEvent(event)


class DotsButton(QPushButton):
    """A button representing dots."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setStyleSheet("background-color: transparent; border: 0px")
        self.icons = {
                "passive": get_icon_pixmap("dots_passive.png"),
                "hover": get_icon_pixmap("dots_hover.png"),
                "click": get_icon_pixmap("dots_click.png"),
                }
        self.switch_to_passive_icon()
        self.clicked.connect(self.switch_to_click_icon)
        self.released.connect(self.release_event)

    def release_event(self):
        """Called when releasing the mouse."""
        if self.underMouse():
            self.switch_to_hover_icon()
        else:
            self.switch_to_passive_icon()

    def switch_to_click_icon(self):
        """Switch to the click icon."""
        self.setIcon(self.icons["click"])

    def switch_to_hover_icon(self):
        """Switch to the hover icon."""
        self.setIcon(self.icons["hover"])

    def switch_to_passive_icon(self):
        """Switch to the passive icon."""
        self.setIcon(self.icons["passive"])

    def enterEvent(self, *args, **kwargs):
        """Called when mouse enters button."""
        self.switch_to_hover_icon()
        super().enterEvent(*args, **kwargs)

    def leaveEvent(self, *args, **kwargs):
        """Called when mouse leaves the button."""
        self.switch_to_passive_icon()
        super().leaveEvent(*args, **kwargs)


class RangeCalendar(QCalendarWidget):
    """A calendar which is able to select a range of dates."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setFirstDayOfWeek(Qt.Sunday)
        self.setGridVisible(False)
        self.setHorizontalHeaderFormat(
                QCalendarWidget.SingleLetterDayNames)
        self.setVerticalHeaderFormat(
                QCalendarWidget.NoVerticalHeader)
        self._date0 = None
        self._date1 = None
        self.moving_start = False
        self._range_selection_enabled = False
        self.selectionChanged.connect(self.selected_date_changed)

    def setSelectionMode(self, mode: int) -> Any:
        """Set the selection mode of the calendar."""
        if mode == 2:
            self._range_selection_enabled = True
        else:
            self._range_selection_enabled = False
            # self._date0, self._date1 = None, None
            super().setSelectionMode(mode)
        self.updateCells()

    def selected_date_changed(self) -> None:
        """Called when the selected date changed."""
        self.update_selection()
        self.updateCells()

    def update_selection(self):
        """Update the endpoints of the selection."""
        newdate = self.selectedDate()
        if self._date0 is None:
            self._date0 = newdate
            return
        if not self._range_selection_enabled:
            self._date0 = newdate
            self._date1 = None
            return
        if self._date_equal(newdate, self._date0) and not self.moving_start:
            # switch to move start
            self.moving_start = True
        if self._date1 is not None:
            if self._date_equal(newdate, self._date1) and self.moving_start:
                self.moving_start = False
        if self.moving_start:
            # moving date0. check if clicked date is before or after date1
            if self._date1 is None:
                if self._is_date_before(newdate, self._date0):
                    self._date1 = self._date0
                    self._date0 = newdate
                    return
                self._date1 = newdate
                return
            if self._is_date_before(newdate, self._date1):
                # date is before -> move date0
                self._date0 = newdate
                return
            # date is after current date1 -> switch dates
            self._date0 = self._date1
            self._date1 = newdate
            return
        # moving the end date
        if self._is_date_before(newdate, self._date0):
            # switch dates
            self._date1 = self._date0
            self._date0 = newdate
            return
        self._date1 = newdate
        self.updateCells()

    def _date_equal(self, dt0: QDate, dt1: QDate) -> bool:
        return qdate_to_date(dt0) == qdate_to_date(dt1)

    def paintCell(self, painter: QPainter, rect: QRect, dt: QDate) -> None:
        """Paint cell call."""
        super().paintCell(painter, rect, dt)
        if self._date_equal(dt, self.selectedDate()):
            self._custom_paint(painter, rect, dt, "medium_dark_brown")
            return
        if not self._range_selection_enabled or self._date0 is None or (
                self._date1 is None):
            return  # nothing else to do
        # color the other end point
        if self.moving_start:
            # date1 is lighter
            if self._date_equal(dt, self._date1):
                self._custom_paint(painter, rect, dt, "brown")
                return
        else:
            if self._date_equal(dt, self._date0):
                self._custom_paint(painter, rect, dt, "brown")
                return
        # check if in between
        if self._is_date_within(dt, self._date0, self._date1):
            self._custom_paint(painter, rect, dt, "light_brown")

    def _custom_paint(
            self, painter: QPainter, rect: QRect,
            dt: QDate, color: str) -> None:
        painter.save()
        color = QColor(*hex_to_rgb(__PALETTE__[color]))
        painter.fillRect(rect, color)
        painter.drawText(
                rect, Qt.AlignCenter | Qt.TextSingleLine, str(dt.day()))
        painter.restore()

    def _is_date_within(self, dt: QDate, d0: QDate, d1: QDate):
        """Check if dt is within d0 and d1."""
        return qdate_to_date(d0) < qdate_to_date(dt) < qdate_to_date(d1)

    def _is_date_before(self, date0, date1):
        """Check if date0 is before date 1."""
        # compare years
        if date0.year() < date1.year():
            return True
        if date0.year() > date1.year():
            return False
        # same year so compare months
        if date0.month() < date1.month():
            return True
        if date0.month() > date1.month():
            return False
        # same month, compare days
        if date0.day() < date1.day():
            return True
        # return false on equality
        return False


class SimpleDialogWithHeader(QFrame):
    """Simple dialog frame with a header and close button."""

    def __init__(self, *args, title: str = "", **kwargs):
        super().__init__(*args, **kwargs)
        self.title = title
        self.setup_ui()

    def setup_ui(self) -> None:
        """Setup the dialog UI."""
        self.setLayout(QVBoxLayout())
        self.setup_header()

    def setup_header(self) -> None:
        """Setup the convert dialog header."""
        self.header = QFrame(parent=self)
        self.header.setLayout(QHBoxLayout())
        self.layout().addWidget(self.header)
        self.title_label = QLabel(
                parent=self.header, text=self.title)
        self.title_label.setAlignment(Qt.AlignCenter)
        self.header.layout().addWidget(self.title_label)
        self.close_button = SmallCloseButton(parent=self.header)
        self.header.layout().addWidget(self.close_button)
        self.close_button.clicked.connect(self.hide)

    def move_center(self, parent=None) -> None:
        """Move dialog at the center of its parent."""
        if parent is None:
            parent = self.parent()
        center = parent.geometry().center()
        pos = QPoint(
                center.x() - self.width() // 2,
                center.y() - self.height() // 2)
        self.move(pos)


class HeightForWidthLabel(QLabel):
    """A qlabel with a height for width implementation."""

    def __init__(self, *args, height_factor=1, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.height_factor = height_factor

    def hasHeightForWidth(self) -> bool:
        """Return True."""
        return True

    def heightForWidth(self, width: int) -> int:
        """Return height for given width."""
        return int(round(self.height_factor * width, 0))


class HoverableFrame(QFrame):
    """An hoverable frame."""

    entered = Signal()
    left = Signal()

    def enterEvent(self, *args, **kwargs) -> Any:
        """Called when entering the frame."""
        self.entered.emit()
        return super().enterEvent(*args, **kwargs)

    def leaveEvent(self, *args, **kwargs) -> Any:
        """Called when leaving the frame."""
        self.left.emit()
        return super().leaveEvent(*args, **kwargs)


class HoverableClickableFrame(HoverableFrame):
    """A clickable, hoverable frame."""

    clicked = Signal()
    released = Signal()

    def mousePressEvent(self, *args, **kwargs) -> Any:
        """Called when mouse is pressed inside."""
        self.clicked.emit()
        return super().mousePressEvent(*args, **kwargs)

    def mouseReleaseEvent(self, *args, **kwargs) -> Any:
        """Called when mouse is released inside."""
        self.released.emit()
        return super().mouseReleaseEvent(*args, **kwargs)
