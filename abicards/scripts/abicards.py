"""Main script for the abicards application."""
import argparse
import logging
import os
import stat
import subprocess
import sys

from PySide6.QtWidgets import QApplication

from abicards.main_window import AbicardsMainWindow
from abicards.routines import get_abicards_repo_root


def get_abicards_shellscript() -> str:
    """Get the abicards shellscript path."""
    return os.path.join(os.path.dirname(__file__), "abicards.sh")


def install_script():
    """Installation script."""
    # detect if we use X11 or wayland
    wmclass = None
    if "XDG_SESSION_TYPE" in os.environ:
        session = os.environ["XDG_SESSION_TYPE"]
        if session == "x11":
            wmclass = "abicards.py"
        elif session == "wayland":
            wmclass = "abicards.python3"
    path = os.path.expanduser("~/.local/share/applications/abicards.desktop")
    if os.path.exists(path):
        # update it by removing
        os.remove(path)
    iconpath = os.path.abspath(os.path.join(
            os.path.dirname(__file__), "..", "icons", "desktop_icon.png",
            ))
    binpath = sys.path[0]  # assuming we're in a venv
    # check if there is an activate script in this bin
    for filename in os.listdir(binpath):
        if filename == "activate":
            break
    else:
        raise FileNotFoundError("Not in a venv...")
    activate = os.path.join(binpath, "activate")
    # create shell script to be used by desktop entry
    shellscript = get_abicards_shellscript()
    if os.path.exists(shellscript):
        os.remove(shellscript)
    with open(shellscript, "w") as f:
        f.write("#!/bin/bash\n")
        # first check if the programm is running
        f.write("if [[ $(ps aux |grep execute_abicards_app | grep -v grep) "
                "!= '' ]]; then\n")
        f.write("  echo 'already running'\n")
        f.write("else\n")
        f.write(f"  source {activate}\n")
        # f.write("  export QT_QPA_PLATFORM=xcb\n")
        f.write("  execute_abicards_app\n")
        f.write("fi\n")
        f.write("sleep 1s\n")
    os.chmod(shellscript, os.stat(shellscript).st_mode | stat.S_IXUSR)
    content = [
            "[Desktop Entry]",
            "Type=Application",
            "Name=abicards",
            "Comment=Appli de gestion avec des cartes comme trello.",
            f"Icon={iconpath}",
            f"Exec={shellscript}",
            "Terminal=false",
            ]
    if wmclass is not None:
        content.append(f"StartupWMClass={wmclass}")
    with open(path, "w") as f:
        f.write("\n".join(content))
    # make it executable
    os.chmod(path, os.stat(path).st_mode | stat.S_IXUSR)  # = chmod +x


def upgrade_repo() -> None:
    """Upgrade repository using git."""
    root = get_abicards_repo_root()
    subprocess.run(["git", "-C", f"{root}", "pull", "origin", "main"])


def execute_abicards_app():
    """Execute abicards app."""
    app = QApplication()
    app.setApplicationName("abicards")
    app.setOrganizationDomain("abicards")
    app.loglevel = logging.INFO
    # load style sheet
    with open(
            os.path.join(os.path.dirname(__file__), "../abicards_style.qss"),
            "r") as f:
        app.setStyleSheet(f.read().replace("\n", ""))
    main_window = AbicardsMainWindow()
    main_window.show()
    sys.exit(app.exec_())


def main():
    """Execute the main abicards script entrypoint."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
            "--install", action="store_true",
            help=("Execute installation script."),
            )
    parser.add_argument(
            "--upgrade", "--update", action="store_true",
            help=("Upgrade application using git."),
            )
    args = parser.parse_args()
    if args.install:
        install_script()
    elif args.upgrade:
        upgrade_repo()
    else:
        # execute_abicards_app()
        import subprocess
        subprocess.run(get_abicards_shellscript())


if __name__ == "__main__":
    main()
