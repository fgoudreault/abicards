"""Main window code."""
import json
import logging
import os
import sys
import time
from typing import Any, Mapping

from PySide6.QtCore import Qt
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import (
        QFileDialog,
        QMainWindow, QMessageBox,
        )

import psutil

from .main_menubar import AbicardsMainMenuBar
from .main_toolbar import AbicardsMainToolBar
from .main_widget import AbicardsMainWidget
from .routines import get_generic_file_dialog_for_loading_board, get_icon_path
from .settings import SettingsManager, SettingsWindow
from .update_manager import UpdateManager


class AbicardsMainWindow(QMainWindow):
    """Main abicards window class."""

    def __init__(
            self, *args, loglevel=logging.INFO,
            load_file_on_startup: bool = True,
            alert_user_of_unsaved_changes_on_close: bool = True,
            **kwargs):
        """Main abicards window class.

        Parameters
        ----------
        loglevel: int, optional
            The logging level.
        load_file_on_startup: bool, optional
            If True, we load the file stored in settings on startup
            if there is one.
        alert_user_of_unsaved_changes_on_close: bool, optional
            If True, the user will be alerted of unsaved changes
            before app is closed.
        """
        super().__init__(*args, **kwargs)
        logging.basicConfig()
        self._logger = logging.getLogger("Abicards")
        self._logger.setLevel(loglevel)

        self.setWindowTitle("Untitled")
        self.setWindowIcon(QIcon(get_icon_path("window_icon.png")))
        self.settings_manager = SettingsManager()
        self.settings_window = None
        self._last_save_path_this_session = None
        self.setup_ui()
        self._set_window_geometry()
        self._unsaved_changes = False
        if load_file_on_startup:
            # check if we need to load a board on startup
            self._load_boards_on_startup()
        # check if an update is available
        self.update_manager = None
        if self.settings_manager.get_value(
                "check_update_available_on_startup"
                ).lower() == "true":
            self._check_update_available()
        self.alert_user_of_unsaved_changes_on_close = (
                alert_user_of_unsaved_changes_on_close)

    def _load_boards_on_startup(self):
        path = self.settings_manager.get_value("load_file_on_startup")
        if path == "None":
            return
        self.open_boards(path=path)

    @property
    def unsaved_changes(self) -> bool:
        """Return true if there are unsaved changes."""
        return self._unsaved_changes

    @unsaved_changes.setter
    def unsaved_changes(self, unsaved: bool) -> None:
        """Setter for unsaved changes."""
        self._unsaved_changes = unsaved
        enabled = self.toolbar.save_button.isEnabled()
        if unsaved and not enabled:
            self.toolbar.save_button.setEnabled(True)
            return
        if not unsaved and enabled:
            self.toolbar.save_button.setEnabled(False)
            return

    @property
    def board(self):
        """Return the board currently shown."""
        return self.abicards_widget.board

    def alert_user_unsaved_changes(self):
        """Alert user when closing if unsaved changes.

        Returns True if we quit and False if we abort.
        """
        btn = QMessageBox.warning(
                self, "Warning: possibly unsaved changes!",
                (
                 "There are possible unsaved changes and leaving now will "
                 "discard them. Are you sure you want to leave?"
                 ),
                buttons=QMessageBox.Abort | QMessageBox.Yes,
                )
        if btn == QMessageBox.Yes:
            return True
        return False

    def closeEvent(self, event) -> None:
        """Handle close events."""
        self.settings_manager.set_value("geometry", self.saveGeometry())
        # self.settings_manager.setValue("state", self.saveState())
        self.settings_manager.save_settings()
        if self.unsaved_changes and (
                self.alert_user_of_unsaved_changes_on_close):
            # alert user that ther is unsaved data
            quit_ = self.alert_user_unsaved_changes()
            if not quit_:
                return event.ignore()
        self._logger.info("Closing application. Bye bye :)")
        super().closeEvent(event)

    def open_boards(self, path: str = None):
        """Open a json file containing the board data.

        A dialog will be open to ask user which file to load.

        Parameters
        ----------
        path: str, optional
            The path to load. If None, a file dialog opens for user.
        """
        if path is None:
            last_dir = self.settings_manager.get_value(
                    "last_open_card_file_dir")
            last_base = self.settings_manager.get_value(
                    "last_open_card_file_basename")
            path = QFileDialog.getOpenFileName(
                    parent=self, caption="Open file",
                    dir=os.path.join(last_dir, last_base),
                    selectedFilter="JSON (*.json)",
                    filter="JSON (*.json)",
                    )[0]
        self._logger.info(f"Loading board from file {path}.")
        self._set_window_title_from_filename(path)
        try:
            with open(path, "r") as f:
                data = json.load(f)
        except Exception as err:
            self._logger.exception(err)
            self._logger.error(f"Cannot read file '{path}'.")
            return
        try:
            self.load_json_data(data)
            self._last_save_path_this_session = path
        except Exception as err:
            self._logger.exception(err)
            self._logger.error(
                    f"Cannot load data from file '{path}'.")

    def open_settings(self):
        """Open the settings window."""
        self._logger.info("Opening settings from main window.")
        if self.settings_window is None:
            self.settings_window = SettingsWindow(self.settings_manager)
        self.settings_window.show()

    def get_json_data(self):
        """Get a json dictionary of board data to be loaded later."""
        return {
                "abicards": self.abicards_widget.get_json_data(),
                }

    def load_json_data(self, data: Mapping[str, Any]) -> None:
        """Load board from a json dictionary."""
        self.abicards_widget.load_json_data(data["abicards"])

    def save(self):
        """Save cards to predefined location."""
        if self._last_save_path_this_session is None:
            self.save_as()
            return
        # last_dir = self.settings_manager.get_value(
        #         "last_open_card_file_dir")
        # last_base = self.settings_manager.get_value(
        #         "last_open_card_file_basename")
        # file_name = os.path.join(last_dir, last_base)
        file_name = self._last_save_path_this_session
        self._logger.info(f"Saving project as {file_name}.")
        jsondata = self.get_json_data()
        with open(file_name, "w") as f:
            json.dump(jsondata, f, indent=4)
        self.unsaved_changes = False  # reset
        # set window title to the basename of file without extension
        self._set_window_title_from_filename(file_name)

    def _set_window_title_from_filename(self, filename: str) -> None:
        base = os.path.basename(filename)
        split = base.split(".")
        if len(split) > 1:
            base_no_ext = ".".join(base.split(".")[:-1])
        else:
            base_no_ext = split[0]
        self.setWindowTitle(base_no_ext)

    def save_as(self):
        """Save card to a location that will be asked."""
        dialog = get_generic_file_dialog_for_loading_board(
                self.settings_manager, self)
        file_name = dialog.getSaveFileName(
                filter="JSON (*.json)",
                selectedFilter="JSON (*.json)")[0]
        if not file_name:
            self._logger.info("not saving (cancelled by user)")
            return
        if not file_name.endswith(".json"):
            file_name += ".json"
        self.settings_manager.set_value(
                "last_open_card_file_dir", os.path.dirname(file_name))
        self.settings_manager.set_value(
                "last_open_card_file_basename", os.path.basename(file_name))
        self._last_save_path_this_session = file_name
        self.save()

    def setup_ui(self):
        """Build the main window gui."""
        self.abicards_widget = AbicardsMainWidget(parent=self)
        self.setCentralWidget(self.abicards_widget)
        # label = QLabel("test", parent=self)
        # self.setStyleSheet("background-color: blue")
        # self.setCentralWidget(label)
        self.setup_menubar()
        self._build_toolbar()

    def _build_toolbar(self) -> None:
        """Build the toolbar."""
        self.toolbar = AbicardsMainToolBar(parent=self)
        self.addToolBar(self.toolbar)

    def setup_menubar(self) -> None:
        """Build the menu bar."""
        self.menubar = AbicardsMainMenuBar(parent=self)
        self.setMenuBar(self.menubar)

    def _set_window_geometry(self):
        # look in config if a previous one was stored
        geo = self.settings_manager.get_value("geometry")
        if geo is not None:
            self.restoreGeometry(geo)
            return
        geometry = self.screen().availableGeometry()
        width = geometry.width() * 0.5
        height = geometry.height() * 0.5
        self.resize(width, height)

    def update_app(self) -> None:
        """Update the app and restart it."""
        self._logger.info("Update app.")
        if self.update_manager is None:
            self.update_manager = UpdateManager(
                    parent=self,
                    )
        self.update_manager.launch_update()

    def _check_update_available(self) -> None:
        self._logger.info("Checking if an update is available.")
        if self.update_manager is None:
            self.update_manager = UpdateManager(parent=self)
        self.update_manager.check_update_available()

    def restart_app(self) -> None:
        """Restart the app."""
        closed = self.close()
        if not closed:
            # close event was aborted -> do not restart
            return
        time.sleep(2)  # wait 2 seconds to process closing events
        # https://stackoverflow.com/a/33334183/6362595
        try:
            p = psutil.Process(os.getpid())
            for handler in p.open_files() + p.connections():
                os.close(handler.fd)
        except Exception as e:
            self._logger.error(e)
        python = sys.executable
        os.execl(python, python, *sys.argv)

    def new_version_available(self) -> None:
        """Inform user a new version of the app is available."""
        box = QMessageBox(parent=self)
        box.setWindowTitle("New version is available!")
        box.setText(
                "A new version is available. Do you want to update?"
                 )
        box.setStandardButtons(
                QMessageBox.Yes | QMessageBox.No,
                )
        box.setDefaultButton(QMessageBox.Yes)
        box.setWindowFlags(box.windowFlags() | Qt.Popup)
        btn = box.exec()
        if btn == QMessageBox.Yes:
            self.update_app()
