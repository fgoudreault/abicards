"""Main tool bar module."""
from PySide6.QtWidgets import QPushButton, QToolBar


class AbicardsMainToolBar(QToolBar):
    """Abicards main tool bar."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._logger = self.parent()._logger
        self.setup_ui()

    def setup_ui(self) -> None:
        """Setup the toolbar ui."""
        self.save_button = QPushButton(
                "Save", parent=self, objectName="transparent_btn")
        self.addWidget(self.save_button)
        self.save_button.setEnabled(False)
        self.save_button.clicked.connect(self.parent().save)
        # add card list
        self.add_card_list_button = QPushButton(
                "+ Add card list", parent=self, objectName="transparent_btn")
        self.addWidget(self.add_card_list_button)
        self.add_card_list_button.clicked.connect(
                self.parent().abicards_widget.add_card_list)
        # add timer card
        self.add_pomodoro_button = QPushButton(
                "+ Add pomodoro", parent=self, objectName="transparent_btn")
        self.add_pomodoro_button.clicked.connect(
                self.parent().abicards_widget.add_pomodoro)
        self.addWidget(self.add_pomodoro_button)
        # add planer card
        self.add_planer_button = QPushButton(
                "+ Add planer", parent=self, objectName="transparent_btn")
        self.add_planer_button.clicked.connect(
                self.parent().abicards_widget.add_planer)
        self.addWidget(self.add_planer_button)
