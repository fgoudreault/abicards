"""Unittest module for checklist items."""
from PySide6.QtCore import Qt

import pytest


class TestChecklistItem:
    """Test case for checklist items."""

    @pytest.fixture(autouse=True)
    def setup(self, qtbot, abicards_with_one_checklist_item) -> None:
        """Setup fixture for checklist items unittests."""
        self.qtbot = qtbot
        self.abicards = abicards_with_one_checklist_item
        self.qtbot.addWidget(self.abicards)
        self.board = self.abicards.abicards_widget.boards[0]
        self.card_list = self.board.card_lists[0]
        self.card = self.card_list.cards[0]
        self.card.toggle_full_edit()
        self.full_edit_dialog = self.card.full_card_edit_dialog
        self.checklist_widget = self.full_edit_dialog.checklist_fields[0]
        self.checklist = self.checklist_widget.checklist
        self.checklist_item = self.checklist.checklist_items[0]
        self.checklist_item_widget = (
                self.checklist_widget.checklist_item_widgets[0])

    def test_convert_dialog_buttons_enabled(self) -> None:
        """Test that convert dialog buttons are always enabled."""
        self.checklist_item_widget.toggle_edit_item()
        ed = self.checklist_item_widget.edit_item_dialog
        assert ed.convert_to_dialog is None
        self.qtbot.mouseClick(ed.convert_button, Qt.LeftButton)
        assert ed.convert_to_dialog is not None
        cd = ed.convert_to_dialog
        assert cd.board_button.isEnabled()
        assert cd.card_list_button.isEnabled()
        assert cd.card_button.isEnabled()
        assert cd.checklist_button.isEnabled()
        assert cd.checklist_item_button is None
