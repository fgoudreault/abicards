"""Coding quality tests."""
from flake8.api import legacy as flake8


def test_code_quality():
    """Test the code quality."""
    style_guide = flake8.get_style_guide()
    report = style_guide.check_files()
    msg = "Found code syntax errors (and warnings)!\n"
    nerr = f"Number of errors = {report.total_errors}.\n"
    assert report.total_errors == 0, msg + nerr
