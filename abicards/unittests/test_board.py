"""Module for unittesting the boards."""
from PySide6.QtCore import Qt

import pytest

from .routines import random_string


class TestBoard:
    """Test class for Board objects."""

    @pytest.fixture(autouse=True)
    def setup(self, qtbot, abicards_with_one_board):
        """Setup fixture for Board object unittests."""
        self.qtbot = qtbot
        self.abicards = abicards_with_one_board
        self.board = self.abicards.abicards_widget.boards[0]

    @pytest.mark.parametrize("title_length", [0, 1, 10, 100])
    def test_add_card_list(self, title_length: int) -> None:
        """Test adding a card list to a board works."""
        assert len(self.board.card_lists) == 0
        self.board.add_card_list()
        assert len(self.board.card_lists) == 1
        title = random_string(title_length)
        self.board.add_card_list(load_data={"title": title})
        assert len(self.board.card_lists) == 2
        assert self.board.card_lists[-1].title == title

    def test_non_empty_board_cannot_be_converted(self) -> None:
        """Test that a non-empty board cannot be converted."""
        assert len(self.board.card_lists) == 0
        # check that it can be converted
        assert self.board.can_be_converted()
        # open convert dialog and check all related buttons are enabled
        self.board.toggle_edit_dialog()
        edit_dialog = self.board.edit_dialog
        with self.qtbot.waitExposed(edit_dialog):
            pass
        assert edit_dialog.convert_button.isEnabled()
        assert edit_dialog.isVisible()
        self.qtbot.mouseClick(edit_dialog.convert_button, Qt.LeftButton)
        assert edit_dialog.convert_to_dialog is not None
        with self.qtbot.waitExposed(edit_dialog.convert_to_dialog):
            pass
        convert_dialog = edit_dialog.convert_to_dialog
        assert convert_dialog.isVisible()
        assert convert_dialog.board_button is None
        assert convert_dialog.card_list_button.isEnabled()
        assert convert_dialog.card_button.isEnabled()
        assert convert_dialog.checklist_button.isEnabled()
        assert convert_dialog.checklist_item_button.isEnabled()
        # dismiss dialog
        self.board.close_subdialogs()
        assert not convert_dialog.isVisible()
        # add 1 card list and retoggle the convert dialog
        # all buttons should be disabled
        self.board.add_card_list()
        self.board.toggle_edit_dialog()
        self.qtbot.mouseClick(edit_dialog.convert_button, Qt.LeftButton)
        assert convert_dialog.board_button is None
        assert not convert_dialog.card_list_button.isEnabled()
        assert not convert_dialog.card_button.isEnabled()
        assert not convert_dialog.checklist_button.isEnabled()
        assert not convert_dialog.checklist_item_button.isEnabled()
