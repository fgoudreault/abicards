"""Unittest routines."""
import random
import string


def random_string(length: int = 10) -> str:
    """Generate a random string.

    Parameters
    ----------
    length: int, optional
        The string length.
    """
    chars = string.ascii_uppercase + string.digits + " \n"
    return "".join(random.choices(chars, k=length))
