"""Global level fixtures."""
import asyncio

import pytest

from .routines import random_string
from ..main_window import AbicardsMainWindow


@pytest.fixture(scope="session")
def event_loop():
    """Event loop fixture that can run session scoped async fixtures."""
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture
def abicards_with_one_board():
    """Fixture for the abicards app with one board."""
    window = AbicardsMainWindow(
            load_file_on_startup=False,
            alert_user_of_unsaved_changes_on_close=False,
            )
    window.show()
    widget = window.abicards_widget
    widget.add_board()
    yield window


@pytest.fixture
def abicards_with_one_card_list(abicards_with_one_board):
    """Fixture of the whole app with one card list."""
    board = abicards_with_one_board.abicards_widget.boards[0]
    board.add_card_list()
    yield abicards_with_one_board


@pytest.fixture
def abicards_with_one_card(abicards_with_one_card_list):
    """Fixture for the whole app with one card."""
    board = abicards_with_one_card_list.abicards_widget.boards[0]
    card_list = board.card_lists[0]
    card_list.add_card()
    yield abicards_with_one_card_list


@pytest.fixture
def abicards_with_one_checklist(abicards_with_one_card):
    """Fixture for the whole app with one checklist."""
    board = abicards_with_one_card.abicards_widget.boards[0]
    card_list = board.card_lists[0]
    card = card_list.cards[0]
    card.add_checklist()
    checklist = card.checklists[0]
    checklist.set_title(random_string())
    yield abicards_with_one_card


@pytest.fixture
def abicards_with_one_checklist_item(abicards_with_one_checklist):
    """Fixture for the whole app with one checklist item."""
    board = abicards_with_one_checklist.abicards_widget.boards[0]
    card_list = board.card_lists[0]
    card = card_list.cards[0]
    checklist = card.checklists[0]
    checklist.add_checklist_item()
    item = checklist.checklist_items[0]
    item.set_title(random_string())
    yield abicards_with_one_checklist
