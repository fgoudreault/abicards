"""Test module for Cards."""
from PySide6.QtCore import Qt

import pytest


class TestCards:
    """Test class for cards unittests."""

    @pytest.fixture(autouse=True)
    def setup(self, qtbot, abicards_with_one_card) -> None:
        """Setup fixture for the cards unittests."""
        self.qtbot = qtbot
        self.abicards = abicards_with_one_card
        self.qtbot.addWidget(self.abicards)
        self.board = self.abicards.abicards_widget.boards[0]
        self.card_list = self.board.card_lists[0]
        self.card = self.card_list.cards[0]

    def test_convert_dialog_btns_disabled_if_card_not_empty(self) -> None:
        """Test convert dialog buttons are disabled if card is not empty.

        Also check they are enabled if card is empty.
        """
        assert len(self.card.checklists) == 0
        self.card.toggle_full_edit()
        fd = self.card.full_card_edit_dialog
        assert fd is not None
        ntb = fd.non_title_buttons
        assert ntb.convert_to_dialog is None
        self.qtbot.mouseClick(ntb.convert_button, Qt.LeftButton)
        assert ntb.convert_to_dialog is not None
        cd = ntb.convert_to_dialog
        assert cd.board_button.isEnabled()
        assert cd.card_list_button.isEnabled()
        assert cd.card_button is None
        assert cd.checklist_button.isEnabled()
        assert cd.checklist_item_button.isEnabled()
        # dismiss dialog, add checklist and check all buttons are disabled
        self.card.toggle_full_edit()
        self.card.add_checklist()
        assert len(self.card.checklists) == 1
        self.card.toggle_full_edit()
        self.qtbot.mouseClick(ntb.convert_button, Qt.LeftButton)
        assert not cd.board_button.isEnabled()
        assert not cd.card_list_button.isEnabled()
        assert cd.card_button is None
        assert not cd.checklist_button.isEnabled()
        assert not cd.checklist_item_button.isEnabled()
