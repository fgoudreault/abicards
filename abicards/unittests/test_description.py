"""Test the description label of the full card edit dialog."""
from .routines import random_string


def test_card_description_wraps(qtbot, abicards_with_one_card) -> None:
    """Test that a card description with a super long description wraps."""
    qtbot.addWidget(abicards_with_one_card)
    # open card full edit dialog
    board = abicards_with_one_card.abicards_widget.boards[0]
    card_list = board.card_lists[0]
    card = card_list.cards[0]
    card.toggle_full_edit()
    full_edit_dialog = card.full_card_edit_dialog
    with qtbot.waitExposed(full_edit_dialog):
        pass
    description_widget = full_edit_dialog.non_title_fields.description_field
    desc_label = description_widget.card_description_button
    assert desc_label.text() == ""
    height = desc_label.height()
    assert not desc_label.isVisible()
    desc = random_string(length=10000)
    desc_label.setText(desc)
    desc_label.adjustSize()
    assert desc_label.text() == desc
    assert desc_label.description_label.wordWrap()
    assert desc_label.height() > height
