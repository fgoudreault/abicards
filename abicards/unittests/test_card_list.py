"""Module for unittesting the card lists."""
from PySide6.QtCore import Qt

import pytest


class TestCardList:
    """Test case for card lists."""

    @pytest.fixture(autouse=True)
    def setup(self, qtbot, abicards_with_one_card_list) -> None:
        """Setup fixture for card list unittests."""
        self.qtbot = qtbot
        self.abicards = abicards_with_one_card_list
        self.qtbot.addWidget(self.abicards)
        with self.qtbot.waitExposed(self.abicards):
            pass
        self.board = self.abicards.abicards_widget.boards[0]
        self.card_list = self.board.card_lists[0]

    def test_added_card_list_has_focus_on_title_edit(self) -> None:
        """Test that, when adding a card list, its title is in edit mode."""
        with self.qtbot.waitExposed(self.card_list):
            assert self.card_list.edit_title_lineedit is not None
            assert self.card_list.edit_title_lineedit.hasFocus()
            assert self.card_list.edit_title_lineedit.height() > 30

    def test_convert_dialog_btns_enabled_or_not(self) -> None:
        """Test when card list is non-empty, converting is disabled.

        Test also enabled otherwise.
        """
        assert len(self.card_list.cards) == 0
        # conerting should be enabled
        self.card_list.toggle_edit_menu()
        edit_menu = self.card_list.edit_menu
        assert edit_menu.convert_to_dialog is None
        self.qtbot.mouseClick(edit_menu.convert_button, Qt.LeftButton)
        assert edit_menu.convert_to_dialog is not None
        assert edit_menu.convert_to_dialog.board_button.isEnabled()
        assert edit_menu.convert_to_dialog.card_button.isEnabled()
        assert edit_menu.convert_to_dialog.checklist_button.isEnabled()
        assert edit_menu.convert_to_dialog.checklist_item_button.isEnabled()
        assert edit_menu.convert_to_dialog.card_list_button is None
        # dismiss and add one card. convert dialog btns should be disabled
        self.card_list.close_subdialogs()
        assert not edit_menu.convert_to_dialog.isVisible()
        self.card_list.add_card()
        self.card_list.toggle_edit_menu()
        self.qtbot.mouseClick(edit_menu.convert_button, Qt.LeftButton)
        assert not edit_menu.convert_to_dialog.board_button.isEnabled()
        assert not edit_menu.convert_to_dialog.card_button.isEnabled()
        assert not edit_menu.convert_to_dialog.checklist_button.isEnabled()
        assert not (
                edit_menu.convert_to_dialog.checklist_item_button.isEnabled())
        assert edit_menu.convert_to_dialog.card_list_button is None
