"""Unittest module for the checklists."""
from PySide6.QtCore import Qt

import pytest

from .routines import random_string
from ..routines import get_widget_background_color


class TestChecklistWidget:
    """Test cases for a checklist widget within an opened card dialog."""

    @pytest.fixture(autouse=True)
    def setup(self, qtbot, abicards_with_one_checklist) -> None:
        """Setup the tests."""
        self.qtbot = qtbot
        self.abicards = abicards_with_one_checklist
        # open card full edit dialog
        self.qtbot.addWidget(self.abicards)
        with self.qtbot.waitExposed(self.abicards):
            pass
        self.board = self.abicards.abicards_widget.boards[0]
        self.card_list = self.board.card_lists[0]
        self.card = self.card_list.cards[0]
        self.card.toggle_full_edit()
        self.full_edit_dialog = self.card.full_card_edit_dialog
        self.checklist_widget = (
                self.full_edit_dialog.non_title_fields.checklist_fields[0])
        self.checklist = self.checklist_widget.checklist
        with self.qtbot.waitExposed(self.checklist_widget):
            pass

    def test_add_item_button_opens_form(self) -> None:
        """Test that when clicked on add btn, the checklist item form opens."""
        assert self.checklist_widget.add_item_widget is None
        self.qtbot.mouseClick(
                self.checklist_widget.add_item_btn, Qt.LeftButton)
        assert self.checklist_widget.add_item_widget is not None
        with self.qtbot.waitExposed(self.checklist_widget.add_item_widget):
            pass
        assert self.checklist_widget.add_item_widget.isVisible()

    @pytest.mark.parametrize("title_length", [0, 1, 100, 10000])
    def test_add_item(self, title_length) -> None:
        """Test adding a checklist item works."""
        # open form
        self.checklist_widget.toggle_add_item_mode()
        # random checklist item name
        title = random_string(title_length)
        self.checklist_widget.add_item_widget.set_item_title(title)
        assert self.checklist_widget.add_item_widget.get_item_title() == title
        # click the add btn
        nitem = len(self.checklist.checklist_items)
        self.qtbot.mouseClick(
                self.checklist_widget.add_item_widget.add_btn, Qt.LeftButton)
        # check that the checklist item was correctly added
        assert len(self.checklist.checklist_items) == nitem + 1
        item = self.checklist.checklist_items[-1]
        assert item.title == title
        # check a checklist item widget is present
        assert len(self.checklist_widget.checklist_item_widgets) == nitem + 1
        checklist_item_widget = (
                self.checklist_widget.checklist_item_widgets[-1])
        assert checklist_item_widget.title_label.get_title() == title
        # now check that checklist widget is ready to accept a new item
        assert self.checklist_widget.add_item_widget.isVisible()
        assert self.checklist_widget.add_item_widget.get_item_title() == ""

    def test_title_edit_mode(self) -> None:
        """Test that a checklist title can be edited when clicking on it."""
        title_widget = self.checklist_widget.title_widget
        # check that clicking on title makes the lineedit appeaers
        assert title_widget.title_lineedit is None
        self.qtbot.mouseRelease(title_widget, Qt.NoButton)
        assert title_widget.title_lineedit is not None
        with self.qtbot.waitExposed(title_widget.title_lineedit):
            pass
        assert title_widget.title_lineedit.isVisible()
        assert not title_widget.title_label.isVisible()
        assert title_widget.title_lineedit.hasFocus()
        assert title_widget.title_lineedit.hasSelectedText()
        assert title_widget.title_lineedit.selectedText() == (
                title_widget.title_label.text())
        # for some reason, I am unable to test shortcuts...
        # check that they are enabled at least
        assert title_widget.escape_shortcut is not None
        assert title_widget.escape_shortcut.isEnabled()
        assert not self.full_edit_dialog.escape_shortcut.isEnabled()
        # test leaving edit mode
        title_widget.cancel()
        with self.qtbot.waitExposed(title_widget.title_label):
            pass
        assert title_widget.title_label.isVisible()
        assert not title_widget.title_lineedit.isVisible()
        assert title_widget.title_label.text() == self.checklist.title
        # change edit title via pressing key
        title_widget.toggle_edit_mode()
        with self.qtbot.waitExposed(title_widget.title_lineedit):
            pass
        assert title_widget.enter_shortcut.isEnabled()
        assert title_widget.return_shortcut.isEnabled()
        newtitle = random_string()
        title_widget.title_lineedit.setText(newtitle)
        title_widget.save()
        with self.qtbot.waitExposed(title_widget.title_label):
            pass
        assert self.checklist.title == newtitle
        assert title_widget.title_label.text() == newtitle

    def test_title_can_be_hovered(self) -> None:
        """Test that the checklist title widget has a hovered color."""
        title_widget = self.checklist_widget.title_widget
        # check that the color changes when hovering the checklist widget
        self.qtbot.mouseMove(self.full_edit_dialog)
        assert not title_widget.underMouse()
        color = get_widget_background_color(title_widget)
        assert color is not None
        self.qtbot.mouseMove(title_widget.title_label)
        # wait a bit since sometimes this assert fails randomly
        self.qtbot.wait(10)
        assert title_widget.underMouse()
        color_hovered = get_widget_background_color(title_widget)
        assert color_hovered is not None
        assert color_hovered != color

    def test_title_edit_mode_dismissed_when_clicking_out(
            self) -> None:
        """Test when editing checklist title, clicking out cancels changes."""
        title_widget = self.checklist_widget.title_widget
        # start edit mode
        title_widget.toggle_edit_mode()
        with self.qtbot.waitExposed(title_widget.title_lineedit):
            pass
        self.qtbot.mouseClick(self.full_edit_dialog, Qt.NoButton)
        with self.qtbot.waitExposed(title_widget.title_label):
            pass
        assert title_widget.title_label.isVisible()
        assert not title_widget.title_lineedit.isVisible()
        assert title_widget.title_label.text() == self.checklist.title

    def test_convert_dialog_buttons_disabled_if_not_empty(self) -> None:
        """Test convert dialog buttons are disabled if checklist not empty.

        Also test they are enabled if checklist is empty.
        """
        assert len(self.checklist.checklist_items) == 0
        self.checklist_widget.toggle_edit_menu()
        ed = self.checklist_widget.edit_dialog
        assert ed.convert_to_dialog is None
        self.qtbot.mouseClick(ed.convert_button, Qt.LeftButton)
        assert ed.convert_to_dialog is not None
        cd = ed.convert_to_dialog
        assert cd.board_button.isEnabled()
        assert cd.card_list_button.isEnabled()
        assert cd.card_button.isEnabled()
        assert cd.checklist_button is None
        assert cd.checklist_item_button.isEnabled()
        # close dialog, add item and check if convert button is disabled
        self.checklist_widget.toggle_edit_menu()
        assert not cd.isVisible()
        self.checklist.add_checklist_item()
        assert len(self.checklist.checklist_items) == 1
        self.checklist_widget.toggle_edit_menu()
        assert not ed.convert_button.isEnabled()
