"""Module for unittesting the main widget."""
from abicards.main_widget import AbicardsMainWidget


def test_add_board(qtbot):
    """Test adding through api a board works."""
    widget = AbicardsMainWidget()
    qtbot.addWidget(widget)
    assert hasattr(widget, "board_tabs")
    assert widget.board_tabs.count() == 1

    widget.add_board()
    assert widget.board_tabs.count() == 2

    widget.add_board(load_from={"title": "title"})
    assert widget.board_tabs.count() == 3
    assert widget.boards[-1].title == "title"
