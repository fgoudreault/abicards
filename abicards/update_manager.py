"""Update manager module."""
import logging
import re
import subprocess
import time

from PySide6.QtCore import (
        QObject, QThread, QTimer, Signal,
        )
from PySide6.QtWidgets import (
        QFrame, QGridLayout, QLabel, QProgressDialog,
        QPushButton,
        )

from .routines import (
        get_abicards_repo_root, get_loglevel, get_main_window,
        )
from .styles_constants import __BUTTON_HEIGHT_BIG__


class BaseUpdateWorker(QObject):
    """Base class for update worker objects."""

    finished = Signal()
    progress = Signal()
    nsteps = None

    def __init__(self, *args, loglevel=logging.INFO, **kwargs):
        super().__init__(*args, **kwargs)
        logging.basicConfig()
        self._logger = logging.getLogger("Updater")
        self._logger.setLevel(loglevel)
        if self.nsteps is None:
            raise ValueError("Number of steps unspecified.")

    def compare_versions(self) -> bool:
        """Compare version from current app and upstream one.

        Returns
        -------
        bool: True if versions are the same. False otherwise.
        """
        # compare git hash using git ls-remote
        root = get_abicards_repo_root()
        output = subprocess.check_output(
                ["git", "-C", root, "ls-remote", "origin"],
                )
        remote_hash = re.split(r'\t+', output.decode('ascii'))[0]
        # local hash
        local_hash = subprocess.check_output(
                ["git", "-C", root, "rev-parse", "HEAD"],
                ).decode("ascii").strip()
        self._logger.info(f"Local commit hash = {local_hash}")
        self._logger.info(f"Remote commit hash = {remote_hash}")
        # compare remote version and self version
        if remote_hash != local_hash:
            self._logger.info(
                    "Different version detected -> new version available.",
                    )
            return False
        return True


class UpdateWorker(BaseUpdateWorker):
    """Update worker for another thread.

    Actually does the upgrade.
    """

    nsteps = 4

    def update_workflow(self):
        """Function (executed in a thread) handles the workflow."""
        finished = self.compare_versions()
        if finished:
            time.sleep(0.5)
            self.finished.emit()
            return
        time.sleep(0.5)
        self.progress.emit()
        self.pull_new_version()
        time.sleep(0.5)
        self.progress.emit()
        self.pip_install_new_version()
        time.sleep(0.5)
        self.progress.emit()
        self.abicards_install()
        time.sleep(0.5)
        self.finished.emit()

    def abicards_install(self):
        """Run the install script of abicards."""
        subprocess.run(["abicards", "--install"])

    def pip_install_new_version(self):
        """Install new version in virtual environment."""
        root = get_abicards_repo_root()
        subprocess.run(["pip", "install", "-e", root])

    def pull_new_version(self):
        """Pull a new version using git."""
        root = get_abicards_repo_root()
        subprocess.run(["git", "-C", root, "pull", "origin", "main"])


class UpdateAvailableCheckerWorker(BaseUpdateWorker):
    """Update worker that only checks if an update is available."""

    new_version_available = Signal()
    nsteps = 1

    def workflow(self):
        """Update check workflow."""
        different_version = self.compare_versions()
        self.finished.emit()
        if not different_version:
            self.new_version_available.emit()


class UpdateManager(QObject):
    """Update manager widget used to update the app."""

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._logger = logging.getLogger("UpdateManager")
        self._logger.setLevel(get_loglevel())
        self.waitdialog = None
        self.thread_check_update_avail = None
        self.thread = None
        self.restart_required_dialog = None
        self._worker = None
        self.worker_check_update_avail = None

    @property
    def worker(self):
        """The update worker."""
        if self._worker is None:
            self._worker = UpdateWorker(loglevel=self._logger.level)
        return self._worker

    def check_update_available(self):
        """Check if an update is available."""
        if self.thread_check_update_avail is not None:
            if self.thread_check_update_avail.isRunning():
                self._logger.error("Another update checker is running.")
                return
        self.thread_check_update_avail = QThread()
        self.worker_check_update_avail = (
                UpdateAvailableCheckerWorker(loglevel=self._logger.level))
        self.worker_check_update_avail.moveToThread(
                self.thread_check_update_avail)
        self.thread_check_update_avail.started.connect(
                self.worker_check_update_avail.workflow)
        self.worker_check_update_avail.new_version_available.connect(
                self.parent().new_version_available)
        self.worker_check_update_avail.finished.connect(
                self.thread_check_update_avail.exit)
        self.thread_check_update_avail.start()

    def get_wait_dialog(self):
        """Launches the wait dialog."""
        dialog = QProgressDialog(
                "Checking upstream version...", "Cancel",
                0, self.worker.nsteps,
                parent=self.parent(),
                )
        dialog.setWindowTitle("Updating...")
        dialog.setMinimumDuration(0)
        dialog.setModal(True)
        dialog.setValue(dialog.minimum())
        dialog.setAutoClose(False)
        dialog.setAutoReset(False)
        return dialog

    def launch_update(self):
        """Launch update workflow."""
        # check version and compare with upstream version
        self.waitdialog = self.get_wait_dialog()
        self.waitdialog.show()
        self.thread = QThread()
        self.waitdialog.canceled.connect(self.thread.terminate)
        self.worker.moveToThread(self.thread)
        self.worker.finished.connect(self.done)
        self.worker.progress.connect(self.progress)
        self.thread.started.connect(self.worker.update_workflow)
        self.thread.start()

    def progress(self):
        """Update progress +1."""
        # we have an update available
        if self.waitdialog.value() == 0:
            self.waitdialog.setLabelText("Downloading new version...")
            self.waitdialog.setValue(1)
        elif self.waitdialog.value() == 1:
            self.waitdialog.setLabelText("Installing new version...")
            self.waitdialog.setValue(2)
        elif self.waitdialog.value() == 2:
            self.waitdialog.setLabelText("Running post-install script...")
            self.waitdialog.setValue(3)

    def done(self):
        """Update is done."""
        extra_time = 0
        need_restart = True
        if self.waitdialog.value() == 0:
            # already at newest version
            self.waitdialog.setLabelText("You already have newest version.")
            extra_time = 500
            need_restart = False
        self.waitdialog.setValue(self.waitdialog.maximum())
        self.thread.exit()
        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.waitdialog.close)
        self.timer.start(1000 + extra_time)
        if need_restart:
            QTimer.singleShot(
                    1000 + extra_time,
                    self.open_restart_required_dialog)
        # remove thread
        self.thread = None

    def open_restart_required_dialog(self):
        """Open the restart required dialog."""
        if self.restart_required_dialog is None:
            self.restart_required_dialog = RestartRequiredDialog(
                    parent=self.parent().board,
                    )
        self.parent().settings_window.hide()
        self.restart_required_dialog.show()


class RestartRequiredDialog(QFrame):
    """The restart required dialog."""

    def __init__(self, *args, **kwargs):
        super().__init__(
                *args, objectName="dark_grey_dialogs",
                **kwargs)
        self.setup_ui()

    def setup_ui(self):
        """Setup the ui."""
        self.layout = QGridLayout()
        self.setLayout(self.layout)
        self.text_label = QLabel(
                text="A new version was installed.\n Restart is required.",
                parent=self, objectName="transparent_centered",
                )
        self.text_label.setWordWrap(True)
        self.setFixedSize(
                300, 2.5 * __BUTTON_HEIGHT_BIG__)
        self.layout.addWidget(self.text_label, 0, 0, 1, 2)
        self.restart_button = QPushButton(
                text="Restart", parent=self, objectName="red_btn")
        self.restart_button.setFixedHeight(__BUTTON_HEIGHT_BIG__)
        self.restart_button.clicked.connect(get_main_window().restart_app)
        self.layout.addWidget(self.restart_button, 1, 0, 1, 1)
        self.cancel_button = QPushButton(
                text="Nevermind", parent=self,
                objectName="transparent_borderless_btn",
                )
        self.cancel_button.clicked.connect(self.close)
        self.cancel_button.setFixedHeight(__BUTTON_HEIGHT_BIG__)
        self.layout.addWidget(self.cancel_button, 1, 1, 1, 1)

        # move dialog at center of screen
        self.move((self.parent().width() - self.width()) / 2,
                  (self.parent().height() - self.height()) / 2,
                  )
        # highlight self
        self.parent().highlight_widgets(self)

    def close(self):
        """Close the dialog."""
        # unhighlight this widget
        self.parent().unhighlight()
        super().close()
