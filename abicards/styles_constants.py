"""Styles python constants."""

__BUTTON_HEIGHT_MEDIUM__ = 30
__BUTTON_HEIGHT_BIG__ = 50
__DELETE_BUTTON_WIDTH__ = 100
__FULL_CARD_EDIT_DIALOG_BUTTONS_COLUMN_WIDTH__ = 200
__FULL_CARD_EDIT_DIALOG_MAIN_BUTTON_HEIGHT__ = 40
__FULL_CARD_EDIT_DIALOG_ICON_COLUMN_WIDTH__ = 40
__LABEL_HEIGHT_SMALL__ = 15

# colors
__PALETTE__ = {
        # purples
        "dark_purple": "#331940",
        "medium_dark_purple": "#482e5b",
        "light_purple": "#9079a3",
        "purple": "#715c83",
        # greens
        "dark_green": "#102821",
        "medium_dark_green": "#213734",
        "green": "#324742",
        "light_green": "#4c6444",
        # browns
        "dark_brown": "#4e2d18",
        "medium_dark_brown": "#8a6340",
        "brown": "#857061",
        "light_brown": "#cbba9d",
        # reds
        "dark_red": "#800000",
        "red": "#aa5454",
        "light_red": "#cb9797",
        # white/gray
        "black": "#000000",
        "dark_gray": "#2a2a2a",
        "medium_dark_gray": "#444444",
        "gray": "#767676",
        "light_gray": "#dedede",
        "white": "#ffffff",
        }

# board items
__BOARD_ITEM_WIDTH__ = 300
__BOARD_ITEM_TITLE_HEIGHT__ = 20
