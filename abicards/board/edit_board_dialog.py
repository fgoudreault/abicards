"""The edit board dialog submodule."""
from typing import Any, Sequence

from PySide6.QtCore import Qt
from PySide6.QtGui import QShortcut
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QLineEdit, QMessageBox,
        QPushButton, QVBoxLayout,
        )

from ..routines import set_widget_font
from ..styles_constants import __BUTTON_HEIGHT_BIG__


class EditBoardDialog(QFrame):
    """The edit board dialog."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, objectName="board_item", **kwargs)
        self.board = self.parent()
        self.convert_to_dialog = None
        self.setup_ui()

    @property
    def subdialogs(self) -> Sequence:
        """The list of subdialogs."""
        subs = [self.convert_to_dialog]
        if self.convert_to_dialog is not None:
            subs += self.convert_to_dialog.subdialogs
        return subs

    def setup_ui(self) -> None:
        """Setup the dialog ui."""
        self.setLayout(QVBoxLayout())
        self.setFixedWidth(300)
        self.title_label = QLabel(
                parent=self, text="Rename board")
        set_widget_font(self.title_label, point_size=14, bold=True)
        self.title_label.setAlignment(Qt.AlignCenter)
        self.layout().addWidget(self.title_label)
        self.line_edit = QLineEdit(parent=self)
        self.line_edit.setPlaceholderText(
                "Enter new board name.")
        self.layout().addWidget(self.line_edit)
        self.setup_buttons()
        self.setup_convert_ui()
        self.setup_delete_button()

    def setup_convert_ui(self) -> None:
        """Setup the conversion ui."""
        self.convert_button = QPushButton(
                parent=self, text="Convert", objectName="gray_btn")
        self.layout().addWidget(self.convert_button)
        self.convert_button.clicked.connect(self.open_convert_to_dialog)

    def setup_delete_button(self) -> None:
        """Setup the delete button."""
        self.delete_label = QLabel("Delete board?")
        set_widget_font(self.delete_label, point_size=14, bold=True)
        self.delete_label.setAlignment(Qt.AlignCenter)
        self.delete_button = QPushButton(
                text="Delete", parent=self, objectName="red_btn",
                )
        self.layout().addWidget(self.delete_label)
        self.layout().addWidget(self.delete_button)
        self.delete_button.clicked.connect(self.confirm_delete)
        self.delete_button.setFixedHeight(__BUTTON_HEIGHT_BIG__)

    def setup_buttons(self) -> None:
        """Setup the dialog buttons."""
        self.buttons_container = QFrame(parent=self)
        self.buttons_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.buttons_container)
        self.save_button = QPushButton(
                text="Save", parent=self.buttons_container,
                objectName="purple_btn")
        self.save_button.clicked.connect(self.save)
        self.save_button.clicked.connect(self.save)
        self.buttons_container.layout().addWidget(self.save_button)
        self.cancel_button = QPushButton(
                text="Cancel", parent=self.buttons_container,
                objectName="transparent_btn")
        self.buttons_container.layout().addWidget(self.cancel_button)
        self.cancel_button.clicked.connect(self.hide_all)
        # shortcuts
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self)
        self.enter_shortcut.activated.connect(self.save)
        self.return_shortcut = QShortcut(Qt.Key_Return, self)
        self.return_shortcut.activated.connect(self.save)
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(self.hide)

    def hideEvent(self, *args, **kwargs) -> Any:
        """Called when hiding the rename dialog."""
        self.return_shortcut.setEnabled(False)
        self.enter_shortcut.setEnabled(False)
        self.escape_shortcut.setEnabled(False)
        self.parent().unhighlight()
        return super().hideEvent(*args, **kwargs)

    def showEvent(self, *args, **kwargs) -> Any:
        """Called when showing the rename dialog."""
        self.return_shortcut.setEnabled(True)
        self.enter_shortcut.setEnabled(True)
        self.escape_shortcut.setEnabled(True)
        self.parent().highlight_widgets(self)
        return super().showEvent(*args, **kwargs)

    def save(self):
        """Save the new board name."""
        self.board.set_title(self.get_title())
        self.hide()

    def get_title(self) -> str:
        """The written new title."""
        return self.line_edit.text()

    def set_title(self, title: str) -> None:
        """Set the current title to given one."""
        self.line_edit.setText(title)

    def setFocus(self):
        """Set the focus on current dialog."""
        self.line_edit.setFocus()
        self.line_edit.selectAll()

    def confirm_delete(self) -> None:
        """Ask user to confirm deleting board."""
        btn = QMessageBox.warning(
                self, "Confirm delete",
                (f"Do you really want to delete board '{self.board.title}'?\n"
                 "This action cannot be undone."),
                QMessageBox.Yes | QMessageBox.Abort,
                )
        if btn == QMessageBox.Yes:
            self.board.delete()

    def open_convert_to_dialog(self) -> None:
        """Open the convert to dialog."""
        from ..cards.convert import ConvertToDialog
        if self.convert_to_dialog is None:
            self.convert_to_dialog = ConvertToDialog(
                    "board", self.board, parent=self.parent())
            self.convert_to_dialog.converted.connect(
                    self.hide_all)
        self.convert_to_dialog.setEnabled(
                self.board.can_be_converted())
        if self.convert_to_dialog.isVisible():
            self.convert_to_dialog.hide()
        else:
            self.convert_to_dialog.move(
                    self.parent().mapFromGlobal(
                        self.convert_button.parent().mapToGlobal(
                            self.convert_button.geometry().bottomLeft())))
            self.convert_to_dialog.show()

    def hide_all(self) -> None:
        """Hide self and subdialogs."""
        self.hide()
        for dialog in self.subdialogs:
            if dialog is None:
                continue
            dialog.hide()
