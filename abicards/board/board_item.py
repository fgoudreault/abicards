"""Base class for board items."""
from typing import Any

from PySide6.QtCore import Signal
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QVBoxLayout, QWidget,
        )

from ..custom_widgets import DotsButton, SmallCloseButton
from ..routines import (
        build_qss, notimplemented_feature, parse_qss, set_widget_font,
        unsaved_changes,
        )
from ..styles_constants import (
        __BOARD_ITEM_TITLE_HEIGHT__,
        __BOARD_ITEM_WIDTH__,
        __PALETTE__,
        )


class BaseBoardItem(QFrame):
    """Base class for board items so that they look uniform."""

    close_when_clicking_outside = False
    editable = True
    has_editable_title = True
    has_collision = False
    has_close_button = True
    has_draggable_subelements = False
    background_color = "black"
    board_item_updated = Signal()
    save_moves = True

    def __init__(
            self, *args,
            objectName="board_item",
            title: str = "",
            **kwargs):
        QFrame.__init__(self, *args, objectName=objectName, **kwargs)
        self._logger = self.parent()._logger
        self.edit_menu = None
        self._title = title
        self.board = self.parent()
        self.setup_ui()

    @property
    def title(self) -> str:
        """The title of the list card."""
        return self._title

    @unsaved_changes
    def set_title(self, title: str, unsaved_changes: bool = True) -> None:
        """Set the card list title.

        Parameters
        ----------
        title: str
            The new title.
        unsaved_changes: bool, optional
            If True and title is different than actual title,
            the user will be warned of unsaved changes.
        """
        if title == self._title:
            return
        self._title = title
        self.title_label.setText(self.title)
        if unsaved_changes:
            self._logger.info(
                    f"Setting name of {self} to '{title}'.")
            return True

    def setup_ui(self) -> None:
        """Setup the ui."""
        self.setFixedWidth(self._get_board_item_width())
        self._set_background_color()
        self.setLayout(QVBoxLayout())
        self.setup_header_ui()
        self.setup_title_ui()

    def _set_background_color(self) -> None:
        """Set the board item background color."""
        name, ss = parse_qss(self.styleSheet())
        ss["background-color"] = __PALETTE__[self.background_color]
        if not name:
            name = f"QFrame#{self.objectName()}"
        self.setStyleSheet(build_qss(name, ss))

    def setup_header_ui(self) -> None:
        """Setup the header ui."""
        # header is the edit button + spacer to grab widget
        self.header_container = QWidget(parent=self)
        self.header_container.setContentsMargins(0, 0, 0, 0)
        self.header_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.header_container)
        self.header_container.layout().addStretch()

        if self.editable:
            self.edit_button = DotsButton(parent=self.header_container)
            self.edit_button.clicked.connect(self.toggle_edit_menu)
        else:
            if self.has_close_button:
                self.edit_button = SmallCloseButton(parent=self.header_container)
                self.edit_button.clicked.connect(self.close)
            else:
                self.edit_button = None
        if self.edit_button is not None:
            self.edit_button.setFixedSize(20, 20)
            self.header_container.layout().addWidget(self.edit_button)

    def setup_title_ui(self) -> None:
        """Setup the title UI."""
        self.title_label_container = QWidget(parent=self)
        self.title_label_container.setLayout(QHBoxLayout())
        self.title_label = QLabel(
                self.title, parent=self.title_label_container,
                objectName="board_item_title_label",
                )
        set_widget_font(self.title_label, bold=True, point_size=15)
        self.title_label_container.layout().addWidget(self.title_label)
        self.title_label_container.layout().addStretch()
        self.title_label.setFixedHeight(__BOARD_ITEM_TITLE_HEIGHT__)
        self.layout().addWidget(self.title_label_container)

    @notimplemented_feature
    def toggle_edit_menu(self):
        """Toggle the edit menu."""
        pass

    @unsaved_changes
    def move(self, *args, unsaved_changes: bool = None, **kwargs) -> Any:
        """Move the board item."""
        if unsaved_changes is None:
            unsaved_changes = self.save_moves  # default
        curr_pos = self.pos()
        QFrame.move(self, *args, **kwargs)
        new_pos = self.pos()
        if (curr_pos.x() != new_pos.x() or curr_pos.y() != new_pos.y()):
            if unsaved_changes:
                # self._logger.info(f"Moving {self}.")
                return True

    def _get_board_item_width(self) -> int:
        """Get the board item width. By default it is a constant."""
        return __BOARD_ITEM_WIDTH__

    def get_json_data(self) -> dict[str, Any]:
        """Construct the json data dict for this board item."""
        data = {"pos": (self.pos().x(), self.pos().y())}
        if self.has_editable_title:
            data["title"] = self.title
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load data from the json file."""
        self.set_title(data["title"], unsaved_changes=False)
        if "pos" in data:
            self.move(*data["pos"], unsaved_changes=False)
