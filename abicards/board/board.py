"""The board module."""
import logging
from typing import Any, Sequence

from PySide6.QtCore import QMimeData, QPoint, QRect, Qt, Signal
from PySide6.QtGui import QDrag
from PySide6.QtWidgets import (
        QFrame,
        )

from .edit_board_dialog import EditBoardDialog
from ..cards.convert import ConvertibleObject
from ..routines import (
        get_app, get_loglevel, get_main_toolbar, get_main_widget,
        unsaved_changes,
        )
from ..styles_constants import __PALETTE__


def isabove(w1, w2) -> bool:
    """Return true if w1 is above w2."""
    b1, t2 = w1.geometry().bottom(), w2.geometry().top()
    if b1 > t2:
        return False
    # build a rect with same top and check if intersect
    rect = QRect(w1.geometry().topLeft(), w1.geometry().bottomRight())
    rect.moveTop(t2)
    return rect.intersects(w2.geometry())


def isbelow(w1, w2) -> bool:
    """Return true if w1 is directly below w2."""
    t1, b2 = w1.geometry().top(), w2.geometry().bottom()
    if t1 < b2:
        return False
    # build a rect with same top and check if intersect
    rect = QRect(w1.geometry().topLeft(), w1.geometry().bottomRight())
    rect.moveBottom(b2)
    return rect.intersects(w2.geometry())


def toleft(w1, w2) -> bool:
    """Return true if w1 is directly to the left of w2."""
    r1, l2 = w1.geometry().right(), w2.geometry().left()
    if r1 > l2:
        return False
    # build a rect with same top and check if intersect
    rect = QRect(w1.geometry().topLeft(), w1.geometry().bottomRight())
    rect.moveLeft(l2)
    return rect.intersects(w2.geometry())


def toright(w1, w2) -> bool:
    """Return true if w1 is directly to the right of w2."""
    l1, r2 = w1.geometry().left(), w2.geometry().right()
    if l1 < r2:
        return False
    # build a rect with same top and check if intersect
    rect = QRect(w1.geometry().topLeft(), w1.geometry().bottomRight())
    rect.moveRight(r2)
    return rect.intersects(w2.geometry())


class Board(QFrame, ConvertibleObject):
    """Represent the whole board with all its subwidgets."""

    board_item_updated = Signal()
    title_changed = Signal()

    def __init__(self, *args, **kwargs):
        self.title = ""
        super().__init__(*args, objectName="board", **kwargs)
        self._logger = logging.getLogger("Board")
        self._logger.setLevel(get_loglevel())
        self.board_items = []
        self.planer = None
        self.setAcceptDrops(True)
        self._drag_source = None
        self._drag_start_pos = None
        self._drag_delta = QPoint(0, 0)
        self.overlay = None  # to highlight widgets
        self.edit_dialog = None
        self.setup_ui()

    def __repr__(self) -> str:
        """Board string representation."""
        return f"< Board: '{self.title}' >"

    def can_be_converted(self) -> bool:
        """Return True if board can be converted into something else."""
        if self.planer:
            return False
        if self.card_lists:
            return False
        return True

    def setup_ui(self):
        """Setup the board UI."""
        self.setStyleSheet(
                f"""
                QFrame#board {{
                    background-color: {__PALETTE__['medium_dark_gray']}
                    }}
                """)
        # this widget has no layout. everything is floating inside it
        # self.setStyleSheet("background-color: rgba(255, 0, 0, 100)")

    @unsaved_changes
    def set_title(self, title: str, unsaved_changes: bool = True) -> None:
        """Set the board title."""
        if title == self.title:
            return
        og = self.title
        self.title = title
        self.title_changed.emit()
        if unsaved_changes:
            self._logger.info(
                    f"Setting board title '{og}' => '{title}'.")
            return True

    @unsaved_changes
    def add_card_list(
            self, load_data: dict[str, Any] = None,
            unsaved_changes: bool = True,
            ) -> None:
        """Add a card list.

        Parameters
        ----------
        load_data: dict, optional
            The json data to load the card list if necessary.
        """
        from ..cards import CardList
        cardlist = CardList(
                parent=self,
                edit_title_upon_creation=load_data is None,
                )
        if load_data is not None:
            cardlist.load_json_data(load_data)
        self.board_items.append(cardlist)
        cardlist.card_updated.connect(self.board_item_updated)
        cardlist.card_list_updated.connect(self.board_item_updated)
        cardlist.show()
        if unsaved_changes:
            self._logger.info(f"Adding card list to '{self}'.")
            return True

    def clear_board(self) -> None:
        """Remove all widgets from board."""
        for item in self.board_items:
            item.hide()
            item.destroy()
        self.board_items = []

    @unsaved_changes
    def delete(self) -> None:
        """Delete the board."""
        get_main_widget().delete_board(self)
        return True

    @unsaved_changes
    def add_planer(self, load_from: dict[str, Any] = None) -> None:
        """Add the planer card."""
        if self.planer is not None:
            return
        from ..planer import Planer
        self.planer = Planer(parent=self)
        if load_from is not None:
            self.planer.load_json_data(load_from)
        self.board_items.append(self.planer)
        self.update_main_toolbar_buttons()
        self.planer.show()
        if load_from is None:
            self._logger.info("Adding planer.")
            return True

    def update_main_toolbar_buttons(self) -> None:
        """Enables or disables some main tool bar buttons."""
        tb = get_main_toolbar()
        if tb is None:
            # no toolbar exists for some reason
            return
        tb.add_planer_button.setEnabled(self.planer is None)

    @unsaved_changes
    def remove_planer(self) -> None:
        """Remove the planer."""
        self.board_items.remove(self.planer)
        self.planer.hide()
        self.planer.destroy()
        self.planer = None
        self.update_main_toolbar_buttons()
        self._logger.info("Removing planer.")
        return True

    @property
    def card_lists(self) -> Sequence:
        """The list of card list widgets."""
        items = []
        from ..cards import CardList
        for item in self.board_items:
            if isinstance(item, CardList):
                items.append(item)
        return items

    def get_json_data(self) -> list[dict]:
        """Return data for json storage."""
        data_card_lists = []
        for card_list in self.card_lists:
            data_card_lists.append(
                    card_list.get_json_data())
        if self.planer is not None:
            planer_data = self.planer.get_json_data()
        else:
            planer_data = None
        return {
                "card_lists": data_card_lists,
                "planer": planer_data,
                "title": self.title,
                }

    def load_json_data(self, data: list[dict]) -> None:
        """Load data from a json object."""
        # clear any card lists if there was any
        self.clear_board()
        if "card_list_managers" in data:
            # legacy
            for card_list_manager_data in data["card_list_managers"]:
                self.add_card_list(
                        load_data=card_list_manager_data["card_list"],
                        unsaved_changes=True,
                        )
        if "card_lists" in data:
            for card_list_data in data["card_lists"]:
                self.add_card_list(
                        load_data=card_list_data, unsaved_changes=False)
        if "planer" in data:
            if data["planer"] is not None:
                self.add_planer(load_from=data["planer"])
        if "title" in data:
            self.set_title(data["title"], unsaved_changes=False)

    def dragEnterEvent(self, event) -> None:
        """Define what happens when we drag a card list within."""
        event.accept()

    def dragMoveEvent(self, event) -> None:
        """Defines what happens when we're dragging a card list."""
        if event.buttons() != Qt.LeftButton or self._drag_source is None:
            return super().dragMoveEvent(event)
        left = event.pos().x() + self._drag_delta.x()
        top = event.pos().y() + self._drag_delta.y()
        topleft = QPoint(left, top)
        right = left + self._drag_source.width()
        bottom = top + self._drag_source.height()
        target_rect = QRect(topleft, QPoint(right, bottom))
        if self._drag_source in self.board_items:
            # handle collision
            # modify target rectangle so that no collision occurs
            if hasattr(self._drag_source, "has_collision"):
                if self._drag_source.has_collision:
                    bbox_rect = self._bounding_box_rectangle(self._drag_source)
                    self._handle_collision(bbox_rect, target_rect)
        else:
            # a subitem: it will handle itself
            self._drag_source.handle_drag_move(target_rect)
        self._drag_source.move(target_rect.topLeft())

    def _handle_collision(self, bbox_rect, target_rect):
        """Handle the collision for target rectangle to fit within bbox."""
        # rectify target rect according to bounding box
        if target_rect.left() < bbox_rect.left():
            target_rect.moveLeft(bbox_rect.left())
        if target_rect.right() > bbox_rect.right():
            target_rect.moveRight(bbox_rect.right())
        if target_rect.top() < bbox_rect.top():
            target_rect.moveTop(bbox_rect.top())
        if target_rect.bottom() > bbox_rect.bottom():
            target_rect.moveBottom(bbox_rect.bottom())

    def _bounding_box_rectangle(self, widget):
        """Get bounding box for given widget."""
        directly_above, directly_below = [], []
        directly_left, directly_right = [], []
        spacing = 3
        for w in self.board_items:
            if w is widget:
                continue
            if hasattr(w, "has_collision"):
                if not w.has_collision:
                    continue
            if not w.isVisible():
                # otherwise we collide with something invisible
                continue
            if isabove(w, widget):
                directly_above.append(w)
                continue
            if isbelow(w, widget):
                directly_below.append(w)
                continue
            if toright(w, widget):
                directly_right.append(w)
                continue
            if toleft(w, widget):
                directly_left.append(w)
        # build 'walls'
        if not directly_above:
            top = self.geometry().top()
        else:
            top = max((w.geometry().bottom()
                       for w in directly_above)) + spacing
        if not directly_below:
            bottom = self.geometry().bottom()
        else:
            bottom = min((w.geometry().top()
                          for w in directly_below)) - spacing
        if not directly_left:
            left = 0
        else:
            left = max((w.geometry().right() for w in directly_left)) + spacing
        if not directly_right:
            right = self.width()
        else:
            right = min((w.geometry().left()
                         for w in directly_right)) - spacing
        return QRect(QPoint(left, top), QPoint(right, bottom))

    def dropEvent(self, event) -> None:
        """Define what happens when we drop a dragged card."""
        if hasattr(self._drag_source, "pop_from_parent"):
            self._drag_source.put_into_new_parent()
            self._drag_source.show()
        self.reset_drag_state()

    def reset_drag_state(self):
        """Reset draggable stuff status."""
        self._drag_source = None
        self._drag_start_pos = None
        self._drag_delta = QPoint(0, 0)

    def subdialogs_opened(self) -> bool:
        """Return True if some subdialogs are opened and under the mouse."""
        if self.edit_dialog is not None:
            if self.edit_dialog.isVisible():
                return True
        for item in self.board_items:
            if not hasattr(item, "subdialogs_opened"):
                continue
            if item.subdialogs_opened():
                return True
        if self.edit_dialog is not None:
            if self.edit_dialog.isVisible():
                return True
        return False

    def close_subdialogs(self) -> None:
        """Close subdialogs."""
        if self.edit_dialog is not None:
            if self.edit_dialog.underMouse():
                return
            any_sub_under_mouse = False
            for sub in self.edit_dialog.subdialogs:
                if sub is None:
                    continue
                if sub.underMouse():
                    any_sub_under_mouse = True
                    break
            if not any_sub_under_mouse:
                self.edit_dialog.hide_all()
        for item in self.board_items:
            if hasattr(item, "close_when_clicking_outside"):
                if item.close_when_clicking_outside:
                    any_subdialog_under_mouse = any(
                            [s.underMouse()
                             for s in item.subdialogs if s is not None])
                    if not item.underMouse() and not any_subdialog_under_mouse:
                        item.hide()
            if not hasattr(item, "close_subdialogs"):
                continue
            if not item.subdialogs_opened():
                continue
            if item.underMouse():
                # let the item handle the close events itself
                continue
            item.close_subdialogs()

    def _get_item_under_event(self, event):
        app = get_app()
        item = app.widgetAt(self.parent().mapToGlobal(event.pos()))
        if item is None:
            return None
        # item is directly under the mouse
        # detect whose widget it is part of
        for board_item in self.board_items:
            if item is board_item:
                return board_item
            children = board_item.findChildren(item.__class__)
            if item not in children:
                continue
            # this board item contains the widget
            return board_item
        return None

    def mouseReleaseEvent(self, *args, **kwargs) -> Any:
        """Called when mouse is released."""
        self.reset_drag_state()
        return super().mouseReleaseEvent(*args, **kwargs)

    def mousePressEvent(self, event) -> Any:
        """Called when mouse is pressed inside board."""
        if event.buttons() == Qt.LeftButton:
            self.close_subdialogs()
            item = self._get_item_under_event(event)
            if item is None:
                return super().mousePressEvent(event)  # nothing to do
            # raise to be sure, unless something is highlighted
            if self.overlay is not None:
                if not self.overlay.isVisible():
                    item.raise_()
            if hasattr(item, "has_draggable_subelements"):
                if not item.has_draggable_subelements:
                    # start dragging the whole item
                    self._set_dragging_widget(item, event)
                    return
            else:
                # assume false
                self._set_dragging_widget(item, event)
                return
            # check we're not dragging anything within item
            subitem = item.draggable_subelement()
            if subitem is None:
                # drag whole item
                self._set_dragging_widget(item, event)
                return
            # drag subitem
            self._set_dragging_widget(subitem, event)
        return super().mousePressEvent(event)

    def _set_dragging_widget(self, widget, event):
        self._drag_source = widget
        self._drag_start_pos = self.mapToGlobal(event.pos())

    def mouseMoveEvent(self, event) -> Any:
        """Called when mouse is moved."""
        if event.buttons() != Qt.LeftButton:
            return super().mouseMoveEvent(event)
        if self._drag_source is None:
            # we're not dragging anything
            return super().mouseMoveEvent(event)
        app = get_app()
        evpos = self.mapToGlobal(event.pos())
        if (evpos - self._drag_start_pos).manhattanLength() < (
                app.startDragDistance()):
            # did not move long enough to start drag and drop
            return super().mouseMoveEvent(event)
        self._logger.info(f"Starting to drag widget '{self._drag_source}'.")
        # if dragging widget can be popped from parent do it
        if hasattr(self._drag_source, "pop_from_parent"):
            # set as parent for dragging purposes
            abs_pos = self._drag_source.parent().mapToGlobal(
                    self._drag_source.pos())
            self._drag_source.pop_from_parent(self)
            # move back at same place to 'reset' position with new parent
            # since board does not have layout
            self._drag_source.move(
                    self.mapFromGlobal(abs_pos),
                    )
            self._drag_source.show()
        # sometimes, drag source is reset since last check once we're here
        if self._drag_source is None:
            return super().mouseMoveEvent(event)
        # start a drag and drop operation
        global_src = self.mapToGlobal(self._drag_source.pos())
        self._drag_delta = QPoint(
                global_src.x() - evpos.x(),
                global_src.y() - evpos.y(),
                )
        drag = QDrag(self)
        mime = QMimeData()
        drag.setMimeData(mime)
        drag.exec_(Qt.MoveAction)

    @unsaved_changes
    def delete_card_list(self, card_list) -> None:
        """Delete the card list."""
        if card_list not in self.board_items:
            raise ValueError("Not a card list.")
        card_list.hide()
        self.board_items.remove(card_list)
        card_list.destroy()
        self._logger.info(f"Deleting '{card_list.title}'.")
        return True

    def highlight_widgets(self, *widgets) -> None:
        """Bring forth a global dark overlay to highlight >= 1 widgets."""
        if self.overlay is None:
            self.overlay = QFrame(parent=self)
            self.overlay.setStyleSheet(
                    "background-color: rgba(0, 0, 0, 150)",
                    )
            self.overlay.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.overlay.resize(self.size())
        self.overlay.show()
        for widget in widgets:
            self.overlay.stackUnder(widget)

    def unhighlight(self) -> None:
        """Remove highlighting."""
        if self.overlay is None:
            return
        self.overlay.hide()

    def resizeEvent(self, *args, **kwargs) -> Any:
        """Handle resize events."""
        # also resize the global overlay as it is not part of a layout
        if self.overlay is not None:
            self.overlay.resize(self.size())
        return super().resizeEvent(*args, **kwargs)

    def center_widget(self, w) -> None:
        """Center the widget on screen."""
        center = self.geometry().center()
        x = center.x() - w.width() // 2
        y = center.y() - w.height() // 2
        w.move(x, y)

    def toggle_edit_dialog(self) -> None:
        """Open the edit dialog."""
        if self.edit_dialog is None:
            self.edit_dialog = EditBoardDialog(parent=self)
        self.center_widget(self.edit_dialog)
        self.edit_dialog.set_title(self.title)
        if self.edit_dialog.isVisible():
            self.edit_dialog.hide()
            return
        self.edit_dialog.show()
        self.edit_dialog.setFocus()
