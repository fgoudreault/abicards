"""The pomodoro time setter dialog submodule."""
from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QIntValidator, QShortcut
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout, QLabel, QLineEdit, QPushButton,
        QVBoxLayout, QWidget,
        )

from .routines import seconds_to_hms
from ..custom_widgets import SmallCloseButton


class TimeSetterDialog(QFrame):
    """The pomodoro time setters dialog."""

    cancel = Signal()
    setting_done = Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, objectName="dark_grey_dialogs", **kwargs)
        self.clock = "work"
        self.setup_ui()

    def setup_ui(self):
        """Setup the dialog UI."""
        self.setLayout(QVBoxLayout())
        # header
        self.header_container = QWidget(parent=self)
        self.header_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.header_container)
        self.desc_label = QLabel(parent=self, text="")
        self.desc_label.setAlignment(Qt.AlignCenter)
        self.set_clock(self.clock)
        self.header_container.layout().addWidget(self.desc_label)
        self.close_button = SmallCloseButton(parent=self)
        self.close_button.clicked.connect(self.cancel)
        self.header_container.layout().addWidget(self.close_button)

        # lineedits
        self.setup_lineedit_ui()
        # buttons
        self.setup_buttons()
        self.adjustSize()

    def setup_buttons(self):
        """Setup the buttons."""
        self.buttons_container = QWidget(parent=self)
        self.buttons_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.buttons_container)
        self.save_button = QPushButton(
                parent=self, text="Save", objectName="gray_btn_centered")
        self.buttons_container.layout().addWidget(self.save_button)
        self.save_button.clicked.connect(self.setting_done)

        self.cancel_button = QPushButton(
                parent=self, text="Cancel", objectName="gray_btn_centered")
        self.buttons_container.layout().addWidget(self.cancel_button)
        self.cancel_button.clicked.connect(self.cancel)

        # shortcuts
        self.enter_shortcut = QShortcut(Qt.Key_Enter, self)
        self.enter_shortcut.activated.connect(self.setting_done)
        self.return_shortcut = QShortcut(Qt.Key_Return, self)
        self.return_shortcut.activated.connect(self.setting_done)
        self.escape_shortcut = QShortcut(Qt.Key_Escape, self)
        self.escape_shortcut.activated.connect(self.cancel)

    def showEvent(self, *args, **kwargs) -> None:
        """Called when showing dialog."""
        for shortcut in [
                self.enter_shortcut, self.return_shortcut,
                self.escape_shortcut]:
            shortcut.setEnabled(True)
        return super().showEvent(*args, **kwargs)

    def hideEvent(self, *args, **kwargs) -> None:
        """Called when hiding dialog."""
        for shortcut in [
                self.enter_shortcut, self.return_shortcut,
                self.escape_shortcut]:
            shortcut.setEnabled(False)
        return super().hideEvent(*args, **kwargs)

    def setup_lineedit_ui(self):
        """Setup the lineedits."""
        self.lineedits_container = QWidget(parent=self)
        self.lineedits_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.lineedits_container)

        self.hours_lineedit = QLineEdit(parent=self)
        self.hours_lineedit.setValidator(QIntValidator(0, 99, parent=self))
        self.hours_lineedit.setFixedWidth(30)
        self.lineedits_container.layout().addWidget(self.hours_lineedit)
        self.hours_label = QLabel(text="h", parent=self)
        self.hours_label.setAlignment(Qt.AlignLeft | Qt.AlignBottom)
        self.lineedits_container.layout().addWidget(self.hours_label)

        self.minutes_lineedit = QLineEdit(parent=self)
        self.minutes_lineedit.setValidator(QIntValidator(0, 59, parent=self))
        self.minutes_lineedit.setFixedWidth(30)
        self.lineedits_container.layout().addWidget(self.minutes_lineedit)
        self.minutes_label = QLabel(text="m", parent=self)
        self.minutes_label.setAlignment(Qt.AlignLeft | Qt.AlignBottom)
        self.lineedits_container.layout().addWidget(self.minutes_label)

        self.seconds_lineedit = QLineEdit(parent=self)
        self.seconds_lineedit.setValidator(QIntValidator(0, 59, parent=self))
        self.seconds_lineedit.setFixedWidth(30)
        self.lineedits_container.layout().addWidget(self.seconds_lineedit)
        self.seconds_label = QLabel(text="s", parent=self)
        self.seconds_label.setAlignment(Qt.AlignLeft | Qt.AlignBottom)
        self.lineedits_container.layout().addWidget(self.seconds_label)

    def set_clock(self, clock: str) -> None:
        """Set the clock type for the setter dialog."""
        self.clock = clock
        self.desc_label.setText(
                f"Set the time for the '{clock}' part\n of the pomodoro.")

    def set_current_time(self, seconds) -> None:
        """Set the time in the line edits."""
        h, m, s = seconds_to_hms(seconds)
        self.hours_lineedit.setText(str(h))
        self.minutes_lineedit.setText(str(m))
        self.seconds_lineedit.setText(str(s))

    def get_current_time(self) -> int:
        """Return the time in seconds."""
        h = int(self.hours_lineedit.text())
        m = int(self.minutes_lineedit.text())
        s = int(self.seconds_lineedit.text())
        seconds = h * 3600 + m * 60 + s
        if seconds == 0:
            # set to 1 sec
            return 1
        return seconds
