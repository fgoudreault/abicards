"""Routines for the pomodoro submodule."""


def seconds_to_hms(seconds: int) -> tuple[int]:
    """Return number of hours, minutes and seconds in given time in secs."""
    hh, mm = 0, 0
    if seconds >= 3600:
        hh = int(seconds // 3600)
        seconds -= 3600 * hh
    if seconds >= 60:
        mm = int(seconds // 60)
        seconds -= mm * 60
    return hh, mm, seconds
