"""The timer card module."""
import os
import time
from math import floor
from typing import Any

from PySide6.QtCore import QPoint, QRectF, QTimer, QUrl, Qt
from PySide6.QtGui import QColor, QPainter, QPainterPath, QPen, QPixmap
from PySide6.QtMultimedia import QAudioOutput, QMediaPlayer
from PySide6.QtWidgets import (
        QFrame, QHBoxLayout,
        QLabel, QPushButton, QVBoxLayout, QWidget,
        )

from .pomodoro_time_setter_dialog import TimeSetterDialog
from .routines import seconds_to_hms
from ..board import BaseBoardItem
from ..routines import (
        get_icon_path, get_main_widget,
        get_main_window, set_widget_font,
        )


class Pomodoro(BaseBoardItem):
    """Timer card class."""

    editable = False
    has_collision = False
    has_close_button = False

    def __init__(self, *args, **kwargs):
        self.remaining_time = 60 * 50  # remaining time
        self._work_time = 60 * 50  # total work time in seconds
        self._break_time = 60 * 10
        self.state = "work"
        self.init_time = None
        self.running = False
        self.time_setter_dialog = None
        super().__init__(*args, title="Pomodoro", **kwargs)
        self.setWindowFlags(Qt.Dialog)
        self.setWindowTitle("Pomodoro")
        # load on startup to avoid lag
        self.media_player = QMediaPlayer()
        self.audio_output = QAudioOutput()
        self.media_player.setAudioOutput(self.audio_output)
        self.media_player.setSource(QUrl.fromLocalFile(
            os.path.join(
                os.path.dirname(__file__), "imperial_alert_2.mp3")))
        self.audio_output.setVolume(50)

    @property
    def work_time(self) -> int:
        """The work time in seconds."""
        return self._work_time

    @property
    def break_time(self) -> int:
        """The break time in seconds."""
        return self._break_time

    def get_json_data(self):
        """Get the json representation of the timer."""
        data = super().get_json_data()
        data["work_time"] = self.work_time
        data["break_time"] = self.break_time
        return data

    def load_json_data(self, data: dict[str, Any]) -> None:
        """Load the timer data from a json object."""
        super().load_json_data(data)
        if "break_time" in data:
            self.set_break_time(data["break_time"], unsaved_changes=False)
        if "work_time" in data:
            self.set_work_time(data["work_time"], unsaved_changes=False)
        self.remaining_time = self.work_time

    def set_break_time(self, btime: int, unsaved_changes: bool = True) -> None:
        """Set the break time."""
        if btime == self.break_time:
            return
        self._break_time = btime
        self.reset()

    def set_work_time(self, wtime: int, unsaved_changes: bool = True) -> None:
        """Set the work time."""
        if wtime == self.work_time:
            return
        self._logger.info(f"Setting work time to {wtime} seconds.")
        self._work_time = wtime
        self.reset()

    def closeEvent(self, *args, **kwargs) -> Any:
        """Remove from board so that it is not saved."""
        self.parent().remove_pomodoro()
        return super().closeEvent(*args, **kwargs)

    def setup_ui(self) -> None:
        """Setup the UI."""
        super().setup_ui()
        self.setup_clock()
        self.setup_buttons()
        self.update_pomodoro_clock()

    def setup_buttons(self):
        """Setup the buttons."""
        self.buttons_container = QWidget(parent=self)
        self.buttons_container.setLayout(QHBoxLayout())
        self.layout().addWidget(self.buttons_container)
        self.start_button = QPushButton(
                parent=self, objectName="gray_btn_centered", text="Start")
        self.buttons_container.layout().addWidget(self.start_button)
        self.start_button.clicked.connect(self.toggle_start_stop)
        self.reset_button = QPushButton(
                parent=self, objectName="gray_btn_centered", text="Reset")
        self.buttons_container.layout().addWidget(self.reset_button)
        self.reset_button.clicked.connect(self.reset)

    def toggle_start_stop(self):
        """Toggle start or pause state."""
        if self.start_button.text() == "Start":
            self.start()
        elif self.start_button.text() == "Pause":
            self.pause()
        elif self.start_button.text() == "Resume":
            self.resume()

    def reset(self):
        """Reset pomodoro timer."""
        self.stop()
        self.state = "work"
        self.remaining_time = self.work_time
        self.update_pomodoro_clock()

    def start(self, reset_remaining_time: bool = True) -> None:
        """Start the clock."""
        self.start_button.setText("Pause")
        self.running = True
        self.init_time = time.perf_counter()
        self.clock.toggle_work_icon()
        if reset_remaining_time:
            if self.state == "work":
                self.remaining_time = self.work_time
            else:
                self.remaining_time = self.break_time
        self.timer.start()

    def resume(self):
        """Resume the clock."""
        self.start(reset_remaining_time=False)

    def pause(self):
        """Pause the clock."""
        self.running = False
        self.start_button.setText("Resume")
        self.timer.stop()
        self.clock.toggle_work_icon()

    def stop(self):
        """Stop the clock."""
        if self.running:
            self.clock.toggle_work_icon()
        self.running = False
        self.timer.stop()
        self.start_button.setText("Start")

    def setup_clock(self):
        """Setup the clock."""
        self.clock = PomodoroClockWidget(parent=self)
        self.clock.setFixedHeight(282)  # correct height for width for a square
        self.layout().addWidget(self.clock)
        self.timer = QTimer()
        self.timer.setInterval(10)  # milliseconds
        # self.timer.callOnTimeout(self.tick)
        self.timer.timeout.connect(self.tick)

    def open_work_time_setter(self):
        """Set the work time."""
        self.open_time_set_dialog("work")

    def open_break_time_setter(self):
        """Set the break time."""
        self.open_time_set_dialog("break")

    def open_time_set_dialog(self, timer: str) -> None:
        """Open the time setter dialog."""
        main_widget = get_main_widget()
        if self.time_setter_dialog is None:
            self.time_setter_dialog = TimeSetterDialog(
                    parent=main_widget)
            self.time_setter_dialog.setting_done.connect(
                    self.set_time_from_dialog)
            self.time_setter_dialog.cancel.connect(
                    self.hide_time_setter_dialog)
        timer_rect = self.time_setter_dialog.geometry()
        timer_rect.moveCenter(main_widget.rect().center())
        self.time_setter_dialog.move(timer_rect.topLeft())
        self.time_setter_dialog.set_clock(timer)
        self.time_setter_dialog.set_current_time(
                getattr(self, f"{timer}_time"))
        self.time_setter_dialog.show()
        main_widget.highlight_widgets(self.time_setter_dialog)

    def hide_time_setter_dialog(self):
        """Hide the time setter dialog."""
        self.time_setter_dialog.hide()
        get_main_widget().unhighlight()

    def set_time_from_dialog(self) -> None:
        """Set the time from the time setter dialog."""
        if self.time_setter_dialog.clock == "work":
            self.set_work_time(
                    self.time_setter_dialog.get_current_time())
        else:
            self.set_break_time(
                    self.time_setter_dialog.get_current_time())
        self.hide_time_setter_dialog()

    def tick(self):
        """Advance the clock."""
        now = time.perf_counter()
        elapsed = now - self.init_time  # in seconds
        self.init_time = now
        self.remaining_time -= elapsed
        if self.remaining_time <= 0:
            self.alert()
            if self.state == "work":
                self.state = "break"
                self.remaining_time = self.break_time
            else:
                self.state = "work"
                self.remaining_time = self.work_time
            self.clock.toggle_work_icon()
        self.update_pomodoro_clock()

    def ring(self) -> None:
        """Ring a bell for user."""
        self.media_player.play()

    def alert(self) -> None:
        """Alert user that the timer is over."""
        self.ring()
        # bring window to front
        main_window = get_main_window()
        main_window.show()
        main_window.raise_()
        main_window.setWindowState(Qt.WindowState.WindowActive)

    def update_pomodoro_clock(self) -> None:
        """Update pomodoro clock widget."""
        # compute new clock configuration
        if self.state == "work":
            total_time = self.work_time
            self.clock.set_label_text(
                    self.get_time_label(self.break_time), clock="break")
        else:
            total_time = self.break_time
            self.clock.set_label_text(
                    self.get_time_label(self.work_time), clock="work")
        ratio = self.remaining_time / total_time
        oneminusratio = 1 - ratio
        angle = oneminusratio * 360
        red = oneminusratio * 255
        green = ratio * 255
        label = self.get_time_label(self.remaining_time)
        self.clock.set_label_text(label, clock=self.state)
        self.clock.set_angle(angle)
        self.clock.set_color(red, green, 0)

    def get_time_label(self, rest: int) -> str:
        """Build the time label hh:mm:ss string."""
        hh, mm, ss = seconds_to_hms(rest)
        hh = str(hh).zfill(2)
        mm = str(mm).zfill(2)
        ss = str(int(floor(ss))).zfill(2)
        return f"{hh}:{mm}:{ss}"


class PomodoroClockWidget(QFrame):
    """An open circle widget."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.angle = 0
        self.red, self.green, self.blue = 255, 255, 255
        self.setup_ui()

    def set_color(self, r, g, b):
        """Set the clock color."""
        self.red = r
        self.green = g
        self.blue = b

    def setup_ui(self):
        """Setup the UI."""
        self.setLayout(QVBoxLayout())  # QGraphicsLinearLayout())
        self.setContentsMargins(0, 0, 0, 0)
        self.work_info = QFrame(parent=self)
        self.layout().addWidget(self.work_info)
        self.work_info.setLayout(QVBoxLayout())
        self.work_info.layout().setContentsMargins(0, 0, 0, 0)

        self.break_info = QFrame(parent=self)
        self.layout().addWidget(self.break_info)
        self.break_info.setLayout(QVBoxLayout())
        self.break_info.layout().setContentsMargins(0, 0, 0, 0)

        self.work_label = QLabel(parent=self, text="work")
        self.work_label.setFixedHeight(75)
        set_widget_font(self.work_label, point_size=14)
        self.work_label.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self.work_info.layout().addWidget(self.work_label)
        self.work_time_label = QPushButton(
                parent=self, text="00:50:00", objectName="transparent_btn")
        self.work_time_label.clicked.connect(
                self.parent().open_work_time_setter)
        self.work_time_label.setFixedWidth(150)
        set_widget_font(self.work_time_label, bold=True, point_size=24)
        self.work_info.layout().addWidget(
                self.work_time_label,
                alignment=Qt.AlignHCenter,
                )
        # work/break icon in middle
        self.setup_working_icon()

        # break label and timer ui
        self.break_time_label = QPushButton(
                parent=self, text="00:10:00",
                objectName="transparent_btn")
        self.break_time_label.clicked.connect(
                self.parent().open_break_time_setter)
        self.break_time_label.setFixedWidth(150)
        set_widget_font(self.break_time_label, bold=True, point_size=18)
        self.break_info.layout().addWidget(
                self.break_time_label, alignment=Qt.AlignHCenter)

        self.break_label = QLabel(parent=self, text="break")
        set_widget_font(self.break_label, point_size=14)
        self.break_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
        self.break_info.layout().addWidget(self.break_label)
        self.layout().addStretch()

    def setup_working_icon(self) -> None:
        """Setup the work icon that is shown when working."""
        self.work_icon = QLabel(parent=self)
        self.work_icon.setPixmap(
                QPixmap(get_icon_path("desktop_icon_small.png")))
        self.work_icon.setFixedSize(100, 100)
        self.work_icon.setScaledContents(True)
        self.layout().addWidget(self.work_icon, alignment=Qt.AlignCenter)
        self.work_icon.hide()

    def toggle_work_icon(self) -> None:
        """Show or hide the work icon."""
        if self.work_icon.isVisible():
            self.work_icon.hide()
            self.work_time_label.show()
            self.break_time_label.show()
            self.break_label.show()
        else:
            self.work_icon.show()
            self.work_time_label.hide()
            self.break_time_label.hide()
            self.break_label.hide()

    def set_angle(self, angle: float):
        """Set the angle where the circle is not drawn."""
        self.angle = angle
        self.update()

    def set_label_text(self, text: str, clock: str = "work") -> None:
        """Set the timer label text."""
        if clock == "work":
            self.work_time_label.setText(text)
        else:
            self.break_time_label.setText(text)

    def paintEvent(self, event):
        """Called when painting the clock."""
        # path
        gap = 20
        path = QPainterPath()
        path.moveTo(QPoint(self.width() // 2, gap))
        rect = QRectF(
                QPoint(gap, gap),
                QPoint(self.width() - gap, self.height() - gap))
        path.arcTo(rect, 90, (360 - self.angle))
        # painter
        painter = QPainter(self)
        pen = QPen(QColor(self.red, self.green, self.blue))
        pen.setWidth(3)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setPen(pen)
        painter.drawPath(path)
        super().paintEvent(event)
